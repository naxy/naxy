<?php
	
	 class Menu{
		public $id;
		public $nom;
		public $fichier;
		
                function Menu($idE, $nomE,$fichierE){
                    $this->setId($idE);
                    $this->setNom($nomE);
                    $this->setFichier($fichierE);
                }
		function getId(){
			return $this->id;
		}
		
		function setId($newId){
			$this->id = $newId;
		}
				
		function getNom(){
			return $this->nom;
		}
		
		function setNom($newnom){
			$this->nom = $newnom;
		}
		function getFichier(){
			return $this->fichier;
		}
		
		function setFichier($newfichier){
			$this->fichier = $newfichier;
		}
	?>