<?php
  require("Connexion.php");    
  require("UserDAO.php");
  $uDB = new UserDAO();
  $user = $uDB->findUser();

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">


<!-- Mirrored from demo.frontted.com/hero/260320180000/ui-tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Apr 2018 18:47:49 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>TennisPoint</title>


    <link type="text/css" href="assets/css/vendor-bootstrap-datatables.css" rel="stylesheet">

    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <!-- App CSS -->
    <link type="text/css" href="assets/css/app.css" rel="stylesheet">
    <link type="text/css" href="assets/css/app.rtl.css" rel="stylesheet">

    <!-- Simplebar -->
    <link type="text/css" href="assets/vendor/simplebar.css" rel="stylesheet">

</head>

<body>
    <div class="mdk-drawer-layout js-mdk-drawer-layout" data-fullbleed data-push data-responsive-width="992px" data-has-scrolling-region>

        <div class="mdk-drawer-layout__content">
            <!-- header-layout -->
            <div class="mdk-header-layout js-mdk-header-layout  mdk-header--fixed  mdk-header-layout__content--scrollable">
            <?php include("header.php"); ?>
                <!-- content -->
                <div class="mdk-header-layout__content top-navbar mdk-header-layout__content--scrollable h-100">
                    <!-- main content -->
                    <div class="container-fluid">
                        <h2>Our Products here</h2>
                        <p class="lead">
                            For single and twin
                        </p>
                        <hr>
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Articles</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-dark">
                                        <thead>
                                            <tr>
                                                <th scope="col" class="text-center">Name</th>
                                                <th scope="col" class="text-center">E-Mail</th>
                                                <th scope="col" class="text-center">Mode</th>
                                                <th scope="col" class="text-center">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $nb = sizeof($user);
                                        for($i=0; $i<$nb; $i++){ ?>
                                            <tr>
                                                <th class="align-middle text-center" scope="row"><?php echo $user[$i]->name; ?></th>
                                                <td class="align-middle text-center"><?php echo $user[$i]->email; ?></td>
                                                <td class="align-middle text-center"><?php echo $user[$i]->mode; ?></td>
                                                <td class="align-middle text-center"><?php echo $user[$i]->status; ?></td>
                                                <td class="align-middle text-center">
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- // END drawer-layout__content -->

        <?php include("drawer.php"); ?>

        <!-- drawer -->
        <div class="mdk-drawer js-mdk-drawer" id="user-drawer" data-position="right" data-align="end">
            <div class="mdk-drawer__content">
                <div class="mdk-drawer__inner" data-simplebar data-simplebar-force-enabled="true">
                    <nav class="drawer drawer--light">
                        <div class="drawer-spacer drawer-spacer-border">
                            <div class="media align-items-center">
                                <img src="../../../pbs.twimg.com/profile_images/928893978266697728/3enwe0fO_400x400.jpg" class="img-fluid rounded-circle mr-2" width="35" alt="">
                                <div class="media-body">
                                    <a href="#" class="h5 m-0">Frontted</a>
                                    <div>Account Manager</div>
                                </div>
                            </div>
                        </div>
                        <div class="drawer-spacer bg-body-bg">
                            <div class="d-flex justify-content-between mb-2">
                                <p class="h6 text-gray m-0"><i class="material-icons align-middle md-18 text-primary">monetization_on</i> Balance</p>
                                <span>$21,011</span>
                            </div>
                            <div class="d-flex justify-content-between">
                                <p class="h6 text-gray m-0"><i class="material-icons align-middle md-18 text-primary">shopping_cart</i> Sales</p>
                                <span>142</span>
                            </div>
                        </div>
                        <!-- MENU -->
                        <ul class="drawer-menu" id="userMenu" data-children=".drawer-submenu">
                            <li class="drawer-menu-item">
                                <a href="account.html">
        <i class="material-icons">lock</i>
        <span class="drawer-menu-text"> Account</span>
      </a>
                            </li>
                            <li class="drawer-menu-item">
                                <a href="profile.html">
        <i class="material-icons">account_circle</i>
        <span class="drawer-menu-text"> Profile</span>
      </a>
                            </li>
                            <li class="drawer-menu-item">
                                <a href="login.html">
        <i class="material-icons">exit_to_app</i>
        <span class="drawer-menu-text"> Logout</span>
      </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- // END drawer -->

    </div>
    <!-- // END drawer-layout -->



    <!-- jQuery -->
    <script src="assets/vendor/jquery.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/vendor/popper.js"></script>
    <script src="assets/vendor/bootstrap.min.js"></script>

    <!-- Simplebar -->
    <!-- Used for adding a custom scrollbar to the drawer -->
    <script src="assets/vendor/simplebar.js"></script>


    <!-- Vendor -->
    <script src="assets/vendor/Chart.min.js"></script>
    <script src="assets/vendor/moment.min.js"></script>

    <!-- APP -->
    <script src="assets/js/color_variables.js"></script>
    <script src="assets/js/app.js"></script>


    <script src="assets/vendor/dom-factory.js"></script>
    <!-- DOM Factory -->
    <script src="assets/vendor/material-design-kit.js"></script>
    <!-- MDK -->



    <script>
        (function() {
            'use strict';
            // Self Initialize DOM Factory Components
            domFactory.handler.autoInit()


            // Connect button(s) to drawer(s)
            var sidebarToggle = document.querySelectorAll('[data-toggle="sidebar"]')

            sidebarToggle.forEach(function(toggle) {
                toggle.addEventListener('click', function(e) {
                    var selector = e.currentTarget.getAttribute('data-target') || '#default-drawer'
                    var drawer = document.querySelector(selector)
                    if (drawer) {
                        if (selector == '#default-drawer') {
                            $('.container-fluid').toggleClass('container--max');
                        }
                        drawer.mdkDrawer.toggle();
                    }
                })
            })
        })()
    </script>


    <script src="assets/vendor/jquery.dataTables.js"></script>
    <script src="assets/vendor/dataTables.bootstrap4.js"></script>

    <script>
        $('#data-table').dataTable();
    </script>


</body>


<!-- Mirrored from demo.frontted.com/hero/260320180000/ui-tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Apr 2018 18:47:55 GMT -->
</html>