<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en-US"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en-US"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en-US"> <!--<![endif]-->
<html class="no-js" lang="en-US">

<!-- Mirrored from dahz.daffyhazan.com/applique/menswear/category/my-story/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 11:11:11 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<?php include("header.php") ?>
<body class="archive category category-my-story category-24 df-skin-bold unknown windows glob-bw" dir="ltr" itemscope="itemscope" itemtype="http://schema.org/NewsArticle">
<div class="ajax_loader">
<div class="ajax_loader_1">
<div class="double_pulse"><div class="double-bounce1" style="background-color:#1d1c1b"></div><div class="double-bounce2" style="background-color:#1d1c1b"></div></div>
</div>
</div>
<div id="wrapper" class="df-wrapper">
<div class="df-mobile-menu">
<div class="inner-wrapper container">
<div class="df-ham-menu">
<div class="col-left">
<a href="#">
<span class="df-top"></span>
<span class="df-middle"></span>
<span class="df-bottom"></span>
</a>
</div>
<div class="col-right">
<a href="#" class="mobile-subs"><i class="ion-ios-email-outline"></i></a>
<a href="#" class="mobile-search"><i class="ion-ios-search-strong"></i></a>
</div>
</div>
<div class="df-menu-content">
<div class="content-wrap">
<div class="main-navigation" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<div class="nav-wrapper-inner ">
<div class="sticky-logo">
<a href="../../index.html" class="df-sitename" title="Applique Men&#039;s Wear" itemprop="headline">
<img src="../../wp-content/uploads/sites/26/2016/02/menswear-sticky.png" alt="Applique Men&#039;s Wear">
</a>
</div>
<?php include("util.php") ?>
<div class="sticky-btp">
<a class="scroll-top"><i class="ion-ios-arrow-thin-up"></i><i class="ion-ios-arrow-thin-up"></i></a>
</div>
</div>
</div>
<div class="df-social-connect"><a class="df-facebook" href="#" target="_blank"><i class="fa fa-facebook"></i><span class="social-text">Facebook</span></a><a class="df-twitter" href="#" target="_blank"><i class="fa fa-twitter"></i><span class="social-text">Twitter</span></a><a class="df-instagram" href="#" target="_blank"><i class="fa fa-instagram"></i><span class="social-text">Instagram</span></a><a class="df-pinterest" href="#" target="_blank"><i class="fa fa-pinterest"></i><span class="social-text">pinterest</span></a><a class="df-bloglovin" href="#" target="_blank"><i class="fa fa-heart"></i><span class="social-text">Bloglovin</span></a><a class="df-gplus" href="#" target="_blank"><i class="fa fa-google-plus"></i><span class="social-text">Google+</span></a></div> </div>
</div>
</div>
</div>
<div id="masthead" role="banner" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
<div class="df-header-inner">
<div id="branding" class="site-branding border-bottom aligncenter">
<div class="container">
<a href="../../index.html" class="df-sitename" title="Applique Men&#039;s Wear" id="site-title" itemprop="headline">
<img src="../../wp-content/uploads/sites/26/2016/02/menswear-logo.png" alt="Applique Men&#039;s Wear">
</a>
</div>
</div>
<div class="main-navigation" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<div class="nav-wrapper-inner ">
<div class="sticky-logo">
<a href="../../index.html" class="df-sitename" title="Applique Men&#039;s Wear" itemprop="headline">
<img src="../../wp-content/uploads/sites/26/2016/02/menswear-sticky.png" alt="Applique Men&#039;s Wear">
</a>
</div>
<?php include("menus.php") ?>
<div class="sticky-btp">
<a class="scroll-top"><i class="ion-ios-arrow-thin-up"></i><i class="ion-ios-arrow-thin-up"></i></a>
</div>
</div>
</div>
</div>
</div>
<div class="df-header-title aligncenter " style="background-color: #FFFFFF;"><div class="container"><div class="df-header"><span>Browsing Categories</span><h1 class="entry-title display-1" itemprop="headline">My Story</h1></div></div></div>
<div id="content-wrap">
<div class="container main-sidebar-container">
<div class="row">
<div id="df-content" class="df-content col-md-12 df-no-sidebar" role="main">
<div class="row fit_2_col">
<div id="post-326" class="post-326 post type-post status-publish format-standard has-post-thumbnail hentry category-my-story tag-blogger tag-fashion col-md-6" itemscope itemtype="http://schema.org/NewsArticle" itemprop="blogPost" role="article">
<div class="df-post-wrapper">
<div class="clear"></div>
<div class="featured-media  filter_bw"><a href="../../praha-street-style-february-2016/index.html"><img width="600" height="600" src="../../wp-content/uploads/sites/26/2015/12/menswear_06-600x600.jpg" class="attachment-loop-blog size-loop-blog wp-post-image" alt="" srcset="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06-600x600.jpg 600w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06-150x150.jpg 150w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06-250x250.jpg 250w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06-80x80.jpg 80w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06-330x330.jpg 330w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06-160x160.jpg 160w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06-293x293.jpg 293w" sizes="(max-width: 600px) 100vw, 600px" data-attachment-id="540" data-permalink="http://dahz.daffyhazan.com/applique/menswear/praha-street-style-february-2016/menswear_06/" data-orig-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06.jpg" data-orig-size="1200,857" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="menswear_06" data-image-description="" data-medium-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06-300x214.jpg" data-large-file="../../wp-content/uploads/sites/26/2015/12/menswear_06-1024x731.jpg" /></a></div>
<div class="grid-outer">
<div class="grid-wrapper">
<div class="df-post-title  aligncenter" onclick="void(0)"><div class="df-post-title-inner"><div class="df-single-category"><span class="entry-terms category" itemprop="articleSection"><a href="index.html" rel="category tag">My Story</a></span></div><h2 class="entry-title " itemprop="headline"><a href="../../praha-street-style-february-2016/index.html" title="Praha Street Style February 2016">Praha Street Style February 2016</a></h2><div class="df-post-on"><time class="entry-published updated" datetime="2015-12-11T07:12:00+00:00" title="Friday, December 11, 2015, 7:12 am"><a href="../../2015/12/11/index.html"><meta itemprop="datePublished" content="December 11, 2015">December 11, 2015</a></time></div></div></div>
<div class="entry-summary" itemprop="description">
<p>Applique is a fully responsive Fashion blog theme that designed with style publishers in mind! We keep things simple and elegant to make sure any fashion blogger—even those without IT background, can use it. We believe that a blog theme, should feel fluid, light, and intuitive. That’s what we are aiming to make with applique, &hellip; <a class="more-link" href="../../praha-street-style-february-2016/index.html">Continue Reading<i class="ion-ios-arrow-thin-right"></i></a></p>
</div>
</div>
<div class="clear"></div>
<div class="df-postmeta-wrapper"><div class="df-postmeta border-top"><div class="clear"></div><div class="col-left alignleft"><span itemtype="http://schema.org/Comment" itemscope="itemscope" itemprop="comment"><a class="comment-permalink perma" href="../../praha-street-style-february-2016/index.html#respond" itemprop="url">No Comments</a></span></div><div class="col-right alignright"><ul class="df-share"><li><span>Share</span></li><li><a class="df-facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http://dahz.daffyhazan.com/applique/menswear/praha-street-style-february-2016/"><i class="fa fa-facebook"></i><i class="fa fa-facebook"></i></a></li><li><a class="df-twitter" target="_blank" href="https://twitter.com/home?status=Check%20out%20this%20article:%20Praha+Street+Style+February+2016%20-%20http://dahz.daffyhazan.com/applique/menswear/praha-street-style-february-2016/"><i class="fa fa-twitter"></i><i class="fa fa-twitter"></i></a></li><li><a class="df-google" target="_blank" href="https://plus.google.com/share?url=http://dahz.daffyhazan.com/applique/menswear/praha-street-style-february-2016/"><i class="fa fa-google-plus"></i><i class="fa fa-google-plus"></i></a></li><li><a class="df-pinterest" target="_blank" href="https://pinterest.com/pin/create/button/?url=http://dahz.daffyhazan.com/applique/menswear/praha-street-style-february-2016/&amp;media=http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06.jpg&amp;description=Praha+Street+Style+February+2016"><i class="fa fa-pinterest"></i><i class="fa fa-pinterest"></i></a></li></ul></div><div class="clear"></div></div><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="8px" viewBox="550 5 100 8">
<g>
<polygon fill="#231F20" points="24.629,8.174 30.374,13 30.925,13 24.629,7.711   " />
<polygon fill="#231F20" points="26.034,5 35.558,13 36.108,13 26.586,5   " />
<polygon fill="#231F20" points="31.415,5 40.94,13 41.489,13 31.966,5    " />
<polygon fill="#231F20" points="36.465,5 45.801,12.842 46.231,12.741 37.017,5   " />
<polygon fill="#231F20" points="48.755,10.627 42.058,5 41.506,5 48.325,10.728   " />
<polygon fill="#231F20" points="51.237,8.441 47.144,5 46.592,5 50.808,8.542     " />
<polygon fill="#231F20" points="53.657,6.223 52.202,5 51.649,5 53.228,6.324     " />
<polygon fill="#231F20" points="55.733,5 46.849,13 47.392,13 56.276,5   " />
<polygon fill="#231F20" points="60.874,5 51.987,13 52.532,13 61.417,5   " />
<polygon fill="#231F20" points="66.455,5 66.015,5 57.128,13 57.671,13 66.536,5.018  " />
<polygon fill="#231F20" points="68.174,7.684 62.269,13 62.812,13 68.174,8.172 68.174,8.174 73.919,13 74.47,13 68.174,7.711  " />
<polygon fill="#231F20" points="24.629,11.547 26.358,13 26.909,13 24.629,11.085     " />
<polygon fill="#231F20" points="68.174,11.025 65.979,13 66.522,13 68.174,11.514 68.174,11.547 69.903,13 70.454,13

                            68.174,11.085   " />
<polygon fill="#231F20" points="69.579,5 79.103,13 79.653,13 70.131,5   " />
<polygon fill="#231F20" points="74.96,5 84.485,13 85.035,13 75.511,5    " />
<polygon fill="#231F20" points="80.01,5 89.346,12.842 89.777,12.741 80.562,5    " />
<polygon fill="#231F20" points="92.3,10.627 85.603,5 85.051,5 91.87,10.728  " />
<polygon fill="#231F20" points="94.782,8.441 90.688,5 90.137,5 94.353,8.542     " />
<polygon fill="#231F20" points="97.202,6.223 95.747,5 95.194,5 96.772,6.324     " />
<polygon fill="#231F20" points="99.278,5 90.395,13 90.937,13 99.821,5   " />
<polygon fill="#231F20" points="104.419,5 95.532,13 96.077,13 104.962,5     " />
<polygon fill="#231F20" points="110,5 109.56,5 100.673,13 101.216,13 110.081,5.018  " />
<polygon fill="#231F20" points="111.719,7.684 105.813,13 106.356,13 111.719,8.172 111.719,8.174 117.464,13 118.015,13

                            111.719,7.711   " />
<polygon fill="#231F20" points="111.719,11.025 109.524,13 110.067,13 111.719,11.514 111.719,11.547 113.448,13 113.999,13

                            111.719,11.085  " />
<polygon fill="#231F20" points="113.124,5 122.647,13 123.198,13 113.676,5   " />
<polygon fill="#231F20" points="118.505,5 128.03,13 128.58,13 119.056,5     " />
<polygon fill="#231F20" points="123.555,5 132.891,12.842 133.322,12.741 124.106,5   " />
<polygon fill="#231F20" points="135.845,10.627 129.147,5 128.596,5 135.415,10.728   " />
<polygon fill="#231F20" points="138.327,8.441 134.233,5 133.682,5 137.897,8.542     " />
<polygon fill="#231F20" points="140.747,6.223 139.292,5 138.739,5 140.317,6.324     " />
<polygon fill="#231F20" points="142.823,5 133.939,13 134.481,13 143.366,5   " />
<polygon fill="#231F20" points="147.964,5 139.077,13 139.622,13 148.507,5   " />
<polygon fill="#231F20" points="153.545,5 153.104,5 144.218,13 144.761,13 153.626,5.018     " />
<polygon fill="#231F20" points="155.264,7.684 149.358,13 149.901,13 155.264,8.172 155.264,8.174 161.009,13 161.56,13

                            155.264,7.711   " />
<polygon fill="#231F20" points="155.264,11.025 153.069,13 153.612,13 155.264,11.514 155.264,11.547 156.993,13 157.544,13

                            155.264,11.085  " />
<polygon fill="#231F20" points="156.669,5 166.192,13 166.743,13 157.221,5   " />
<polygon fill="#231F20" points="162.05,5 171.575,13 172.125,13 162.601,5    " />
<polygon fill="#231F20" points="167.1,5 176.436,12.842 176.867,12.741 167.651,5     " />
<polygon fill="#231F20" points="179.39,10.627 172.692,5 172.141,5 178.96,10.728     " />
<polygon fill="#231F20" points="181.872,8.441 177.778,5 177.227,5 181.442,8.542     " />
<polygon fill="#231F20" points="184.292,6.223 182.837,5 182.284,5 183.862,6.324     " />
<polygon fill="#231F20" points="186.368,5 177.484,13 178.026,13 186.911,5   " />
<polygon fill="#231F20" points="191.509,5 182.622,13 183.167,13 192.052,5   " />
<polygon fill="#231F20" points="197.09,5 196.649,5 187.763,13 188.306,13 197.171,5.018  " />
<polygon fill="#231F20" points="198.809,7.684 192.903,13 193.446,13 198.809,8.172 198.809,8.174 204.554,13 205.104,13

                            198.809,7.711   " />
<polygon fill="#231F20" points="198.809,11.025 196.614,13 197.157,13 198.809,11.514 198.809,11.547 200.538,13 201.089,13

                            198.809,11.085  " />
<polygon fill="#231F20" points="200.214,5 209.737,13 210.288,13 200.766,5   " />
<polygon fill="#231F20" points="205.595,5 215.12,13 215.67,13 206.146,5     " />
<polygon fill="#231F20" points="210.645,5 219.98,12.842 220.412,12.741 211.196,5    " />
<polygon fill="#231F20" points="222.935,10.627 216.237,5 215.686,5 222.505,10.728   " />
<polygon fill="#231F20" points="225.417,8.441 221.323,5 220.771,5 224.987,8.542     " />
<polygon fill="#231F20" points="227.837,6.223 226.382,5 225.829,5 227.407,6.324     " />
<polygon fill="#231F20" points="229.913,5 221.029,13 221.571,13 230.456,5   " />
<polygon fill="#231F20" points="235.054,5 226.167,13 226.712,13 235.597,5   " />
<polygon fill="#231F20" points="240.635,5 240.194,5 231.308,13 231.851,13 240.716,5.018     " />
<polygon fill="#231F20" points="242.354,7.684 236.448,13 236.991,13 242.354,8.172 242.354,8.174 248.099,13 248.649,13

                            242.354,7.711   " />
<polygon fill="#231F20" points="242.354,11.025 240.159,13 240.702,13 242.354,11.514 242.354,11.547 244.083,13 244.634,13

                            242.354,11.085  " />
<polygon fill="#231F20" points="243.759,5 253.282,13 253.833,13 244.311,5   " />
<polygon fill="#231F20" points="249.14,5 258.665,13 259.215,13 249.69,5     " />
<polygon fill="#231F20" points="254.189,5 263.525,12.842 263.957,12.741 254.741,5   " />
<polygon fill="#231F20" points="266.479,10.627 259.782,5 259.23,5 266.05,10.728     " />
<polygon fill="#231F20" points="268.962,8.441 264.868,5 264.316,5 268.532,8.542     " />
<polygon fill="#231F20" points="271.382,6.223 269.927,5 269.374,5 270.952,6.324     " />
<polygon fill="#231F20" points="273.458,5 264.574,13 265.116,13 274.001,5   " />
<polygon fill="#231F20" points="278.599,5 269.712,13 270.257,13 279.142,5   " />
<polygon fill="#231F20" points="284.18,5 283.739,5 274.853,13 275.396,13 284.261,5.018  " />
<polygon fill="#231F20" points="285.898,7.684 279.993,13 280.536,13 285.898,8.172 285.898,8.174 291.644,13 292.194,13

                            285.898,7.711   " />
<polygon fill="#231F20" points="285.898,11.025 283.704,13 284.247,13 285.898,11.514 285.898,11.547 287.628,13 288.179,13

                            285.898,11.085  " />
<polygon fill="#231F20" points="287.304,5 296.827,13 297.378,13 287.855,5   " />
<polygon fill="#231F20" points="292.685,5 302.21,13 302.76,13 293.235,5     " />
<polygon fill="#231F20" points="297.734,5 307.07,12.842 307.502,12.741 298.286,5    " />
<polygon fill="#231F20" points="310.024,10.627 303.327,5 302.775,5 309.595,10.728   " />
<polygon fill="#231F20" points="312.507,8.441 308.413,5 307.861,5 312.077,8.542     " />
<polygon fill="#231F20" points="314.927,6.223 313.472,5 312.919,5 314.497,6.324     " />
<polygon fill="#231F20" points="317.003,5 308.119,13 308.661,13 317.546,5   " />
<polygon fill="#231F20" points="322.144,5 313.257,13 313.802,13 322.687,5   " />
<polygon fill="#231F20" points="327.725,5 327.284,5 318.397,13 318.94,13 327.806,5.018  " />
<polygon fill="#231F20" points="329.443,7.684 323.538,13 324.081,13 329.443,8.172 329.443,8.174 335.188,13 335.739,13

                            329.443,7.711   " />
<polygon fill="#231F20" points="329.443,11.025 327.249,13 327.792,13 329.443,11.514 329.443,11.547 331.173,13 331.724,13

                            329.443,11.085  " />
<polygon fill="#231F20" points="330.849,5 340.372,13 340.923,13 331.4,5     " />
<polygon fill="#231F20" points="336.229,5 345.755,13 346.305,13 336.78,5    " />
<polygon fill="#231F20" points="341.279,5 350.615,12.842 351.047,12.741 341.831,5   " />
<polygon fill="#231F20" points="353.569,10.627 346.872,5 346.32,5 353.14,10.728     " />
<polygon fill="#231F20" points="356.052,8.441 351.958,5 351.406,5 355.622,8.542     " />
<polygon fill="#231F20" points="358.472,6.223 357.017,5 356.464,5 358.042,6.324     " />
<polygon fill="#231F20" points="360.548,5 351.664,13 352.206,13 361.091,5   " />
<polygon fill="#231F20" points="365.688,5 356.802,13 357.347,13 366.231,5   " />
<polygon fill="#231F20" points="371.27,5 370.829,5 361.942,13 362.485,13 371.351,5.018  " />
<polygon fill="#231F20" points="372.988,7.684 367.083,13 367.626,13 372.988,8.172 372.988,8.174 378.733,13 379.284,13

                            372.988,7.711   " />
<polygon fill="#231F20" points="372.988,11.025 370.794,13 371.337,13 372.988,11.514 372.988,11.547 374.718,13 375.269,13

                            372.988,11.085  " />
<polygon fill="#231F20" points="374.394,5 383.917,13 384.468,13 374.945,5   " />
<polygon fill="#231F20" points="379.774,5 389.3,13 389.85,13 380.325,5  " />
<polygon fill="#231F20" points="384.824,5 394.16,12.842 394.592,12.741 385.376,5    " />
<polygon fill="#231F20" points="397.114,10.627 390.417,5 389.865,5 396.685,10.728   " />
<polygon fill="#231F20" points="399.597,8.441 395.503,5 394.951,5 399.167,8.542     " />
<polygon fill="#231F20" points="402.017,6.223 400.562,5 400.009,5 401.587,6.324     " />
<polygon fill="#231F20" points="404.093,5 395.209,13 395.751,13 404.636,5   " />
<polygon fill="#231F20" points="409.233,5 400.347,13 400.892,13 409.776,5   " />
<polygon fill="#231F20" points="414.814,5 414.374,5 405.487,13 406.03,13 414.896,5.018  " />
<polygon fill="#231F20" points="416.533,7.684 410.628,13 411.171,13 416.533,8.172 416.533,8.174 422.278,13 422.829,13

                            416.533,7.711   " />
<polygon fill="#231F20" points="416.533,11.025 414.339,13 414.882,13 416.533,11.514 416.533,11.547 418.263,13 418.813,13

                            416.533,11.085  " />
<polygon fill="#231F20" points="417.938,5 427.462,13 428.013,13 418.49,5    " />
<polygon fill="#231F20" points="423.319,5 432.845,13 433.395,13 423.87,5    " />
<polygon fill="#231F20" points="428.369,5 437.705,12.842 438.137,12.741 428.921,5   " />
<polygon fill="#231F20" points="440.659,10.627 433.962,5 433.41,5 440.229,10.728    " />
<polygon fill="#231F20" points="443.142,8.441 439.048,5 438.496,5 442.712,8.542     " />
<polygon fill="#231F20" points="445.562,6.223 444.106,5 443.554,5 445.132,6.324     " />
<polygon fill="#231F20" points="447.638,5 438.754,13 439.296,13 448.181,5   " />
<polygon fill="#231F20" points="452.778,5 443.892,13 444.437,13 453.321,5   " />
<polygon fill="#231F20" points="458.359,5 457.919,5 449.032,13 449.575,13 458.44,5.018  " />
<polygon fill="#231F20" points="460.078,7.684 454.173,13 454.716,13 460.078,8.172 460.078,8.174 465.823,13 466.374,13

                            460.078,7.711   " />
<polygon fill="#231F20" points="460.078,11.025 457.884,13 458.427,13 460.078,11.514 460.078,11.547 461.808,13 462.358,13

                            460.078,11.085  " />
<polygon fill="#231F20" points="461.483,5 471.007,13 471.558,13 462.035,5   " />
<polygon fill="#231F20" points="466.864,5 476.39,13 476.939,13 467.415,5    " />
<polygon fill="#231F20" points="471.914,5 481.25,12.842 481.682,12.741 472.466,5    " />
<polygon fill="#231F20" points="484.204,10.627 477.507,5 476.955,5 483.774,10.728   " />
<polygon fill="#231F20" points="486.687,8.441 482.593,5 482.041,5 486.257,8.542     " />
<polygon fill="#231F20" points="489.106,6.223 487.651,5 487.099,5 488.677,6.324     " />
<polygon fill="#231F20" points="491.183,5 482.299,13 482.841,13 491.726,5   " />
<polygon fill="#231F20" points="496.323,5 487.437,13 487.981,13 496.866,5   " />
<polygon fill="#231F20" points="501.904,5 501.464,5 492.577,13 493.12,13 501.985,5.018  " />
<polygon fill="#231F20" points="503.623,7.684 497.718,13 498.261,13 503.623,8.172 503.623,8.174 509.368,13 509.919,13

                            503.623,7.711   " />
<polygon fill="#231F20" points="503.623,11.025 501.429,13 501.972,13 503.623,11.514 503.623,11.547 505.353,13 505.903,13

                            503.623,11.085  " />
<polygon fill="#231F20" points="505.028,5 514.552,13 515.103,13 505.58,5    " />
<polygon fill="#231F20" points="510.409,5 519.935,13 520.484,13 510.96,5    " />
<polygon fill="#231F20" points="515.459,5 524.795,12.842 525.227,12.741 516.011,5   " />
<polygon fill="#231F20" points="527.749,10.627 521.052,5 520.5,5 527.319,10.728     " />
<polygon fill="#231F20" points="530.231,8.441 526.138,5 525.586,5 529.802,8.542     " />
<polygon fill="#231F20" points="532.651,6.223 531.196,5 530.644,5 532.222,6.324     " />
<polygon fill="#231F20" points="534.728,5 525.844,13 526.386,13 535.271,5   " />
<polygon fill="#231F20" points="539.868,5 530.981,13 531.526,13 540.411,5   " />
<polygon fill="#231F20" points="545.449,5 545.009,5 536.122,13 536.665,13 545.53,5.018  " />
<polygon fill="#231F20" points="547.168,7.684 541.263,13 541.806,13 547.168,8.172 547.168,8.174 552.913,13 553.464,13

                            547.168,7.711   " />
<polygon fill="#231F20" points="547.168,11.025 544.974,13 545.517,13 547.168,11.514 547.168,11.547 548.897,13 549.448,13

                            547.168,11.085  " />
<polygon fill="#231F20" points="548.573,5 558.097,13 558.647,13 549.125,5   " />
<polygon fill="#231F20" points="553.954,5 563.479,13 564.029,13 554.505,5   " />
<polygon fill="#231F20" points="559.004,5 568.34,12.842 568.771,12.741 559.556,5    " />
<polygon fill="#231F20" points="571.294,10.627 564.597,5 564.045,5 570.864,10.728   " />
<polygon fill="#231F20" points="573.776,8.441 569.683,5 569.131,5 573.347,8.542     " />
<polygon fill="#231F20" points="576.196,6.223 574.741,5 574.188,5 575.767,6.324     " />
<polygon fill="#231F20" points="578.272,5 569.389,13 569.931,13 578.815,5   " />
<polygon fill="#231F20" points="583.413,5 574.526,13 575.071,13 583.956,5   " />
<polygon fill="#231F20" points="588.994,5 588.554,5 579.667,13 580.21,13 589.075,5.018  " />
<polygon fill="#231F20" points="590.713,7.684 584.808,13 585.351,13 590.713,8.172 590.713,8.174 596.458,13 597.009,13

                            590.713,7.711   " />
<polygon fill="#231F20" points="590.713,11.025 588.519,13 589.062,13 590.713,11.514 590.713,11.547 592.442,13 592.993,13

                            590.713,11.085  " />
<polygon fill="#231F20" points="592.118,5 601.642,13 602.192,13 592.67,5    " />
<polygon fill="#231F20" points="597.499,5 607.024,13 607.574,13 598.05,5    " />
<polygon fill="#231F20" points="602.549,5 611.885,12.842 612.316,12.741 603.101,5   " />
<polygon fill="#231F20" points="614.839,10.627 608.142,5 607.59,5 614.409,10.728    " />
<polygon fill="#231F20" points="617.321,8.441 613.228,5 612.676,5 616.892,8.542     " />
<polygon fill="#231F20" points="619.741,6.223 618.286,5 617.733,5 619.312,6.324     " />
<polygon fill="#231F20" points="621.817,5 612.934,13 613.476,13 622.36,5    " />
<polygon fill="#231F20" points="626.958,5 618.071,13 618.616,13 627.501,5   " />
<polygon fill="#231F20" points="632.539,5 632.099,5 623.212,13 623.755,13 632.62,5.018  " />
<polygon fill="#231F20" points="634.258,7.684 628.353,13 628.896,13 634.258,8.172 634.258,8.174 640.003,13 640.554,13

                            634.258,7.711   " />
<polygon fill="#231F20" points="634.258,11.025 632.063,13 632.606,13 634.258,11.514 634.258,11.547 635.987,13 636.538,13

                            634.258,11.085  " />
<polygon fill="#231F20" points="635.663,5 645.187,13 645.737,13 636.215,5   " />
<polygon fill="#231F20" points="641.044,5 650.569,13 651.119,13 641.595,5   " />
<polygon fill="#231F20" points="646.094,5 655.43,12.842 655.861,12.741 646.646,5    " />
<polygon fill="#231F20" points="658.384,10.627 651.687,5 651.135,5 657.954,10.728   " />
<polygon fill="#231F20" points="660.866,8.441 656.772,5 656.221,5 660.437,8.542     " />
<polygon fill="#231F20" points="663.286,6.223 661.831,5 661.278,5 662.856,6.324     " />
<polygon fill="#231F20" points="665.362,5 656.479,13 657.021,13 665.905,5   " />
<polygon fill="#231F20" points="670.503,5 661.616,13 662.161,13 671.046,5   " />
<polygon fill="#231F20" points="676.084,5 675.644,5 666.757,13 667.3,13 676.165,5.018   " />
<polygon fill="#231F20" points="677.803,7.684 671.897,13 672.44,13 677.803,8.172 677.803,8.174 683.548,13 684.099,13

                            677.803,7.711   " />
<polygon fill="#231F20" points="677.803,11.025 675.608,13 676.151,13 677.803,11.514 677.803,11.547 679.532,13 680.083,13

                            677.803,11.085  " />
<polygon fill="#231F20" points="679.208,5 688.731,13 689.282,13 679.76,5    " />
<polygon fill="#231F20" points="684.589,5 694.114,13 694.664,13 685.14,5    " />
<polygon fill="#231F20" points="689.639,5 698.975,12.842 699.406,12.741 690.19,5    " />
<polygon fill="#231F20" points="701.929,10.627 695.231,5 694.68,5 701.499,10.728    " />
<polygon fill="#231F20" points="704.411,8.441 700.317,5 699.766,5 703.981,8.542     " />
<polygon fill="#231F20" points="706.831,6.223 705.376,5 704.823,5 706.401,6.324     " />
<polygon fill="#231F20" points="708.907,5 700.023,13 700.565,13 709.45,5    " />
<polygon fill="#231F20" points="714.048,5 705.161,13 705.706,13 714.591,5   " />
<polygon fill="#231F20" points="719.629,5 719.188,5 710.302,13 710.845,13 719.71,5.018  " />
<polygon fill="#231F20" points="721.348,7.684 715.442,13 715.985,13 721.348,8.172 721.348,8.174 727.093,13 727.644,13

                            721.348,7.711   " />
<polygon fill="#231F20" points="721.348,11.025 719.153,13 719.696,13 721.348,11.514 721.348,11.547 723.077,13 723.628,13

                            721.348,11.085  " />
<polygon fill="#231F20" points="722.753,5 732.276,13 732.827,13 723.305,5   " />
<polygon fill="#231F20" points="728.134,5 737.659,13 738.209,13 728.685,5   " />
<polygon fill="#231F20" points="733.184,5 742.52,12.842 742.951,12.741 733.735,5    " />
<polygon fill="#231F20" points="745.474,10.627 738.776,5 738.225,5 745.044,10.728   " />
<polygon fill="#231F20" points="747.956,8.441 743.862,5 743.311,5 747.526,8.542     " />
<polygon fill="#231F20" points="750.376,6.223 748.921,5 748.368,5 749.946,6.324     " />
<polygon fill="#231F20" points="752.452,5 743.568,13 744.11,13 752.995,5    " />
<polygon fill="#231F20" points="757.593,5 748.706,13 749.251,13 758.136,5   " />
<polygon fill="#231F20" points="763.174,5 762.733,5 753.847,13 754.39,13 763.255,5.018  " />
<polygon fill="#231F20" points="764.893,7.684 758.987,13 759.53,13 764.893,8.172 764.893,8.174 770.638,13 771.188,13

                            764.893,7.711   " />
<polygon fill="#231F20" points="764.893,11.025 762.698,13 763.241,13 764.893,11.514 764.893,11.547 766.622,13 767.173,13

                            764.893,11.085  " />
<polygon fill="#231F20" points="766.298,5 775.821,13 776.372,13 766.85,5    " />
<polygon fill="#231F20" points="771.679,5 781.204,13 781.754,13 772.229,5   " />
<polygon fill="#231F20" points="776.729,5 786.064,12.842 786.496,12.741 777.28,5    " />
<polygon fill="#231F20" points="789.019,10.627 782.321,5 781.77,5 788.589,10.728    " />
<polygon fill="#231F20" points="791.501,8.441 787.407,5 786.855,5 791.071,8.542     " />
<polygon fill="#231F20" points="793.921,6.223 792.466,5 791.913,5 793.491,6.324     " />
<polygon fill="#231F20" points="795.997,5 787.113,13 787.655,13 796.54,5    " />
<polygon fill="#231F20" points="801.138,5 792.251,13 792.796,13 801.681,5   " />
<polygon fill="#231F20" points="806.719,5 806.278,5 797.392,13 797.935,13 806.8,5.018   " />
<polygon fill="#231F20" points="808.438,7.684 802.532,13 803.075,13 808.438,8.172 808.438,8.174 814.183,13 814.733,13

                            808.438,7.711   " />
<polygon fill="#231F20" points="808.438,11.025 806.243,13 806.786,13 808.438,11.514 808.438,11.547 810.167,13 810.718,13

                            808.438,11.085  " />
<polygon fill="#231F20" points="809.843,5 819.366,13 819.917,13 810.395,5   " />
<polygon fill="#231F20" points="815.224,5 824.749,13 825.299,13 815.774,5   " />
<polygon fill="#231F20" points="820.273,5 829.609,12.842 830.041,12.741 820.825,5   " />
<polygon fill="#231F20" points="832.563,10.627 825.866,5 825.314,5 832.134,10.728   " />
<polygon fill="#231F20" points="835.046,8.441 830.952,5 830.4,5 834.616,8.542   " />
<polygon fill="#231F20" points="837.466,6.223 836.011,5 835.458,5 837.036,6.324     " />
<polygon fill="#231F20" points="839.542,5 830.658,13 831.2,13 840.085,5     " />
<polygon fill="#231F20" points="844.683,5 835.796,13 836.341,13 845.226,5   " />
<polygon fill="#231F20" points="850.264,5 849.823,5 840.937,13 841.479,13 850.345,5.018     " />
<polygon fill="#231F20" points="851.982,7.684 846.077,13 846.62,13 851.982,8.172 851.982,8.174 857.728,13 858.278,13

                            851.982,7.711   " />
<polygon fill="#231F20" points="851.982,11.025 849.788,13 850.331,13 851.982,11.514 851.982,11.547 853.712,13 854.263,13

                            851.982,11.085  " />
<polygon fill="#231F20" points="853.388,5 862.911,13 863.462,13 853.939,5   " />
<polygon fill="#231F20" points="858.769,5 868.294,13 868.844,13 859.319,5   " />
<polygon fill="#231F20" points="863.818,5 873.154,12.842 873.586,12.741 864.37,5    " />
<polygon fill="#231F20" points="876.108,10.627 869.411,5 868.859,5 875.679,10.728   " />
<polygon fill="#231F20" points="878.591,8.441 874.497,5 873.945,5 878.161,8.542     " />
<polygon fill="#231F20" points="881.011,6.223 879.556,5 879.003,5 880.581,6.324     " />
<polygon fill="#231F20" points="883.087,5 874.203,13 874.745,13 883.63,5    " />
<polygon fill="#231F20" points="888.228,5 879.341,13 879.886,13 888.771,5   " />
<polygon fill="#231F20" points="893.809,5 893.368,5 884.481,13 885.024,13 893.89,5.018  " />
<polygon fill="#231F20" points="895.527,7.684 889.622,13 890.165,13 895.527,8.172 895.527,8.174 901.272,13 901.823,13

                            895.527,7.711   " />
<polygon fill="#231F20" points="895.527,11.025 893.333,13 893.876,13 895.527,11.514 895.527,11.547 897.257,13 897.808,13

                            895.527,11.085  " />
<polygon fill="#231F20" points="896.933,5 906.456,13 907.007,13 897.484,5   " />
<polygon fill="#231F20" points="902.313,5 911.839,13 912.389,13 902.864,5   " />
<polygon fill="#231F20" points="907.363,5 916.699,12.842 917.131,12.741 907.915,5   " />
<polygon fill="#231F20" points="919.653,10.627 912.956,5 912.404,5 919.224,10.728   " />
<polygon fill="#231F20" points="922.136,8.441 918.042,5 917.49,5 921.706,8.542  " />
<polygon fill="#231F20" points="924.556,6.223 923.101,5 922.548,5 924.126,6.324     " />
<polygon fill="#231F20" points="926.632,5 917.748,13 918.29,13 927.175,5    " />
<polygon fill="#231F20" points="931.772,5 922.886,13 923.431,13 932.315,5   " />
<polygon fill="#231F20" points="937.354,5 936.913,5 928.026,13 928.569,13 937.435,5.018     " />
<polygon fill="#231F20" points="939.072,7.684 933.167,13 933.71,13 939.072,8.172 939.072,8.174 944.817,13 945.368,13

                            939.072,7.711   " />
<polygon fill="#231F20" points="939.072,11.025 936.878,13 937.421,13 939.072,11.514 939.072,11.547 940.802,13 941.353,13

                            939.072,11.085  " />
<polygon fill="#231F20" points="940.478,5 950.001,13 950.552,13 941.029,5   " />
<polygon fill="#231F20" points="945.858,5 955.384,13 955.934,13 946.409,5   " />
<polygon fill="#231F20" points="950.908,5 960.244,12.842 960.676,12.741 951.46,5    " />
<polygon fill="#231F20" points="963.198,10.627 956.501,5 955.949,5 962.769,10.728   " />
<polygon fill="#231F20" points="965.681,8.441 961.587,5 961.035,5 965.251,8.542     " />
<polygon fill="#231F20" points="968.101,6.223 966.646,5 966.093,5 967.671,6.324     " />
<polygon fill="#231F20" points="970.177,5 961.293,13 961.835,13 970.72,5    " />
<polygon fill="#231F20" points="975.317,5 966.431,13 966.976,13 975.86,5    " />
<polygon fill="#231F20" points="980.898,5 980.458,5 971.571,13 972.114,13 980.979,5.018     " />
<polygon fill="#231F20" points="982.617,7.684 976.712,13 977.255,13 982.617,8.172 982.617,8.174 988.362,13 988.913,13

                            982.617,7.711   " />
<polygon fill="#231F20" points="982.617,11.025 980.423,13 980.966,13 982.617,11.514 982.617,11.547 984.347,13 984.897,13

                            982.617,11.085  " />
<polygon fill="#231F20" points="984.022,5 993.546,13 994.097,13 984.574,5   " />
<polygon fill="#231F20" points="989.403,5 998.929,13 999.479,13 989.954,5   " />
<polygon fill="#231F20" points="994.453,5 1003.789,12.842 1004.221,12.741 995.005,5     " />
<polygon fill="#231F20" points="1006.743,10.627 1000.046,5 999.494,5 1006.313,10.728    " />
<polygon fill="#231F20" points="1009.226,8.441 1005.132,5 1004.58,5 1008.796,8.542  " />
<polygon fill="#231F20" points="1011.646,6.223 1010.19,5 1009.638,5 1011.216,6.324  " />
<polygon fill="#231F20" points="1013.722,5 1004.838,13 1005.38,13 1014.265,5    " />
<polygon fill="#231F20" points="1018.862,5 1009.976,13 1010.521,13 1019.405,5   " />
<polygon fill="#231F20" points="1024.443,5 1024.003,5 1015.116,13 1015.659,13 1024.524,5.018    " />
<polygon fill="#231F20" points="1026.162,7.684 1020.257,13 1020.8,13 1026.162,8.172 1026.162,8.174 1031.907,13 1032.458,13

                            1026.162,7.711  " />
<polygon fill="#231F20" points="1026.162,11.025 1023.968,13 1024.511,13 1026.162,11.514 1026.162,11.547 1027.892,13

                            1028.442,13 1026.162,11.085     " />
<polygon fill="#231F20" points="1027.567,5 1037.091,13 1037.642,13 1028.119,5   " />
<polygon fill="#231F20" points="1032.948,5 1042.474,13 1043.023,13 1033.499,5   " />
<polygon fill="#231F20" points="1037.998,5 1047.334,12.842 1047.766,12.741 1038.55,5    " />
<polygon fill="#231F20" points="1050.288,10.627 1043.591,5 1043.039,5 1049.858,10.728   " />
<polygon fill="#231F20" points="1052.771,8.441 1048.677,5 1048.125,5 1052.341,8.542     " />
<polygon fill="#231F20" points="1055.19,6.223 1053.735,5 1053.183,5 1054.761,6.324  " />
<polygon fill="#231F20" points="1057.267,5 1048.383,13 1048.925,13 1057.81,5    " />
<polygon fill="#231F20" points="1062.407,5 1053.521,13 1054.065,13 1062.95,5    " />
<polygon fill="#231F20" points="1067.988,5 1067.548,5 1058.661,13 1059.204,13 1068.069,5.018    " />
<polygon fill="#231F20" points="1069.707,7.684 1063.802,13 1064.345,13 1069.707,8.172 1069.707,8.174 1075.452,13 1076.003,13

                            1069.707,7.711  " />
<polygon fill="#231F20" points="1069.707,11.025 1067.513,13 1068.056,13 1069.707,11.514 1069.707,11.547 1071.437,13

                            1071.987,13 1069.707,11.085     " />
<polygon fill="#231F20" points="1071.112,5 1080.636,13 1081.187,13 1071.664,5   " />
<polygon fill="#231F20" points="1076.493,5 1086.019,13 1086.568,13 1077.044,5   " />
<polygon fill="#231F20" points="1081.543,5 1090.879,12.842 1091.311,12.741 1082.095,5   " />
<polygon fill="#231F20" points="1093.833,10.627 1087.136,5 1086.584,5 1093.403,10.728   " />
<polygon fill="#231F20" points="1096.315,8.441 1092.222,5 1091.67,5 1095.886,8.542  " />
<polygon fill="#231F20" points="1098.735,6.223 1097.28,5 1096.728,5 1098.306,6.324  " />
<polygon fill="#231F20" points="1100.812,5 1091.928,13 1092.47,13 1101.354,5    " />
<polygon fill="#231F20" points="1105.952,5 1097.065,13 1097.61,13 1106.495,5    " />
<polygon fill="#231F20" points="1111.533,5 1111.093,5 1102.206,13 1102.749,13 1111.614,5.018    " />
<polygon fill="#231F20" points="1113.252,7.684 1107.347,13 1107.89,13 1113.252,8.172 1113.252,8.174 1118.997,13 1119.548,13

                            1113.252,7.711  " />
<polygon fill="#231F20" points="1113.252,11.025 1111.058,13 1111.601,13 1113.252,11.514 1113.252,11.547 1114.981,13

                            1115.532,13 1113.252,11.085     " />
<polygon fill="#231F20" points="1114.657,5 1124.181,13 1124.731,13 1115.209,5   " />
<polygon fill="#231F20" points="1120.038,5 1129.563,13 1130.113,13 1120.589,5   " />
<polygon fill="#231F20" points="1125.088,5 1134.424,12.842 1134.855,12.741 1125.64,5    " />
<polygon fill="#231F20" points="1137.378,10.627 1130.681,5 1130.129,5 1136.948,10.728   " />
<polygon fill="#231F20" points="1139.86,8.441 1135.767,5 1135.215,5 1139.431,8.542  " />
<polygon fill="#231F20" points="1142.28,6.223 1140.825,5 1140.272,5 1141.851,6.324  " />
<polygon fill="#231F20" points="1144.356,5 1135.473,13 1136.015,13 1144.899,5   " />
<polygon fill="#231F20" points="1149.497,5 1140.61,13 1141.155,13 1150.04,5     " />
<polygon fill="#231F20" points="1155.078,5 1154.638,5 1145.751,13 1146.294,13 1155.159,5.018    " />
<polygon fill="#231F20" points="1156.797,7.684 1150.892,13 1151.435,13 1156.797,8.172 1156.797,8.174 1162.542,13 1163.093,13

                            1156.797,7.711  " />
<polygon fill="#231F20" points="1156.797,11.025 1154.603,13 1155.146,13 1156.797,11.514 1156.797,11.547 1158.526,13

                            1159.077,13 1156.797,11.085     " />
<polygon fill="#231F20" points="1158.202,5 1167.726,13 1168.276,13 1158.754,5   " />
<polygon fill="#231F20" points="1163.583,5 1173.108,13 1173.658,13 1164.134,5   " />
<polygon fill="#231F20" points="1168.633,5 1177.969,12.842 1178.4,12.741 1169.185,5     " />
<polygon fill="#231F20" points="1180.923,10.627 1174.226,5 1173.674,5 1180.493,10.728   " />
<polygon fill="#231F20" points="1183.405,8.441 1179.312,5 1178.76,5 1182.976,8.542  " />
<polygon fill="#231F20" points="1185.825,6.223 1184.37,5 1183.817,5 1185.396,6.324  " />
<polygon fill="#231F20" points="1187.901,5 1179.018,13 1179.56,13 1188.444,5    " />
<polygon fill="#231F20" points="1193.042,5 1184.155,13 1184.7,13 1193.585,5     " />
<polygon fill="#231F20" points="1189.296,13 1189.839,13 1194.629,8.688 1194.629,8.199   " />
<polygon fill="#231F20" points="1194.629,13 1194.629,12.827 1194.437,13     " />
</g>
</svg></div>
<div class="clear"></div>
</div>
</div>
</div>
<div id="post-320" class="post-320 post type-post status-publish format-standard has-post-thumbnail hentry category-my-story tag-blogger col-md-6" itemscope itemtype="http://schema.org/NewsArticle" itemprop="blogPost" role="article">
<div class="df-post-wrapper">
<div class="clear"></div>
<div class="featured-media  filter_bw"><a href="../../2-years-old-jeans/index.html"><img width="600" height="600" src="../../wp-content/uploads/sites/26/2015/12/menswear_23-600x600.jpg" class="attachment-loop-blog size-loop-blog wp-post-image" alt="" srcset="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23-600x600.jpg 600w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23-150x150.jpg 150w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23-250x250.jpg 250w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23-80x80.jpg 80w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23-330x330.jpg 330w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23-160x160.jpg 160w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23-293x293.jpg 293w" sizes="(max-width: 600px) 100vw, 600px" data-attachment-id="547" data-permalink="http://dahz.daffyhazan.com/applique/menswear/2-years-old-jeans/menswear_23/" data-orig-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23.jpg" data-orig-size="1200,857" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="menswear_23" data-image-description="" data-medium-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23-300x214.jpg" data-large-file="../../wp-content/uploads/sites/26/2015/12/menswear_23-1024x731.jpg" /></a></div>
<div class="grid-outer">
<div class="grid-wrapper">
<div class="df-post-title  aligncenter" onclick="void(0)"><div class="df-post-title-inner"><div class="df-single-category"><span class="entry-terms category" itemprop="articleSection"><a href="index.html" rel="category tag">My Story</a></span></div><h2 class="entry-title " itemprop="headline"><a href="../../2-years-old-jeans/index.html" title="2 Years Old Jeans">2 Years Old Jeans</a></h2><div class="df-post-on"><time class="entry-published updated" datetime="2015-12-11T07:06:17+00:00" title="Friday, December 11, 2015, 7:06 am"><a href="../../2015/12/11/index.html"><meta itemprop="datePublished" content="December 11, 2015">December 11, 2015</a></time></div></div></div>
<div class="entry-summary" itemprop="description">
<p>Applique is a fully responsive Fashion blog theme that designed with style publishers in mind! We keep things simple and elegant to make sure any fashion blogger—even those without IT background, can use it. We believe that a blog theme, should feel fluid, light, and intuitive. That’s what we are aiming to make with applique, &hellip; <a class="more-link" href="../../2-years-old-jeans/index.html">Continue Reading<i class="ion-ios-arrow-thin-right"></i></a></p>
</div>
</div>
<div class="clear"></div>
<div class="df-postmeta-wrapper"><div class="df-postmeta border-top"><div class="clear"></div><div class="col-left alignleft"><span itemtype="http://schema.org/Comment" itemscope="itemscope" itemprop="comment"><a class="comment-permalink perma" href="../../2-years-old-jeans/index.html#respond" itemprop="url">No Comments</a></span></div><div class="col-right alignright"><ul class="df-share"><li><span>Share</span></li><li><a class="df-facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http://dahz.daffyhazan.com/applique/menswear/2-years-old-jeans/"><i class="fa fa-facebook"></i><i class="fa fa-facebook"></i></a></li><li><a class="df-twitter" target="_blank" href="https://twitter.com/home?status=Check%20out%20this%20article:%202+Years+Old+Jeans%20-%20http://dahz.daffyhazan.com/applique/menswear/2-years-old-jeans/"><i class="fa fa-twitter"></i><i class="fa fa-twitter"></i></a></li><li><a class="df-google" target="_blank" href="https://plus.google.com/share?url=http://dahz.daffyhazan.com/applique/menswear/2-years-old-jeans/"><i class="fa fa-google-plus"></i><i class="fa fa-google-plus"></i></a></li><li><a class="df-pinterest" target="_blank" href="https://pinterest.com/pin/create/button/?url=http://dahz.daffyhazan.com/applique/menswear/2-years-old-jeans/&amp;media=http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23.jpg&amp;description=2+Years+Old+Jeans"><i class="fa fa-pinterest"></i><i class="fa fa-pinterest"></i></a></li></ul></div><div class="clear"></div></div><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="8px" viewBox="550 5 100 8">
<g>
<polygon fill="#231F20" points="24.629,8.174 30.374,13 30.925,13 24.629,7.711   " />
<polygon fill="#231F20" points="26.034,5 35.558,13 36.108,13 26.586,5   " />
<polygon fill="#231F20" points="31.415,5 40.94,13 41.489,13 31.966,5    " />
<polygon fill="#231F20" points="36.465,5 45.801,12.842 46.231,12.741 37.017,5   " />
<polygon fill="#231F20" points="48.755,10.627 42.058,5 41.506,5 48.325,10.728   " />
<polygon fill="#231F20" points="51.237,8.441 47.144,5 46.592,5 50.808,8.542     " />
<polygon fill="#231F20" points="53.657,6.223 52.202,5 51.649,5 53.228,6.324     " />
<polygon fill="#231F20" points="55.733,5 46.849,13 47.392,13 56.276,5   " />
<polygon fill="#231F20" points="60.874,5 51.987,13 52.532,13 61.417,5   " />
<polygon fill="#231F20" points="66.455,5 66.015,5 57.128,13 57.671,13 66.536,5.018  " />
<polygon fill="#231F20" points="68.174,7.684 62.269,13 62.812,13 68.174,8.172 68.174,8.174 73.919,13 74.47,13 68.174,7.711  " />
<polygon fill="#231F20" points="24.629,11.547 26.358,13 26.909,13 24.629,11.085     " />
<polygon fill="#231F20" points="68.174,11.025 65.979,13 66.522,13 68.174,11.514 68.174,11.547 69.903,13 70.454,13

                            68.174,11.085   " />
<polygon fill="#231F20" points="69.579,5 79.103,13 79.653,13 70.131,5   " />
<polygon fill="#231F20" points="74.96,5 84.485,13 85.035,13 75.511,5    " />
<polygon fill="#231F20" points="80.01,5 89.346,12.842 89.777,12.741 80.562,5    " />
<polygon fill="#231F20" points="92.3,10.627 85.603,5 85.051,5 91.87,10.728  " />
<polygon fill="#231F20" points="94.782,8.441 90.688,5 90.137,5 94.353,8.542     " />
<polygon fill="#231F20" points="97.202,6.223 95.747,5 95.194,5 96.772,6.324     " />
<polygon fill="#231F20" points="99.278,5 90.395,13 90.937,13 99.821,5   " />
<polygon fill="#231F20" points="104.419,5 95.532,13 96.077,13 104.962,5     " />
<polygon fill="#231F20" points="110,5 109.56,5 100.673,13 101.216,13 110.081,5.018  " />
<polygon fill="#231F20" points="111.719,7.684 105.813,13 106.356,13 111.719,8.172 111.719,8.174 117.464,13 118.015,13

                            111.719,7.711   " />
<polygon fill="#231F20" points="111.719,11.025 109.524,13 110.067,13 111.719,11.514 111.719,11.547 113.448,13 113.999,13

                            111.719,11.085  " />
<polygon fill="#231F20" points="113.124,5 122.647,13 123.198,13 113.676,5   " />
<polygon fill="#231F20" points="118.505,5 128.03,13 128.58,13 119.056,5     " />
<polygon fill="#231F20" points="123.555,5 132.891,12.842 133.322,12.741 124.106,5   " />
<polygon fill="#231F20" points="135.845,10.627 129.147,5 128.596,5 135.415,10.728   " />
<polygon fill="#231F20" points="138.327,8.441 134.233,5 133.682,5 137.897,8.542     " />
<polygon fill="#231F20" points="140.747,6.223 139.292,5 138.739,5 140.317,6.324     " />
<polygon fill="#231F20" points="142.823,5 133.939,13 134.481,13 143.366,5   " />
<polygon fill="#231F20" points="147.964,5 139.077,13 139.622,13 148.507,5   " />
<polygon fill="#231F20" points="153.545,5 153.104,5 144.218,13 144.761,13 153.626,5.018     " />
<polygon fill="#231F20" points="155.264,7.684 149.358,13 149.901,13 155.264,8.172 155.264,8.174 161.009,13 161.56,13

                            155.264,7.711   " />
<polygon fill="#231F20" points="155.264,11.025 153.069,13 153.612,13 155.264,11.514 155.264,11.547 156.993,13 157.544,13

                            155.264,11.085  " />
<polygon fill="#231F20" points="156.669,5 166.192,13 166.743,13 157.221,5   " />
<polygon fill="#231F20" points="162.05,5 171.575,13 172.125,13 162.601,5    " />
<polygon fill="#231F20" points="167.1,5 176.436,12.842 176.867,12.741 167.651,5     " />
<polygon fill="#231F20" points="179.39,10.627 172.692,5 172.141,5 178.96,10.728     " />
<polygon fill="#231F20" points="181.872,8.441 177.778,5 177.227,5 181.442,8.542     " />
<polygon fill="#231F20" points="184.292,6.223 182.837,5 182.284,5 183.862,6.324     " />
<polygon fill="#231F20" points="186.368,5 177.484,13 178.026,13 186.911,5   " />
<polygon fill="#231F20" points="191.509,5 182.622,13 183.167,13 192.052,5   " />
<polygon fill="#231F20" points="197.09,5 196.649,5 187.763,13 188.306,13 197.171,5.018  " />
<polygon fill="#231F20" points="198.809,7.684 192.903,13 193.446,13 198.809,8.172 198.809,8.174 204.554,13 205.104,13

                            198.809,7.711   " />
<polygon fill="#231F20" points="198.809,11.025 196.614,13 197.157,13 198.809,11.514 198.809,11.547 200.538,13 201.089,13

                            198.809,11.085  " />
<polygon fill="#231F20" points="200.214,5 209.737,13 210.288,13 200.766,5   " />
<polygon fill="#231F20" points="205.595,5 215.12,13 215.67,13 206.146,5     " />
<polygon fill="#231F20" points="210.645,5 219.98,12.842 220.412,12.741 211.196,5    " />
<polygon fill="#231F20" points="222.935,10.627 216.237,5 215.686,5 222.505,10.728   " />
<polygon fill="#231F20" points="225.417,8.441 221.323,5 220.771,5 224.987,8.542     " />
<polygon fill="#231F20" points="227.837,6.223 226.382,5 225.829,5 227.407,6.324     " />
<polygon fill="#231F20" points="229.913,5 221.029,13 221.571,13 230.456,5   " />
<polygon fill="#231F20" points="235.054,5 226.167,13 226.712,13 235.597,5   " />
<polygon fill="#231F20" points="240.635,5 240.194,5 231.308,13 231.851,13 240.716,5.018     " />
<polygon fill="#231F20" points="242.354,7.684 236.448,13 236.991,13 242.354,8.172 242.354,8.174 248.099,13 248.649,13

                            242.354,7.711   " />
<polygon fill="#231F20" points="242.354,11.025 240.159,13 240.702,13 242.354,11.514 242.354,11.547 244.083,13 244.634,13

                            242.354,11.085  " />
<polygon fill="#231F20" points="243.759,5 253.282,13 253.833,13 244.311,5   " />
<polygon fill="#231F20" points="249.14,5 258.665,13 259.215,13 249.69,5     " />
<polygon fill="#231F20" points="254.189,5 263.525,12.842 263.957,12.741 254.741,5   " />
<polygon fill="#231F20" points="266.479,10.627 259.782,5 259.23,5 266.05,10.728     " />
<polygon fill="#231F20" points="268.962,8.441 264.868,5 264.316,5 268.532,8.542     " />
<polygon fill="#231F20" points="271.382,6.223 269.927,5 269.374,5 270.952,6.324     " />
<polygon fill="#231F20" points="273.458,5 264.574,13 265.116,13 274.001,5   " />
<polygon fill="#231F20" points="278.599,5 269.712,13 270.257,13 279.142,5   " />
<polygon fill="#231F20" points="284.18,5 283.739,5 274.853,13 275.396,13 284.261,5.018  " />
<polygon fill="#231F20" points="285.898,7.684 279.993,13 280.536,13 285.898,8.172 285.898,8.174 291.644,13 292.194,13

                            285.898,7.711   " />
<polygon fill="#231F20" points="285.898,11.025 283.704,13 284.247,13 285.898,11.514 285.898,11.547 287.628,13 288.179,13

                            285.898,11.085  " />
<polygon fill="#231F20" points="287.304,5 296.827,13 297.378,13 287.855,5   " />
<polygon fill="#231F20" points="292.685,5 302.21,13 302.76,13 293.235,5     " />
<polygon fill="#231F20" points="297.734,5 307.07,12.842 307.502,12.741 298.286,5    " />
<polygon fill="#231F20" points="310.024,10.627 303.327,5 302.775,5 309.595,10.728   " />
<polygon fill="#231F20" points="312.507,8.441 308.413,5 307.861,5 312.077,8.542     " />
<polygon fill="#231F20" points="314.927,6.223 313.472,5 312.919,5 314.497,6.324     " />
<polygon fill="#231F20" points="317.003,5 308.119,13 308.661,13 317.546,5   " />
<polygon fill="#231F20" points="322.144,5 313.257,13 313.802,13 322.687,5   " />
<polygon fill="#231F20" points="327.725,5 327.284,5 318.397,13 318.94,13 327.806,5.018  " />
<polygon fill="#231F20" points="329.443,7.684 323.538,13 324.081,13 329.443,8.172 329.443,8.174 335.188,13 335.739,13

                            329.443,7.711   " />
<polygon fill="#231F20" points="329.443,11.025 327.249,13 327.792,13 329.443,11.514 329.443,11.547 331.173,13 331.724,13

                            329.443,11.085  " />
<polygon fill="#231F20" points="330.849,5 340.372,13 340.923,13 331.4,5     " />
<polygon fill="#231F20" points="336.229,5 345.755,13 346.305,13 336.78,5    " />
<polygon fill="#231F20" points="341.279,5 350.615,12.842 351.047,12.741 341.831,5   " />
<polygon fill="#231F20" points="353.569,10.627 346.872,5 346.32,5 353.14,10.728     " />
<polygon fill="#231F20" points="356.052,8.441 351.958,5 351.406,5 355.622,8.542     " />
<polygon fill="#231F20" points="358.472,6.223 357.017,5 356.464,5 358.042,6.324     " />
<polygon fill="#231F20" points="360.548,5 351.664,13 352.206,13 361.091,5   " />
<polygon fill="#231F20" points="365.688,5 356.802,13 357.347,13 366.231,5   " />
<polygon fill="#231F20" points="371.27,5 370.829,5 361.942,13 362.485,13 371.351,5.018  " />
<polygon fill="#231F20" points="372.988,7.684 367.083,13 367.626,13 372.988,8.172 372.988,8.174 378.733,13 379.284,13

                            372.988,7.711   " />
<polygon fill="#231F20" points="372.988,11.025 370.794,13 371.337,13 372.988,11.514 372.988,11.547 374.718,13 375.269,13

                            372.988,11.085  " />
<polygon fill="#231F20" points="374.394,5 383.917,13 384.468,13 374.945,5   " />
<polygon fill="#231F20" points="379.774,5 389.3,13 389.85,13 380.325,5  " />
<polygon fill="#231F20" points="384.824,5 394.16,12.842 394.592,12.741 385.376,5    " />
<polygon fill="#231F20" points="397.114,10.627 390.417,5 389.865,5 396.685,10.728   " />
<polygon fill="#231F20" points="399.597,8.441 395.503,5 394.951,5 399.167,8.542     " />
<polygon fill="#231F20" points="402.017,6.223 400.562,5 400.009,5 401.587,6.324     " />
<polygon fill="#231F20" points="404.093,5 395.209,13 395.751,13 404.636,5   " />
<polygon fill="#231F20" points="409.233,5 400.347,13 400.892,13 409.776,5   " />
<polygon fill="#231F20" points="414.814,5 414.374,5 405.487,13 406.03,13 414.896,5.018  " />
<polygon fill="#231F20" points="416.533,7.684 410.628,13 411.171,13 416.533,8.172 416.533,8.174 422.278,13 422.829,13

                            416.533,7.711   " />
<polygon fill="#231F20" points="416.533,11.025 414.339,13 414.882,13 416.533,11.514 416.533,11.547 418.263,13 418.813,13

                            416.533,11.085  " />
<polygon fill="#231F20" points="417.938,5 427.462,13 428.013,13 418.49,5    " />
<polygon fill="#231F20" points="423.319,5 432.845,13 433.395,13 423.87,5    " />
<polygon fill="#231F20" points="428.369,5 437.705,12.842 438.137,12.741 428.921,5   " />
<polygon fill="#231F20" points="440.659,10.627 433.962,5 433.41,5 440.229,10.728    " />
<polygon fill="#231F20" points="443.142,8.441 439.048,5 438.496,5 442.712,8.542     " />
<polygon fill="#231F20" points="445.562,6.223 444.106,5 443.554,5 445.132,6.324     " />
<polygon fill="#231F20" points="447.638,5 438.754,13 439.296,13 448.181,5   " />
<polygon fill="#231F20" points="452.778,5 443.892,13 444.437,13 453.321,5   " />
<polygon fill="#231F20" points="458.359,5 457.919,5 449.032,13 449.575,13 458.44,5.018  " />
<polygon fill="#231F20" points="460.078,7.684 454.173,13 454.716,13 460.078,8.172 460.078,8.174 465.823,13 466.374,13

                            460.078,7.711   " />
<polygon fill="#231F20" points="460.078,11.025 457.884,13 458.427,13 460.078,11.514 460.078,11.547 461.808,13 462.358,13

                            460.078,11.085  " />
<polygon fill="#231F20" points="461.483,5 471.007,13 471.558,13 462.035,5   " />
<polygon fill="#231F20" points="466.864,5 476.39,13 476.939,13 467.415,5    " />
<polygon fill="#231F20" points="471.914,5 481.25,12.842 481.682,12.741 472.466,5    " />
<polygon fill="#231F20" points="484.204,10.627 477.507,5 476.955,5 483.774,10.728   " />
<polygon fill="#231F20" points="486.687,8.441 482.593,5 482.041,5 486.257,8.542     " />
<polygon fill="#231F20" points="489.106,6.223 487.651,5 487.099,5 488.677,6.324     " />
<polygon fill="#231F20" points="491.183,5 482.299,13 482.841,13 491.726,5   " />
<polygon fill="#231F20" points="496.323,5 487.437,13 487.981,13 496.866,5   " />
<polygon fill="#231F20" points="501.904,5 501.464,5 492.577,13 493.12,13 501.985,5.018  " />
<polygon fill="#231F20" points="503.623,7.684 497.718,13 498.261,13 503.623,8.172 503.623,8.174 509.368,13 509.919,13

                            503.623,7.711   " />
<polygon fill="#231F20" points="503.623,11.025 501.429,13 501.972,13 503.623,11.514 503.623,11.547 505.353,13 505.903,13

                            503.623,11.085  " />
<polygon fill="#231F20" points="505.028,5 514.552,13 515.103,13 505.58,5    " />
<polygon fill="#231F20" points="510.409,5 519.935,13 520.484,13 510.96,5    " />
<polygon fill="#231F20" points="515.459,5 524.795,12.842 525.227,12.741 516.011,5   " />
<polygon fill="#231F20" points="527.749,10.627 521.052,5 520.5,5 527.319,10.728     " />
<polygon fill="#231F20" points="530.231,8.441 526.138,5 525.586,5 529.802,8.542     " />
<polygon fill="#231F20" points="532.651,6.223 531.196,5 530.644,5 532.222,6.324     " />
<polygon fill="#231F20" points="534.728,5 525.844,13 526.386,13 535.271,5   " />
<polygon fill="#231F20" points="539.868,5 530.981,13 531.526,13 540.411,5   " />
<polygon fill="#231F20" points="545.449,5 545.009,5 536.122,13 536.665,13 545.53,5.018  " />
<polygon fill="#231F20" points="547.168,7.684 541.263,13 541.806,13 547.168,8.172 547.168,8.174 552.913,13 553.464,13

                            547.168,7.711   " />
<polygon fill="#231F20" points="547.168,11.025 544.974,13 545.517,13 547.168,11.514 547.168,11.547 548.897,13 549.448,13

                            547.168,11.085  " />
<polygon fill="#231F20" points="548.573,5 558.097,13 558.647,13 549.125,5   " />
<polygon fill="#231F20" points="553.954,5 563.479,13 564.029,13 554.505,5   " />
<polygon fill="#231F20" points="559.004,5 568.34,12.842 568.771,12.741 559.556,5    " />
<polygon fill="#231F20" points="571.294,10.627 564.597,5 564.045,5 570.864,10.728   " />
<polygon fill="#231F20" points="573.776,8.441 569.683,5 569.131,5 573.347,8.542     " />
<polygon fill="#231F20" points="576.196,6.223 574.741,5 574.188,5 575.767,6.324     " />
<polygon fill="#231F20" points="578.272,5 569.389,13 569.931,13 578.815,5   " />
<polygon fill="#231F20" points="583.413,5 574.526,13 575.071,13 583.956,5   " />
<polygon fill="#231F20" points="588.994,5 588.554,5 579.667,13 580.21,13 589.075,5.018  " />
<polygon fill="#231F20" points="590.713,7.684 584.808,13 585.351,13 590.713,8.172 590.713,8.174 596.458,13 597.009,13

                            590.713,7.711   " />
<polygon fill="#231F20" points="590.713,11.025 588.519,13 589.062,13 590.713,11.514 590.713,11.547 592.442,13 592.993,13

                            590.713,11.085  " />
<polygon fill="#231F20" points="592.118,5 601.642,13 602.192,13 592.67,5    " />
<polygon fill="#231F20" points="597.499,5 607.024,13 607.574,13 598.05,5    " />
<polygon fill="#231F20" points="602.549,5 611.885,12.842 612.316,12.741 603.101,5   " />
<polygon fill="#231F20" points="614.839,10.627 608.142,5 607.59,5 614.409,10.728    " />
<polygon fill="#231F20" points="617.321,8.441 613.228,5 612.676,5 616.892,8.542     " />
<polygon fill="#231F20" points="619.741,6.223 618.286,5 617.733,5 619.312,6.324     " />
<polygon fill="#231F20" points="621.817,5 612.934,13 613.476,13 622.36,5    " />
<polygon fill="#231F20" points="626.958,5 618.071,13 618.616,13 627.501,5   " />
<polygon fill="#231F20" points="632.539,5 632.099,5 623.212,13 623.755,13 632.62,5.018  " />
<polygon fill="#231F20" points="634.258,7.684 628.353,13 628.896,13 634.258,8.172 634.258,8.174 640.003,13 640.554,13

                            634.258,7.711   " />
<polygon fill="#231F20" points="634.258,11.025 632.063,13 632.606,13 634.258,11.514 634.258,11.547 635.987,13 636.538,13

                            634.258,11.085  " />
<polygon fill="#231F20" points="635.663,5 645.187,13 645.737,13 636.215,5   " />
<polygon fill="#231F20" points="641.044,5 650.569,13 651.119,13 641.595,5   " />
 <polygon fill="#231F20" points="646.094,5 655.43,12.842 655.861,12.741 646.646,5    " />
<polygon fill="#231F20" points="658.384,10.627 651.687,5 651.135,5 657.954,10.728   " />
<polygon fill="#231F20" points="660.866,8.441 656.772,5 656.221,5 660.437,8.542     " />
<polygon fill="#231F20" points="663.286,6.223 661.831,5 661.278,5 662.856,6.324     " />
<polygon fill="#231F20" points="665.362,5 656.479,13 657.021,13 665.905,5   " />
<polygon fill="#231F20" points="670.503,5 661.616,13 662.161,13 671.046,5   " />
<polygon fill="#231F20" points="676.084,5 675.644,5 666.757,13 667.3,13 676.165,5.018   " />
<polygon fill="#231F20" points="677.803,7.684 671.897,13 672.44,13 677.803,8.172 677.803,8.174 683.548,13 684.099,13

                            677.803,7.711   " />
<polygon fill="#231F20" points="677.803,11.025 675.608,13 676.151,13 677.803,11.514 677.803,11.547 679.532,13 680.083,13

                            677.803,11.085  " />
<polygon fill="#231F20" points="679.208,5 688.731,13 689.282,13 679.76,5    " />
<polygon fill="#231F20" points="684.589,5 694.114,13 694.664,13 685.14,5    " />
<polygon fill="#231F20" points="689.639,5 698.975,12.842 699.406,12.741 690.19,5    " />
<polygon fill="#231F20" points="701.929,10.627 695.231,5 694.68,5 701.499,10.728    " />
<polygon fill="#231F20" points="704.411,8.441 700.317,5 699.766,5 703.981,8.542     " />
<polygon fill="#231F20" points="706.831,6.223 705.376,5 704.823,5 706.401,6.324     " />
<polygon fill="#231F20" points="708.907,5 700.023,13 700.565,13 709.45,5    " />
<polygon fill="#231F20" points="714.048,5 705.161,13 705.706,13 714.591,5   " />
<polygon fill="#231F20" points="719.629,5 719.188,5 710.302,13 710.845,13 719.71,5.018  " />
<polygon fill="#231F20" points="721.348,7.684 715.442,13 715.985,13 721.348,8.172 721.348,8.174 727.093,13 727.644,13

                            721.348,7.711   " />
<polygon fill="#231F20" points="721.348,11.025 719.153,13 719.696,13 721.348,11.514 721.348,11.547 723.077,13 723.628,13

                            721.348,11.085  " />
<polygon fill="#231F20" points="722.753,5 732.276,13 732.827,13 723.305,5   " />
<polygon fill="#231F20" points="728.134,5 737.659,13 738.209,13 728.685,5   " />
<polygon fill="#231F20" points="733.184,5 742.52,12.842 742.951,12.741 733.735,5    " />
 <polygon fill="#231F20" points="745.474,10.627 738.776,5 738.225,5 745.044,10.728   " />
<polygon fill="#231F20" points="747.956,8.441 743.862,5 743.311,5 747.526,8.542     " />
<polygon fill="#231F20" points="750.376,6.223 748.921,5 748.368,5 749.946,6.324     " />
<polygon fill="#231F20" points="752.452,5 743.568,13 744.11,13 752.995,5    " />
<polygon fill="#231F20" points="757.593,5 748.706,13 749.251,13 758.136,5   " />
<polygon fill="#231F20" points="763.174,5 762.733,5 753.847,13 754.39,13 763.255,5.018  " />
<polygon fill="#231F20" points="764.893,7.684 758.987,13 759.53,13 764.893,8.172 764.893,8.174 770.638,13 771.188,13

                            764.893,7.711   " />
<polygon fill="#231F20" points="764.893,11.025 762.698,13 763.241,13 764.893,11.514 764.893,11.547 766.622,13 767.173,13

                            764.893,11.085  " />
<polygon fill="#231F20" points="766.298,5 775.821,13 776.372,13 766.85,5    " />
<polygon fill="#231F20" points="771.679,5 781.204,13 781.754,13 772.229,5   " />
<polygon fill="#231F20" points="776.729,5 786.064,12.842 786.496,12.741 777.28,5    " />
<polygon fill="#231F20" points="789.019,10.627 782.321,5 781.77,5 788.589,10.728    " />
<polygon fill="#231F20" points="791.501,8.441 787.407,5 786.855,5 791.071,8.542     " />
<polygon fill="#231F20" points="793.921,6.223 792.466,5 791.913,5 793.491,6.324     " />
<polygon fill="#231F20" points="795.997,5 787.113,13 787.655,13 796.54,5    " />
<polygon fill="#231F20" points="801.138,5 792.251,13 792.796,13 801.681,5   " />
<polygon fill="#231F20" points="806.719,5 806.278,5 797.392,13 797.935,13 806.8,5.018   " />
<polygon fill="#231F20" points="808.438,7.684 802.532,13 803.075,13 808.438,8.172 808.438,8.174 814.183,13 814.733,13

                            808.438,7.711   " />
<polygon fill="#231F20" points="808.438,11.025 806.243,13 806.786,13 808.438,11.514 808.438,11.547 810.167,13 810.718,13

                            808.438,11.085  " />
<polygon fill="#231F20" points="809.843,5 819.366,13 819.917,13 810.395,5   " />
<polygon fill="#231F20" points="815.224,5 824.749,13 825.299,13 815.774,5   " />
<polygon fill="#231F20" points="820.273,5 829.609,12.842 830.041,12.741 820.825,5   " />
<polygon fill="#231F20" points="832.563,10.627 825.866,5 825.314,5 832.134,10.728   " />
<polygon fill="#231F20" points="835.046,8.441 830.952,5 830.4,5 834.616,8.542   " />
<polygon fill="#231F20" points="837.466,6.223 836.011,5 835.458,5 837.036,6.324     " />
<polygon fill="#231F20" points="839.542,5 830.658,13 831.2,13 840.085,5     " />
<polygon fill="#231F20" points="844.683,5 835.796,13 836.341,13 845.226,5   " />
<polygon fill="#231F20" points="850.264,5 849.823,5 840.937,13 841.479,13 850.345,5.018     " />
<polygon fill="#231F20" points="851.982,7.684 846.077,13 846.62,13 851.982,8.172 851.982,8.174 857.728,13 858.278,13

                            851.982,7.711   " />
<polygon fill="#231F20" points="851.982,11.025 849.788,13 850.331,13 851.982,11.514 851.982,11.547 853.712,13 854.263,13

                            851.982,11.085  " />
<polygon fill="#231F20" points="853.388,5 862.911,13 863.462,13 853.939,5   " />
<polygon fill="#231F20" points="858.769,5 868.294,13 868.844,13 859.319,5   " />
<polygon fill="#231F20" points="863.818,5 873.154,12.842 873.586,12.741 864.37,5    " />
<polygon fill="#231F20" points="876.108,10.627 869.411,5 868.859,5 875.679,10.728   " />
<polygon fill="#231F20" points="878.591,8.441 874.497,5 873.945,5 878.161,8.542     " />
<polygon fill="#231F20" points="881.011,6.223 879.556,5 879.003,5 880.581,6.324     " />
<polygon fill="#231F20" points="883.087,5 874.203,13 874.745,13 883.63,5    " />
<polygon fill="#231F20" points="888.228,5 879.341,13 879.886,13 888.771,5   " />
<polygon fill="#231F20" points="893.809,5 893.368,5 884.481,13 885.024,13 893.89,5.018  " />
<polygon fill="#231F20" points="895.527,7.684 889.622,13 890.165,13 895.527,8.172 895.527,8.174 901.272,13 901.823,13

                            895.527,7.711   " />
<polygon fill="#231F20" points="895.527,11.025 893.333,13 893.876,13 895.527,11.514 895.527,11.547 897.257,13 897.808,13

                            895.527,11.085  " />
<polygon fill="#231F20" points="896.933,5 906.456,13 907.007,13 897.484,5   " />
<polygon fill="#231F20" points="902.313,5 911.839,13 912.389,13 902.864,5   " />
<polygon fill="#231F20" points="907.363,5 916.699,12.842 917.131,12.741 907.915,5   " />
<polygon fill="#231F20" points="919.653,10.627 912.956,5 912.404,5 919.224,10.728   " />
<polygon fill="#231F20" points="922.136,8.441 918.042,5 917.49,5 921.706,8.542  " />
 <polygon fill="#231F20" points="924.556,6.223 923.101,5 922.548,5 924.126,6.324     " />
<polygon fill="#231F20" points="926.632,5 917.748,13 918.29,13 927.175,5    " />
<polygon fill="#231F20" points="931.772,5 922.886,13 923.431,13 932.315,5   " />
<polygon fill="#231F20" points="937.354,5 936.913,5 928.026,13 928.569,13 937.435,5.018     " />
<polygon fill="#231F20" points="939.072,7.684 933.167,13 933.71,13 939.072,8.172 939.072,8.174 944.817,13 945.368,13

                            939.072,7.711   " />
<polygon fill="#231F20" points="939.072,11.025 936.878,13 937.421,13 939.072,11.514 939.072,11.547 940.802,13 941.353,13

                            939.072,11.085  " />
<polygon fill="#231F20" points="940.478,5 950.001,13 950.552,13 941.029,5   " />
<polygon fill="#231F20" points="945.858,5 955.384,13 955.934,13 946.409,5   " />
<polygon fill="#231F20" points="950.908,5 960.244,12.842 960.676,12.741 951.46,5    " />
<polygon fill="#231F20" points="963.198,10.627 956.501,5 955.949,5 962.769,10.728   " />
<polygon fill="#231F20" points="965.681,8.441 961.587,5 961.035,5 965.251,8.542     " />
<polygon fill="#231F20" points="968.101,6.223 966.646,5 966.093,5 967.671,6.324     " />
<polygon fill="#231F20" points="970.177,5 961.293,13 961.835,13 970.72,5    " />
<polygon fill="#231F20" points="975.317,5 966.431,13 966.976,13 975.86,5    " />
<polygon fill="#231F20" points="980.898,5 980.458,5 971.571,13 972.114,13 980.979,5.018     " />
<polygon fill="#231F20" points="982.617,7.684 976.712,13 977.255,13 982.617,8.172 982.617,8.174 988.362,13 988.913,13

                            982.617,7.711   " />
<polygon fill="#231F20" points="982.617,11.025 980.423,13 980.966,13 982.617,11.514 982.617,11.547 984.347,13 984.897,13

                            982.617,11.085  " />
<polygon fill="#231F20" points="984.022,5 993.546,13 994.097,13 984.574,5   " />
<polygon fill="#231F20" points="989.403,5 998.929,13 999.479,13 989.954,5   " />
<polygon fill="#231F20" points="994.453,5 1003.789,12.842 1004.221,12.741 995.005,5     " />
<polygon fill="#231F20" points="1006.743,10.627 1000.046,5 999.494,5 1006.313,10.728    " />
<polygon fill="#231F20" points="1009.226,8.441 1005.132,5 1004.58,5 1008.796,8.542  " />
<polygon fill="#231F20" points="1011.646,6.223 1010.19,5 1009.638,5 1011.216,6.324  " />
<polygon fill="#231F20" points="1013.722,5 1004.838,13 1005.38,13 1014.265,5    " />
<polygon fill="#231F20" points="1018.862,5 1009.976,13 1010.521,13 1019.405,5   " />
<polygon fill="#231F20" points="1024.443,5 1024.003,5 1015.116,13 1015.659,13 1024.524,5.018    " />
<polygon fill="#231F20" points="1026.162,7.684 1020.257,13 1020.8,13 1026.162,8.172 1026.162,8.174 1031.907,13 1032.458,13

                            1026.162,7.711  " />
<polygon fill="#231F20" points="1026.162,11.025 1023.968,13 1024.511,13 1026.162,11.514 1026.162,11.547 1027.892,13

                            1028.442,13 1026.162,11.085     " />
<polygon fill="#231F20" points="1027.567,5 1037.091,13 1037.642,13 1028.119,5   " />
<polygon fill="#231F20" points="1032.948,5 1042.474,13 1043.023,13 1033.499,5   " />
<polygon fill="#231F20" points="1037.998,5 1047.334,12.842 1047.766,12.741 1038.55,5    " />
<polygon fill="#231F20" points="1050.288,10.627 1043.591,5 1043.039,5 1049.858,10.728   " />
<polygon fill="#231F20" points="1052.771,8.441 1048.677,5 1048.125,5 1052.341,8.542     " />
<polygon fill="#231F20" points="1055.19,6.223 1053.735,5 1053.183,5 1054.761,6.324  " />
<polygon fill="#231F20" points="1057.267,5 1048.383,13 1048.925,13 1057.81,5    " />
<polygon fill="#231F20" points="1062.407,5 1053.521,13 1054.065,13 1062.95,5    " />
<polygon fill="#231F20" points="1067.988,5 1067.548,5 1058.661,13 1059.204,13 1068.069,5.018    " />
<polygon fill="#231F20" points="1069.707,7.684 1063.802,13 1064.345,13 1069.707,8.172 1069.707,8.174 1075.452,13 1076.003,13

                            1069.707,7.711  " />
<polygon fill="#231F20" points="1069.707,11.025 1067.513,13 1068.056,13 1069.707,11.514 1069.707,11.547 1071.437,13

                            1071.987,13 1069.707,11.085     " />
<polygon fill="#231F20" points="1071.112,5 1080.636,13 1081.187,13 1071.664,5   " />
<polygon fill="#231F20" points="1076.493,5 1086.019,13 1086.568,13 1077.044,5   " />
<polygon fill="#231F20" points="1081.543,5 1090.879,12.842 1091.311,12.741 1082.095,5   " />
<polygon fill="#231F20" points="1093.833,10.627 1087.136,5 1086.584,5 1093.403,10.728   " />
<polygon fill="#231F20" points="1096.315,8.441 1092.222,5 1091.67,5 1095.886,8.542  " />
<polygon fill="#231F20" points="1098.735,6.223 1097.28,5 1096.728,5 1098.306,6.324  " />
<polygon fill="#231F20" points="1100.812,5 1091.928,13 1092.47,13 1101.354,5    " />
<polygon fill="#231F20" points="1105.952,5 1097.065,13 1097.61,13 1106.495,5    " />
<polygon fill="#231F20" points="1111.533,5 1111.093,5 1102.206,13 1102.749,13 1111.614,5.018    " />
<polygon fill="#231F20" points="1113.252,7.684 1107.347,13 1107.89,13 1113.252,8.172 1113.252,8.174 1118.997,13 1119.548,13

                            1113.252,7.711  " />
<polygon fill="#231F20" points="1113.252,11.025 1111.058,13 1111.601,13 1113.252,11.514 1113.252,11.547 1114.981,13

                            1115.532,13 1113.252,11.085     " />
<polygon fill="#231F20" points="1114.657,5 1124.181,13 1124.731,13 1115.209,5   " />
<polygon fill="#231F20" points="1120.038,5 1129.563,13 1130.113,13 1120.589,5   " />
<polygon fill="#231F20" points="1125.088,5 1134.424,12.842 1134.855,12.741 1125.64,5    " />
<polygon fill="#231F20" points="1137.378,10.627 1130.681,5 1130.129,5 1136.948,10.728   " />
<polygon fill="#231F20" points="1139.86,8.441 1135.767,5 1135.215,5 1139.431,8.542  " />
<polygon fill="#231F20" points="1142.28,6.223 1140.825,5 1140.272,5 1141.851,6.324  " />
 <polygon fill="#231F20" points="1144.356,5 1135.473,13 1136.015,13 1144.899,5   " />
<polygon fill="#231F20" points="1149.497,5 1140.61,13 1141.155,13 1150.04,5     " />
<polygon fill="#231F20" points="1155.078,5 1154.638,5 1145.751,13 1146.294,13 1155.159,5.018    " />
<polygon fill="#231F20" points="1156.797,7.684 1150.892,13 1151.435,13 1156.797,8.172 1156.797,8.174 1162.542,13 1163.093,13

                            1156.797,7.711  " />
<polygon fill="#231F20" points="1156.797,11.025 1154.603,13 1155.146,13 1156.797,11.514 1156.797,11.547 1158.526,13

                            1159.077,13 1156.797,11.085     " />
<polygon fill="#231F20" points="1158.202,5 1167.726,13 1168.276,13 1158.754,5   " />
<polygon fill="#231F20" points="1163.583,5 1173.108,13 1173.658,13 1164.134,5   " />
<polygon fill="#231F20" points="1168.633,5 1177.969,12.842 1178.4,12.741 1169.185,5     " />
<polygon fill="#231F20" points="1180.923,10.627 1174.226,5 1173.674,5 1180.493,10.728   " />
<polygon fill="#231F20" points="1183.405,8.441 1179.312,5 1178.76,5 1182.976,8.542  " />
<polygon fill="#231F20" points="1185.825,6.223 1184.37,5 1183.817,5 1185.396,6.324  " />
 <polygon fill="#231F20" points="1187.901,5 1179.018,13 1179.56,13 1188.444,5    " />
<polygon fill="#231F20" points="1193.042,5 1184.155,13 1184.7,13 1193.585,5     " />
<polygon fill="#231F20" points="1189.296,13 1189.839,13 1194.629,8.688 1194.629,8.199   " />
<polygon fill="#231F20" points="1194.629,13 1194.629,12.827 1194.437,13     " />
</g>
</svg></div>
<div class="clear"></div>
</div>
</div>
</div>
<div id="post-314" class="post-314 post type-post status-publish format-standard has-post-thumbnail hentry category-my-story tag-blogger col-md-6" itemscope itemtype="http://schema.org/NewsArticle" itemprop="blogPost" role="article">
<div class="df-post-wrapper">
<div class="clear"></div>
<div class="featured-media  filter_bw"><a href="../../tgif/index.html"><img width="600" height="600" src="../../wp-content/uploads/sites/26/2015/12/menswear_58-600x600.jpg" class="attachment-loop-blog size-loop-blog wp-post-image" alt="" srcset="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_58-600x600.jpg 600w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_58-150x150.jpg 150w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_58-250x250.jpg 250w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_58-80x80.jpg 80w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_58-330x330.jpg 330w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_58-160x160.jpg 160w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_58-293x293.jpg 293w" sizes="(max-width: 600px) 100vw, 600px" data-attachment-id="549" data-permalink="http://dahz.daffyhazan.com/applique/menswear/tgif/menswear_58/" data-orig-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_58.jpg" data-orig-size="1200,857" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="menswear_58" data-image-description="" data-medium-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_58-300x214.jpg" data-large-file="../../wp-content/uploads/sites/26/2015/12/menswear_58-1024x731.jpg" /></a></div>
<div class="grid-outer">
<div class="grid-wrapper">
<div class="df-post-title  aligncenter" onclick="void(0)"><div class="df-post-title-inner"><div class="df-single-category"><span class="entry-terms category" itemprop="articleSection"><a href="index.html" rel="category tag">My Story</a></span></div><h2 class="entry-title " itemprop="headline"><a href="../../tgif/index.html" title="#TGIF">#TGIF</a></h2><div class="df-post-on"><time class="entry-published updated" datetime="2015-12-11T07:02:34+00:00" title="Friday, December 11, 2015, 7:02 am"><a href="../../2015/12/11/index.html"><meta itemprop="datePublished" content="December 11, 2015">December 11, 2015</a></time></div></div></div>
<div class="entry-summary" itemprop="description">
<p>Applique is a fully responsive Fashion blog theme that designed with style publishers in mind! We keep things simple and elegant to make sure any fashion blogger—even those without IT background, can use it. We believe that a blog theme, should feel fluid, light, and intuitive. That’s what we are aiming to make with applique, &hellip; <a class="more-link" href="../../tgif/index.html">Continue Reading<i class="ion-ios-arrow-thin-right"></i></a></p>
</div>
</div>
<div class="clear"></div>
<div class="df-postmeta-wrapper"><div class="df-postmeta border-top"><div class="clear"></div><div class="col-left alignleft"><span itemtype="http://schema.org/Comment" itemscope="itemscope" itemprop="comment"><a class="comment-permalink perma" href="../../tgif/index.html#respond" itemprop="url">No Comments</a></span></div><div class="col-right alignright"><ul class="df-share"><li><span>Share</span></li><li><a class="df-facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http://dahz.daffyhazan.com/applique/menswear/tgif/"><i class="fa fa-facebook"></i><i class="fa fa-facebook"></i></a></li><li><a class="df-twitter" target="_blank" href="https://twitter.com/home?status=Check%20out%20this%20article:%20%23TGIF%20-%20http://dahz.daffyhazan.com/applique/menswear/tgif/"><i class="fa fa-twitter"></i><i class="fa fa-twitter"></i></a></li><li><a class="df-google" target="_blank" href="https://plus.google.com/share?url=http://dahz.daffyhazan.com/applique/menswear/tgif/"><i class="fa fa-google-plus"></i><i class="fa fa-google-plus"></i></a></li><li><a class="df-pinterest" target="_blank" href="https://pinterest.com/pin/create/button/?url=http://dahz.daffyhazan.com/applique/menswear/tgif/&amp;media=http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_58.jpg&amp;description=%23TGIF"><i class="fa fa-pinterest"></i><i class="fa fa-pinterest"></i></a></li></ul></div><div class="clear"></div></div><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="8px" viewBox="550 5 100 8">
<g>
<polygon fill="#231F20" points="24.629,8.174 30.374,13 30.925,13 24.629,7.711   " />
<polygon fill="#231F20" points="26.034,5 35.558,13 36.108,13 26.586,5   " />
<polygon fill="#231F20" points="31.415,5 40.94,13 41.489,13 31.966,5    " />
<polygon fill="#231F20" points="36.465,5 45.801,12.842 46.231,12.741 37.017,5   " />
<polygon fill="#231F20" points="48.755,10.627 42.058,5 41.506,5 48.325,10.728   " />
<polygon fill="#231F20" points="51.237,8.441 47.144,5 46.592,5 50.808,8.542     " />
<polygon fill="#231F20" points="53.657,6.223 52.202,5 51.649,5 53.228,6.324     " />
<polygon fill="#231F20" points="55.733,5 46.849,13 47.392,13 56.276,5   " />
 <polygon fill="#231F20" points="60.874,5 51.987,13 52.532,13 61.417,5   " />
<polygon fill="#231F20" points="66.455,5 66.015,5 57.128,13 57.671,13 66.536,5.018  " />
<polygon fill="#231F20" points="68.174,7.684 62.269,13 62.812,13 68.174,8.172 68.174,8.174 73.919,13 74.47,13 68.174,7.711  " />
<polygon fill="#231F20" points="24.629,11.547 26.358,13 26.909,13 24.629,11.085     " />
<polygon fill="#231F20" points="68.174,11.025 65.979,13 66.522,13 68.174,11.514 68.174,11.547 69.903,13 70.454,13

                            68.174,11.085   " />
<polygon fill="#231F20" points="69.579,5 79.103,13 79.653,13 70.131,5   " />
<polygon fill="#231F20" points="74.96,5 84.485,13 85.035,13 75.511,5    " />
<polygon fill="#231F20" points="80.01,5 89.346,12.842 89.777,12.741 80.562,5    " />
<polygon fill="#231F20" points="92.3,10.627 85.603,5 85.051,5 91.87,10.728  " />
<polygon fill="#231F20" points="94.782,8.441 90.688,5 90.137,5 94.353,8.542     " />
<polygon fill="#231F20" points="97.202,6.223 95.747,5 95.194,5 96.772,6.324     " />
<polygon fill="#231F20" points="99.278,5 90.395,13 90.937,13 99.821,5   " />
<polygon fill="#231F20" points="104.419,5 95.532,13 96.077,13 104.962,5     " />
<polygon fill="#231F20" points="110,5 109.56,5 100.673,13 101.216,13 110.081,5.018  " />
<polygon fill="#231F20" points="111.719,7.684 105.813,13 106.356,13 111.719,8.172 111.719,8.174 117.464,13 118.015,13

                            111.719,7.711   " />
<polygon fill="#231F20" points="111.719,11.025 109.524,13 110.067,13 111.719,11.514 111.719,11.547 113.448,13 113.999,13

                            111.719,11.085  " />
<polygon fill="#231F20" points="113.124,5 122.647,13 123.198,13 113.676,5   " />
<polygon fill="#231F20" points="118.505,5 128.03,13 128.58,13 119.056,5     " />
<polygon fill="#231F20" points="123.555,5 132.891,12.842 133.322,12.741 124.106,5   " />
<polygon fill="#231F20" points="135.845,10.627 129.147,5 128.596,5 135.415,10.728   " />
<polygon fill="#231F20" points="138.327,8.441 134.233,5 133.682,5 137.897,8.542     " />
<polygon fill="#231F20" points="140.747,6.223 139.292,5 138.739,5 140.317,6.324     " />
<polygon fill="#231F20" points="142.823,5 133.939,13 134.481,13 143.366,5   " />
<polygon fill="#231F20" points="147.964,5 139.077,13 139.622,13 148.507,5   " />
<polygon fill="#231F20" points="153.545,5 153.104,5 144.218,13 144.761,13 153.626,5.018     " />
<polygon fill="#231F20" points="155.264,7.684 149.358,13 149.901,13 155.264,8.172 155.264,8.174 161.009,13 161.56,13

                            155.264,7.711   " />
<polygon fill="#231F20" points="155.264,11.025 153.069,13 153.612,13 155.264,11.514 155.264,11.547 156.993,13 157.544,13

                            155.264,11.085  " />
<polygon fill="#231F20" points="156.669,5 166.192,13 166.743,13 157.221,5   " />
<polygon fill="#231F20" points="162.05,5 171.575,13 172.125,13 162.601,5    " />
<polygon fill="#231F20" points="167.1,5 176.436,12.842 176.867,12.741 167.651,5     " />
<polygon fill="#231F20" points="179.39,10.627 172.692,5 172.141,5 178.96,10.728     " />
<polygon fill="#231F20" points="181.872,8.441 177.778,5 177.227,5 181.442,8.542     " />
<polygon fill="#231F20" points="184.292,6.223 182.837,5 182.284,5 183.862,6.324     " />
<polygon fill="#231F20" points="186.368,5 177.484,13 178.026,13 186.911,5   " />
<polygon fill="#231F20" points="191.509,5 182.622,13 183.167,13 192.052,5   " />
<polygon fill="#231F20" points="197.09,5 196.649,5 187.763,13 188.306,13 197.171,5.018  " />
<polygon fill="#231F20" points="198.809,7.684 192.903,13 193.446,13 198.809,8.172 198.809,8.174 204.554,13 205.104,13

                            198.809,7.711   " />
<polygon fill="#231F20" points="198.809,11.025 196.614,13 197.157,13 198.809,11.514 198.809,11.547 200.538,13 201.089,13

                            198.809,11.085  " />
<polygon fill="#231F20" points="200.214,5 209.737,13 210.288,13 200.766,5   " />
<polygon fill="#231F20" points="205.595,5 215.12,13 215.67,13 206.146,5     " />
<polygon fill="#231F20" points="210.645,5 219.98,12.842 220.412,12.741 211.196,5    " />
<polygon fill="#231F20" points="222.935,10.627 216.237,5 215.686,5 222.505,10.728   " />
<polygon fill="#231F20" points="225.417,8.441 221.323,5 220.771,5 224.987,8.542     " />
<polygon fill="#231F20" points="227.837,6.223 226.382,5 225.829,5 227.407,6.324     " />
<polygon fill="#231F20" points="229.913,5 221.029,13 221.571,13 230.456,5   " />
<polygon fill="#231F20" points="235.054,5 226.167,13 226.712,13 235.597,5   " />
<polygon fill="#231F20" points="240.635,5 240.194,5 231.308,13 231.851,13 240.716,5.018     " />
<polygon fill="#231F20" points="242.354,7.684 236.448,13 236.991,13 242.354,8.172 242.354,8.174 248.099,13 248.649,13

                            242.354,7.711   " />
<polygon fill="#231F20" points="242.354,11.025 240.159,13 240.702,13 242.354,11.514 242.354,11.547 244.083,13 244.634,13

                            242.354,11.085  " />
<polygon fill="#231F20" points="243.759,5 253.282,13 253.833,13 244.311,5   " />
<polygon fill="#231F20" points="249.14,5 258.665,13 259.215,13 249.69,5     " />
<polygon fill="#231F20" points="254.189,5 263.525,12.842 263.957,12.741 254.741,5   " />
<polygon fill="#231F20" points="266.479,10.627 259.782,5 259.23,5 266.05,10.728     " />
<polygon fill="#231F20" points="268.962,8.441 264.868,5 264.316,5 268.532,8.542     " />
<polygon fill="#231F20" points="271.382,6.223 269.927,5 269.374,5 270.952,6.324     " />
<polygon fill="#231F20" points="273.458,5 264.574,13 265.116,13 274.001,5   " />
<polygon fill="#231F20" points="278.599,5 269.712,13 270.257,13 279.142,5   " />
<polygon fill="#231F20" points="284.18,5 283.739,5 274.853,13 275.396,13 284.261,5.018  " />
<polygon fill="#231F20" points="285.898,7.684 279.993,13 280.536,13 285.898,8.172 285.898,8.174 291.644,13 292.194,13

                            285.898,7.711   " />
<polygon fill="#231F20" points="285.898,11.025 283.704,13 284.247,13 285.898,11.514 285.898,11.547 287.628,13 288.179,13

                            285.898,11.085  " />
<polygon fill="#231F20" points="287.304,5 296.827,13 297.378,13 287.855,5   " />
<polygon fill="#231F20" points="292.685,5 302.21,13 302.76,13 293.235,5     " />
<polygon fill="#231F20" points="297.734,5 307.07,12.842 307.502,12.741 298.286,5    " />
<polygon fill="#231F20" points="310.024,10.627 303.327,5 302.775,5 309.595,10.728   " />
<polygon fill="#231F20" points="312.507,8.441 308.413,5 307.861,5 312.077,8.542     " />
<polygon fill="#231F20" points="314.927,6.223 313.472,5 312.919,5 314.497,6.324     " />
<polygon fill="#231F20" points="317.003,5 308.119,13 308.661,13 317.546,5   " />
<polygon fill="#231F20" points="322.144,5 313.257,13 313.802,13 322.687,5   " />
<polygon fill="#231F20" points="327.725,5 327.284,5 318.397,13 318.94,13 327.806,5.018  " />
<polygon fill="#231F20" points="329.443,7.684 323.538,13 324.081,13 329.443,8.172 329.443,8.174 335.188,13 335.739,13

                            329.443,7.711   " />
<polygon fill="#231F20" points="329.443,11.025 327.249,13 327.792,13 329.443,11.514 329.443,11.547 331.173,13 331.724,13

                            329.443,11.085  " />
<polygon fill="#231F20" points="330.849,5 340.372,13 340.923,13 331.4,5     " />
<polygon fill="#231F20" points="336.229,5 345.755,13 346.305,13 336.78,5    " />
<polygon fill="#231F20" points="341.279,5 350.615,12.842 351.047,12.741 341.831,5   " />
<polygon fill="#231F20" points="353.569,10.627 346.872,5 346.32,5 353.14,10.728     " />
<polygon fill="#231F20" points="356.052,8.441 351.958,5 351.406,5 355.622,8.542     " />
<polygon fill="#231F20" points="358.472,6.223 357.017,5 356.464,5 358.042,6.324     " />
<polygon fill="#231F20" points="360.548,5 351.664,13 352.206,13 361.091,5   " />
<polygon fill="#231F20" points="365.688,5 356.802,13 357.347,13 366.231,5   " />
<polygon fill="#231F20" points="371.27,5 370.829,5 361.942,13 362.485,13 371.351,5.018  " />
<polygon fill="#231F20" points="372.988,7.684 367.083,13 367.626,13 372.988,8.172 372.988,8.174 378.733,13 379.284,13

                            372.988,7.711   " />
<polygon fill="#231F20" points="372.988,11.025 370.794,13 371.337,13 372.988,11.514 372.988,11.547 374.718,13 375.269,13

                            372.988,11.085  " />
<polygon fill="#231F20" points="374.394,5 383.917,13 384.468,13 374.945,5   " />
<polygon fill="#231F20" points="379.774,5 389.3,13 389.85,13 380.325,5  " />
<polygon fill="#231F20" points="384.824,5 394.16,12.842 394.592,12.741 385.376,5    " />
<polygon fill="#231F20" points="397.114,10.627 390.417,5 389.865,5 396.685,10.728   " />
 <polygon fill="#231F20" points="399.597,8.441 395.503,5 394.951,5 399.167,8.542     " />
<polygon fill="#231F20" points="402.017,6.223 400.562,5 400.009,5 401.587,6.324     " />
<polygon fill="#231F20" points="404.093,5 395.209,13 395.751,13 404.636,5   " />
<polygon fill="#231F20" points="409.233,5 400.347,13 400.892,13 409.776,5   " />
<polygon fill="#231F20" points="414.814,5 414.374,5 405.487,13 406.03,13 414.896,5.018  " />
<polygon fill="#231F20" points="416.533,7.684 410.628,13 411.171,13 416.533,8.172 416.533,8.174 422.278,13 422.829,13

                            416.533,7.711   " />
<polygon fill="#231F20" points="416.533,11.025 414.339,13 414.882,13 416.533,11.514 416.533,11.547 418.263,13 418.813,13

                            416.533,11.085  " />
<polygon fill="#231F20" points="417.938,5 427.462,13 428.013,13 418.49,5    " />
<polygon fill="#231F20" points="423.319,5 432.845,13 433.395,13 423.87,5    " />
<polygon fill="#231F20" points="428.369,5 437.705,12.842 438.137,12.741 428.921,5   " />
<polygon fill="#231F20" points="440.659,10.627 433.962,5 433.41,5 440.229,10.728    " />
<polygon fill="#231F20" points="443.142,8.441 439.048,5 438.496,5 442.712,8.542     " />
<polygon fill="#231F20" points="445.562,6.223 444.106,5 443.554,5 445.132,6.324     " />
<polygon fill="#231F20" points="447.638,5 438.754,13 439.296,13 448.181,5   " />
<polygon fill="#231F20" points="452.778,5 443.892,13 444.437,13 453.321,5   " />
<polygon fill="#231F20" points="458.359,5 457.919,5 449.032,13 449.575,13 458.44,5.018  " />
<polygon fill="#231F20" points="460.078,7.684 454.173,13 454.716,13 460.078,8.172 460.078,8.174 465.823,13 466.374,13

                            460.078,7.711   " />
<polygon fill="#231F20" points="460.078,11.025 457.884,13 458.427,13 460.078,11.514 460.078,11.547 461.808,13 462.358,13

                            460.078,11.085  " />
<polygon fill="#231F20" points="461.483,5 471.007,13 471.558,13 462.035,5   " />
<polygon fill="#231F20" points="466.864,5 476.39,13 476.939,13 467.415,5    " />
<polygon fill="#231F20" points="471.914,5 481.25,12.842 481.682,12.741 472.466,5    " />
<polygon fill="#231F20" points="484.204,10.627 477.507,5 476.955,5 483.774,10.728   " />
<polygon fill="#231F20" points="486.687,8.441 482.593,5 482.041,5 486.257,8.542     " />
<polygon fill="#231F20" points="489.106,6.223 487.651,5 487.099,5 488.677,6.324     " />
<polygon fill="#231F20" points="491.183,5 482.299,13 482.841,13 491.726,5   " />
<polygon fill="#231F20" points="496.323,5 487.437,13 487.981,13 496.866,5   " />
<polygon fill="#231F20" points="501.904,5 501.464,5 492.577,13 493.12,13 501.985,5.018  " />
<polygon fill="#231F20" points="503.623,7.684 497.718,13 498.261,13 503.623,8.172 503.623,8.174 509.368,13 509.919,13

                            503.623,7.711   " />
<polygon fill="#231F20" points="503.623,11.025 501.429,13 501.972,13 503.623,11.514 503.623,11.547 505.353,13 505.903,13

                            503.623,11.085  " />
<polygon fill="#231F20" points="505.028,5 514.552,13 515.103,13 505.58,5    " />
<polygon fill="#231F20" points="510.409,5 519.935,13 520.484,13 510.96,5    " />
<polygon fill="#231F20" points="515.459,5 524.795,12.842 525.227,12.741 516.011,5   " />
<polygon fill="#231F20" points="527.749,10.627 521.052,5 520.5,5 527.319,10.728     " />
<polygon fill="#231F20" points="530.231,8.441 526.138,5 525.586,5 529.802,8.542     " />
<polygon fill="#231F20" points="532.651,6.223 531.196,5 530.644,5 532.222,6.324     " />
<polygon fill="#231F20" points="534.728,5 525.844,13 526.386,13 535.271,5   " />
<polygon fill="#231F20" points="539.868,5 530.981,13 531.526,13 540.411,5   " />
<polygon fill="#231F20" points="545.449,5 545.009,5 536.122,13 536.665,13 545.53,5.018  " />
<polygon fill="#231F20" points="547.168,7.684 541.263,13 541.806,13 547.168,8.172 547.168,8.174 552.913,13 553.464,13

                            547.168,7.711   " />
<polygon fill="#231F20" points="547.168,11.025 544.974,13 545.517,13 547.168,11.514 547.168,11.547 548.897,13 549.448,13

                            547.168,11.085  " />
<polygon fill="#231F20" points="548.573,5 558.097,13 558.647,13 549.125,5   " />
<polygon fill="#231F20" points="553.954,5 563.479,13 564.029,13 554.505,5   " />
<polygon fill="#231F20" points="559.004,5 568.34,12.842 568.771,12.741 559.556,5    " />
<polygon fill="#231F20" points="571.294,10.627 564.597,5 564.045,5 570.864,10.728   " />
<polygon fill="#231F20" points="573.776,8.441 569.683,5 569.131,5 573.347,8.542     " />
<polygon fill="#231F20" points="576.196,6.223 574.741,5 574.188,5 575.767,6.324     " />
<polygon fill="#231F20" points="578.272,5 569.389,13 569.931,13 578.815,5   " />
<polygon fill="#231F20" points="583.413,5 574.526,13 575.071,13 583.956,5   " />
<polygon fill="#231F20" points="588.994,5 588.554,5 579.667,13 580.21,13 589.075,5.018  " />
<polygon fill="#231F20" points="590.713,7.684 584.808,13 585.351,13 590.713,8.172 590.713,8.174 596.458,13 597.009,13

                            590.713,7.711   " />
<polygon fill="#231F20" points="590.713,11.025 588.519,13 589.062,13 590.713,11.514 590.713,11.547 592.442,13 592.993,13

                            590.713,11.085  " />
<polygon fill="#231F20" points="592.118,5 601.642,13 602.192,13 592.67,5    " />
<polygon fill="#231F20" points="597.499,5 607.024,13 607.574,13 598.05,5    " />
<polygon fill="#231F20" points="602.549,5 611.885,12.842 612.316,12.741 603.101,5   " />
<polygon fill="#231F20" points="614.839,10.627 608.142,5 607.59,5 614.409,10.728    " />
<polygon fill="#231F20" points="617.321,8.441 613.228,5 612.676,5 616.892,8.542     " />
<polygon fill="#231F20" points="619.741,6.223 618.286,5 617.733,5 619.312,6.324     " />
<polygon fill="#231F20" points="621.817,5 612.934,13 613.476,13 622.36,5    " />
<polygon fill="#231F20" points="626.958,5 618.071,13 618.616,13 627.501,5   " />
<polygon fill="#231F20" points="632.539,5 632.099,5 623.212,13 623.755,13 632.62,5.018  " />
<polygon fill="#231F20" points="634.258,7.684 628.353,13 628.896,13 634.258,8.172 634.258,8.174 640.003,13 640.554,13

                            634.258,7.711   " />
<polygon fill="#231F20" points="634.258,11.025 632.063,13 632.606,13 634.258,11.514 634.258,11.547 635.987,13 636.538,13

                            634.258,11.085  " />
<polygon fill="#231F20" points="635.663,5 645.187,13 645.737,13 636.215,5   " />

<polygon fill="#231F20" points="641.044,5 650.569,13 651.119,13 641.595,5   " />
<polygon fill="#231F20" points="646.094,5 655.43,12.842 655.861,12.741 646.646,5    " />
<polygon fill="#231F20" points="658.384,10.627 651.687,5 651.135,5 657.954,10.728   " />
<polygon fill="#231F20" points="660.866,8.441 656.772,5 656.221,5 660.437,8.542     " />
<polygon fill="#231F20" points="663.286,6.223 661.831,5 661.278,5 662.856,6.324     " />
<polygon fill="#231F20" points="665.362,5 656.479,13 657.021,13 665.905,5   " />
<polygon fill="#231F20" points="670.503,5 661.616,13 662.161,13 671.046,5   " />
<polygon fill="#231F20" points="676.084,5 675.644,5 666.757,13 667.3,13 676.165,5.018   " />
<polygon fill="#231F20" points="677.803,7.684 671.897,13 672.44,13 677.803,8.172 677.803,8.174 683.548,13 684.099,13

                            677.803,7.711   " />
<polygon fill="#231F20" points="677.803,11.025 675.608,13 676.151,13 677.803,11.514 677.803,11.547 679.532,13 680.083,13

                            677.803,11.085  " />
<polygon fill="#231F20" points="679.208,5 688.731,13 689.282,13 679.76,5    " />
<polygon fill="#231F20" points="684.589,5 694.114,13 694.664,13 685.14,5    " />
<polygon fill="#231F20" points="689.639,5 698.975,12.842 699.406,12.741 690.19,5    " />
<polygon fill="#231F20" points="701.929,10.627 695.231,5 694.68,5 701.499,10.728    " />
<polygon fill="#231F20" points="704.411,8.441 700.317,5 699.766,5 703.981,8.542     " />
<polygon fill="#231F20" points="706.831,6.223 705.376,5 704.823,5 706.401,6.324     " />
<polygon fill="#231F20" points="708.907,5 700.023,13 700.565,13 709.45,5    " />
<polygon fill="#231F20" points="714.048,5 705.161,13 705.706,13 714.591,5   " />
<polygon fill="#231F20" points="719.629,5 719.188,5 710.302,13 710.845,13 719.71,5.018  " />
<polygon fill="#231F20" points="721.348,7.684 715.442,13 715.985,13 721.348,8.172 721.348,8.174 727.093,13 727.644,13

                            721.348,7.711   " />
<polygon fill="#231F20" points="721.348,11.025 719.153,13 719.696,13 721.348,11.514 721.348,11.547 723.077,13 723.628,13

                            721.348,11.085  " />
<polygon fill="#231F20" points="722.753,5 732.276,13 732.827,13 723.305,5   " />
<polygon fill="#231F20" points="728.134,5 737.659,13 738.209,13 728.685,5   " />
<polygon fill="#231F20" points="733.184,5 742.52,12.842 742.951,12.741 733.735,5    " />
<polygon fill="#231F20" points="745.474,10.627 738.776,5 738.225,5 745.044,10.728   " />
<polygon fill="#231F20" points="747.956,8.441 743.862,5 743.311,5 747.526,8.542     " />
<polygon fill="#231F20" points="750.376,6.223 748.921,5 748.368,5 749.946,6.324     " />
<polygon fill="#231F20" points="752.452,5 743.568,13 744.11,13 752.995,5    " />
<polygon fill="#231F20" points="757.593,5 748.706,13 749.251,13 758.136,5   " />
<polygon fill="#231F20" points="763.174,5 762.733,5 753.847,13 754.39,13 763.255,5.018  " />
<polygon fill="#231F20" points="764.893,7.684 758.987,13 759.53,13 764.893,8.172 764.893,8.174 770.638,13 771.188,13

                            764.893,7.711   " />
<polygon fill="#231F20" points="764.893,11.025 762.698,13 763.241,13 764.893,11.514 764.893,11.547 766.622,13 767.173,13

                            764.893,11.085  " />
<polygon fill="#231F20" points="766.298,5 775.821,13 776.372,13 766.85,5    " />
<polygon fill="#231F20" points="771.679,5 781.204,13 781.754,13 772.229,5   " />
<polygon fill="#231F20" points="776.729,5 786.064,12.842 786.496,12.741 777.28,5    " />
<polygon fill="#231F20" points="789.019,10.627 782.321,5 781.77,5 788.589,10.728    " />
<polygon fill="#231F20" points="791.501,8.441 787.407,5 786.855,5 791.071,8.542     " />
<polygon fill="#231F20" points="793.921,6.223 792.466,5 791.913,5 793.491,6.324     " />
<polygon fill="#231F20" points="795.997,5 787.113,13 787.655,13 796.54,5    " />
<polygon fill="#231F20" points="801.138,5 792.251,13 792.796,13 801.681,5   " />
<polygon fill="#231F20" points="806.719,5 806.278,5 797.392,13 797.935,13 806.8,5.018   " />
<polygon fill="#231F20" points="808.438,7.684 802.532,13 803.075,13 808.438,8.172 808.438,8.174 814.183,13 814.733,13

                            808.438,7.711   " />
<polygon fill="#231F20" points="808.438,11.025 806.243,13 806.786,13 808.438,11.514 808.438,11.547 810.167,13 810.718,13

                            808.438,11.085  " />
<polygon fill="#231F20" points="809.843,5 819.366,13 819.917,13 810.395,5   " />
<polygon fill="#231F20" points="815.224,5 824.749,13 825.299,13 815.774,5   " />
<polygon fill="#231F20" points="820.273,5 829.609,12.842 830.041,12.741 820.825,5   " />
<polygon fill="#231F20" points="832.563,10.627 825.866,5 825.314,5 832.134,10.728   " />
<polygon fill="#231F20" points="835.046,8.441 830.952,5 830.4,5 834.616,8.542   " />
<polygon fill="#231F20" points="837.466,6.223 836.011,5 835.458,5 837.036,6.324     " />
<polygon fill="#231F20" points="839.542,5 830.658,13 831.2,13 840.085,5     " />
<polygon fill="#231F20" points="844.683,5 835.796,13 836.341,13 845.226,5   " />
<polygon fill="#231F20" points="850.264,5 849.823,5 840.937,13 841.479,13 850.345,5.018     " />
<polygon fill="#231F20" points="851.982,7.684 846.077,13 846.62,13 851.982,8.172 851.982,8.174 857.728,13 858.278,13

                            851.982,7.711   " />
<polygon fill="#231F20" points="851.982,11.025 849.788,13 850.331,13 851.982,11.514 851.982,11.547 853.712,13 854.263,13

                            851.982,11.085  " />
<polygon fill="#231F20" points="853.388,5 862.911,13 863.462,13 853.939,5   " />
<polygon fill="#231F20" points="858.769,5 868.294,13 868.844,13 859.319,5   " />
<polygon fill="#231F20" points="863.818,5 873.154,12.842 873.586,12.741 864.37,5    " />
<polygon fill="#231F20" points="876.108,10.627 869.411,5 868.859,5 875.679,10.728   " />
<polygon fill="#231F20" points="878.591,8.441 874.497,5 873.945,5 878.161,8.542     " />
<polygon fill="#231F20" points="881.011,6.223 879.556,5 879.003,5 880.581,6.324     " />
<polygon fill="#231F20" points="883.087,5 874.203,13 874.745,13 883.63,5    " />
<polygon fill="#231F20" points="888.228,5 879.341,13 879.886,13 888.771,5   " />
<polygon fill="#231F20" points="893.809,5 893.368,5 884.481,13 885.024,13 893.89,5.018  " />
<polygon fill="#231F20" points="895.527,7.684 889.622,13 890.165,13 895.527,8.172 895.527,8.174 901.272,13 901.823,13

                            895.527,7.711   " />
<polygon fill="#231F20" points="895.527,11.025 893.333,13 893.876,13 895.527,11.514 895.527,11.547 897.257,13 897.808,13

                            895.527,11.085  " />
<polygon fill="#231F20" points="896.933,5 906.456,13 907.007,13 897.484,5   " />
<polygon fill="#231F20" points="902.313,5 911.839,13 912.389,13 902.864,5   " />
<polygon fill="#231F20" points="907.363,5 916.699,12.842 917.131,12.741 907.915,5   " />
<polygon fill="#231F20" points="919.653,10.627 912.956,5 912.404,5 919.224,10.728   " />
<polygon fill="#231F20" points="922.136,8.441 918.042,5 917.49,5 921.706,8.542  " />
<polygon fill="#231F20" points="924.556,6.223 923.101,5 922.548,5 924.126,6.324     " />
<polygon fill="#231F20" points="926.632,5 917.748,13 918.29,13 927.175,5    " />
<polygon fill="#231F20" points="931.772,5 922.886,13 923.431,13 932.315,5   " />
<polygon fill="#231F20" points="937.354,5 936.913,5 928.026,13 928.569,13 937.435,5.018     " />
<polygon fill="#231F20" points="939.072,7.684 933.167,13 933.71,13 939.072,8.172 939.072,8.174 944.817,13 945.368,13

                            939.072,7.711   " />
<polygon fill="#231F20" points="939.072,11.025 936.878,13 937.421,13 939.072,11.514 939.072,11.547 940.802,13 941.353,13

                            939.072,11.085  " />
<polygon fill="#231F20" points="940.478,5 950.001,13 950.552,13 941.029,5   " />
<polygon fill="#231F20" points="945.858,5 955.384,13 955.934,13 946.409,5   " />
<polygon fill="#231F20" points="950.908,5 960.244,12.842 960.676,12.741 951.46,5    " />
<polygon fill="#231F20" points="963.198,10.627 956.501,5 955.949,5 962.769,10.728   " />
<polygon fill="#231F20" points="965.681,8.441 961.587,5 961.035,5 965.251,8.542     " />
<polygon fill="#231F20" points="968.101,6.223 966.646,5 966.093,5 967.671,6.324     " />
<polygon fill="#231F20" points="970.177,5 961.293,13 961.835,13 970.72,5    " />
<polygon fill="#231F20" points="975.317,5 966.431,13 966.976,13 975.86,5    " />
<polygon fill="#231F20" points="980.898,5 980.458,5 971.571,13 972.114,13 980.979,5.018     " />
<polygon fill="#231F20" points="982.617,7.684 976.712,13 977.255,13 982.617,8.172 982.617,8.174 988.362,13 988.913,13

                            982.617,7.711   " />
<polygon fill="#231F20" points="982.617,11.025 980.423,13 980.966,13 982.617,11.514 982.617,11.547 984.347,13 984.897,13

                            982.617,11.085  " />
<polygon fill="#231F20" points="984.022,5 993.546,13 994.097,13 984.574,5   " />
<polygon fill="#231F20" points="989.403,5 998.929,13 999.479,13 989.954,5   " />
<polygon fill="#231F20" points="994.453,5 1003.789,12.842 1004.221,12.741 995.005,5     " />
<polygon fill="#231F20" points="1006.743,10.627 1000.046,5 999.494,5 1006.313,10.728    " />
<polygon fill="#231F20" points="1009.226,8.441 1005.132,5 1004.58,5 1008.796,8.542  " />
<polygon fill="#231F20" points="1011.646,6.223 1010.19,5 1009.638,5 1011.216,6.324  " />
<polygon fill="#231F20" points="1013.722,5 1004.838,13 1005.38,13 1014.265,5    " />
<polygon fill="#231F20" points="1018.862,5 1009.976,13 1010.521,13 1019.405,5   " />
<polygon fill="#231F20" points="1024.443,5 1024.003,5 1015.116,13 1015.659,13 1024.524,5.018    " />
<polygon fill="#231F20" points="1026.162,7.684 1020.257,13 1020.8,13 1026.162,8.172 1026.162,8.174 1031.907,13 1032.458,13

                            1026.162,7.711  " />
<polygon fill="#231F20" points="1026.162,11.025 1023.968,13 1024.511,13 1026.162,11.514 1026.162,11.547 1027.892,13

                            1028.442,13 1026.162,11.085     " />
<polygon fill="#231F20" points="1027.567,5 1037.091,13 1037.642,13 1028.119,5   " />
<polygon fill="#231F20" points="1032.948,5 1042.474,13 1043.023,13 1033.499,5   " />
<polygon fill="#231F20" points="1037.998,5 1047.334,12.842 1047.766,12.741 1038.55,5    " />
<polygon fill="#231F20" points="1050.288,10.627 1043.591,5 1043.039,5 1049.858,10.728   " />
<polygon fill="#231F20" points="1052.771,8.441 1048.677,5 1048.125,5 1052.341,8.542     " />
<polygon fill="#231F20" points="1055.19,6.223 1053.735,5 1053.183,5 1054.761,6.324  " />
<polygon fill="#231F20" points="1057.267,5 1048.383,13 1048.925,13 1057.81,5    " />
<polygon fill="#231F20" points="1062.407,5 1053.521,13 1054.065,13 1062.95,5    " />
<polygon fill="#231F20" points="1067.988,5 1067.548,5 1058.661,13 1059.204,13 1068.069,5.018    " />
<polygon fill="#231F20" points="1069.707,7.684 1063.802,13 1064.345,13 1069.707,8.172 1069.707,8.174 1075.452,13 1076.003,13

                            1069.707,7.711  " />
<polygon fill="#231F20" points="1069.707,11.025 1067.513,13 1068.056,13 1069.707,11.514 1069.707,11.547 1071.437,13

                            1071.987,13 1069.707,11.085     " />
<polygon fill="#231F20" points="1071.112,5 1080.636,13 1081.187,13 1071.664,5   " />
<polygon fill="#231F20" points="1076.493,5 1086.019,13 1086.568,13 1077.044,5   " />
<polygon fill="#231F20" points="1081.543,5 1090.879,12.842 1091.311,12.741 1082.095,5   " />
<polygon fill="#231F20" points="1093.833,10.627 1087.136,5 1086.584,5 1093.403,10.728   " />
<polygon fill="#231F20" points="1096.315,8.441 1092.222,5 1091.67,5 1095.886,8.542  " />
<polygon fill="#231F20" points="1098.735,6.223 1097.28,5 1096.728,5 1098.306,6.324  " />
<polygon fill="#231F20" points="1100.812,5 1091.928,13 1092.47,13 1101.354,5    " />
<polygon fill="#231F20" points="1105.952,5 1097.065,13 1097.61,13 1106.495,5    " />
<polygon fill="#231F20" points="1111.533,5 1111.093,5 1102.206,13 1102.749,13 1111.614,5.018    " />
<polygon fill="#231F20" points="1113.252,7.684 1107.347,13 1107.89,13 1113.252,8.172 1113.252,8.174 1118.997,13 1119.548,13

                            1113.252,7.711  " />
<polygon fill="#231F20" points="1113.252,11.025 1111.058,13 1111.601,13 1113.252,11.514 1113.252,11.547 1114.981,13

                            1115.532,13 1113.252,11.085     " />
<polygon fill="#231F20" points="1114.657,5 1124.181,13 1124.731,13 1115.209,5   " />
<polygon fill="#231F20" points="1120.038,5 1129.563,13 1130.113,13 1120.589,5   " />
<polygon fill="#231F20" points="1125.088,5 1134.424,12.842 1134.855,12.741 1125.64,5    " />
<polygon fill="#231F20" points="1137.378,10.627 1130.681,5 1130.129,5 1136.948,10.728   " />
<polygon fill="#231F20" points="1139.86,8.441 1135.767,5 1135.215,5 1139.431,8.542  " />
<polygon fill="#231F20" points="1142.28,6.223 1140.825,5 1140.272,5 1141.851,6.324  " />
<polygon fill="#231F20" points="1144.356,5 1135.473,13 1136.015,13 1144.899,5   " />
<polygon fill="#231F20" points="1149.497,5 1140.61,13 1141.155,13 1150.04,5     " />
<polygon fill="#231F20" points="1155.078,5 1154.638,5 1145.751,13 1146.294,13 1155.159,5.018    " />
<polygon fill="#231F20" points="1156.797,7.684 1150.892,13 1151.435,13 1156.797,8.172 1156.797,8.174 1162.542,13 1163.093,13

                            1156.797,7.711  " />
<polygon fill="#231F20" points="1156.797,11.025 1154.603,13 1155.146,13 1156.797,11.514 1156.797,11.547 1158.526,13

                            1159.077,13 1156.797,11.085     " />
<polygon fill="#231F20" points="1158.202,5 1167.726,13 1168.276,13 1158.754,5   " />
<polygon fill="#231F20" points="1163.583,5 1173.108,13 1173.658,13 1164.134,5   " />
<polygon fill="#231F20" points="1168.633,5 1177.969,12.842 1178.4,12.741 1169.185,5     " />
<polygon fill="#231F20" points="1180.923,10.627 1174.226,5 1173.674,5 1180.493,10.728   " />
<polygon fill="#231F20" points="1183.405,8.441 1179.312,5 1178.76,5 1182.976,8.542  " />
<polygon fill="#231F20" points="1185.825,6.223 1184.37,5 1183.817,5 1185.396,6.324  " />
<polygon fill="#231F20" points="1187.901,5 1179.018,13 1179.56,13 1188.444,5    " />
<polygon fill="#231F20" points="1193.042,5 1184.155,13 1184.7,13 1193.585,5     " />
<polygon fill="#231F20" points="1189.296,13 1189.839,13 1194.629,8.688 1194.629,8.199   " />
<polygon fill="#231F20" points="1194.629,13 1194.629,12.827 1194.437,13     " />
</g>
</svg></div>
<div class="clear"></div>
</div>
</div>
</div>
<div id="post-307" class="post-307 post type-post status-publish format-standard has-post-thumbnail hentry category-my-story tag-blogger tag-look tag-outfit tag-travel col-md-6" itemscope itemtype="http://schema.org/NewsArticle" itemprop="blogPost" role="article">
<div class="df-post-wrapper">
<div class="clear"></div>
<div class="featured-media  filter_bw"><a href="../../things-that-ive-learned-in-texas/index.html"><img width="600" height="600" src="../../wp-content/uploads/sites/26/2015/10/menswear_59-600x600.jpg" class="attachment-loop-blog size-loop-blog wp-post-image" alt="" srcset="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/10/menswear_59-600x600.jpg 600w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/10/menswear_59-150x150.jpg 150w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/10/menswear_59-250x250.jpg 250w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/10/menswear_59-80x80.jpg 80w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/10/menswear_59-330x330.jpg 330w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/10/menswear_59-160x160.jpg 160w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/10/menswear_59-293x293.jpg 293w" sizes="(max-width: 600px) 100vw, 600px" data-attachment-id="546" data-permalink="http://dahz.daffyhazan.com/applique/menswear/become-a-rockstar/menswear_59/" data-orig-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/10/menswear_59.jpg" data-orig-size="1200,857" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="menswear_59" data-image-description="" data-medium-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/10/menswear_59-300x214.jpg" data-large-file="../../wp-content/uploads/sites/26/2015/10/menswear_59-1024x731.jpg" /></a></div>
<div class="grid-outer">
<div class="grid-wrapper">
<div class="df-post-title  aligncenter" onclick="void(0)"><div class="df-post-title-inner"><div class="df-single-category"><span class="entry-terms category" itemprop="articleSection"><a href="index.html" rel="category tag">My Story</a></span></div><h2 class="entry-title " itemprop="headline"><a href="../../things-that-ive-learned-in-texas/index.html" title="Things That I&#8217;ve Learned in Texas">Things That I&#8217;ve Learned in Texas</a></h2><div class="df-post-on"><time class="entry-published updated" datetime="2015-12-11T06:58:58+00:00" title="Friday, December 11, 2015, 6:58 am"><a href="../../2015/12/11/index.html"><meta itemprop="datePublished" content="December 11, 2015">December 11, 2015</a></time></div></div></div>
<div class="entry-summary" itemprop="description">
<p>Applique is a fully responsive Fashion blog theme that designed with style publishers in mind! We keep things simple and elegant to make sure any fashion blogger—even those without IT background, can use it. We believe that a blog theme, should feel fluid, light, and intuitive. That’s what we are aiming to make with applique, &hellip; <a class="more-link" href="../../things-that-ive-learned-in-texas/index.html">Continue Reading<i class="ion-ios-arrow-thin-right"></i></a></p>
</div>
</div>
<div class="clear"></div>
<div class="df-postmeta-wrapper"><div class="df-postmeta border-top"><div class="clear"></div><div class="col-left alignleft"><span itemtype="http://schema.org/Comment" itemscope="itemscope" itemprop="comment"><a class="comment-permalink perma" href="../../things-that-ive-learned-in-texas/index.html#respond" itemprop="url">No Comments</a></span></div><div class="col-right alignright"><ul class="df-share"><li><span>Share</span></li><li><a class="df-facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http://dahz.daffyhazan.com/applique/menswear/things-that-ive-learned-in-texas/"><i class="fa fa-facebook"></i><i class="fa fa-facebook"></i></a></li><li><a class="df-twitter" target="_blank" href="https://twitter.com/home?status=Check%20out%20this%20article:%20Things+That+I%26%238217%3Bve+Learned+in+Texas%20-%20http://dahz.daffyhazan.com/applique/menswear/things-that-ive-learned-in-texas/"><i class="fa fa-twitter"></i><i class="fa fa-twitter"></i></a></li><li><a class="df-google" target="_blank" href="https://plus.google.com/share?url=http://dahz.daffyhazan.com/applique/menswear/things-that-ive-learned-in-texas/"><i class="fa fa-google-plus"></i><i class="fa fa-google-plus"></i></a></li><li><a class="df-pinterest" target="_blank" href="https://pinterest.com/pin/create/button/?url=http://dahz.daffyhazan.com/applique/menswear/things-that-ive-learned-in-texas/&amp;media=http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/10/menswear_59.jpg&amp;description=Things+That+I%26%238217%3Bve+Learned+in+Texas"><i class="fa fa-pinterest"></i><i class="fa fa-pinterest"></i></a></li></ul></div><div class="clear"></div></div><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="8px" viewBox="550 5 100 8">
<g>
<polygon fill="#231F20" points="24.629,8.174 30.374,13 30.925,13 24.629,7.711   " />
<polygon fill="#231F20" points="26.034,5 35.558,13 36.108,13 26.586,5   " />
<polygon fill="#231F20" points="31.415,5 40.94,13 41.489,13 31.966,5    " />
<polygon fill="#231F20" points="36.465,5 45.801,12.842 46.231,12.741 37.017,5   " />
<polygon fill="#231F20" points="48.755,10.627 42.058,5 41.506,5 48.325,10.728   " />
<polygon fill="#231F20" points="51.237,8.441 47.144,5 46.592,5 50.808,8.542     " />
<polygon fill="#231F20" points="53.657,6.223 52.202,5 51.649,5 53.228,6.324     " />
<polygon fill="#231F20" points="55.733,5 46.849,13 47.392,13 56.276,5   " />
<polygon fill="#231F20" points="60.874,5 51.987,13 52.532,13 61.417,5   " />
<polygon fill="#231F20" points="66.455,5 66.015,5 57.128,13 57.671,13 66.536,5.018  " />
<polygon fill="#231F20" points="68.174,7.684 62.269,13 62.812,13 68.174,8.172 68.174,8.174 73.919,13 74.47,13 68.174,7.711  " />
<polygon fill="#231F20" points="24.629,11.547 26.358,13 26.909,13 24.629,11.085     " />
<polygon fill="#231F20" points="68.174,11.025 65.979,13 66.522,13 68.174,11.514 68.174,11.547 69.903,13 70.454,13

                            68.174,11.085   " />
<polygon fill="#231F20" points="69.579,5 79.103,13 79.653,13 70.131,5   " />
<polygon fill="#231F20" points="74.96,5 84.485,13 85.035,13 75.511,5    " />
<polygon fill="#231F20" points="80.01,5 89.346,12.842 89.777,12.741 80.562,5    " />
<polygon fill="#231F20" points="92.3,10.627 85.603,5 85.051,5 91.87,10.728  " />
<polygon fill="#231F20" points="94.782,8.441 90.688,5 90.137,5 94.353,8.542     " />
<polygon fill="#231F20" points="97.202,6.223 95.747,5 95.194,5 96.772,6.324     " />
<polygon fill="#231F20" points="99.278,5 90.395,13 90.937,13 99.821,5   " />
<polygon fill="#231F20" points="104.419,5 95.532,13 96.077,13 104.962,5     " />
<polygon fill="#231F20" points="110,5 109.56,5 100.673,13 101.216,13 110.081,5.018  " />
<polygon fill="#231F20" points="111.719,7.684 105.813,13 106.356,13 111.719,8.172 111.719,8.174 117.464,13 118.015,13

                            111.719,7.711   " />
<polygon fill="#231F20" points="111.719,11.025 109.524,13 110.067,13 111.719,11.514 111.719,11.547 113.448,13 113.999,13

                            111.719,11.085  " />
<polygon fill="#231F20" points="113.124,5 122.647,13 123.198,13 113.676,5   " />
<polygon fill="#231F20" points="118.505,5 128.03,13 128.58,13 119.056,5     " />
<polygon fill="#231F20" points="123.555,5 132.891,12.842 133.322,12.741 124.106,5   " />
<polygon fill="#231F20" points="135.845,10.627 129.147,5 128.596,5 135.415,10.728   " />
<polygon fill="#231F20" points="138.327,8.441 134.233,5 133.682,5 137.897,8.542     " />
<polygon fill="#231F20" points="140.747,6.223 139.292,5 138.739,5 140.317,6.324     " />
<polygon fill="#231F20" points="142.823,5 133.939,13 134.481,13 143.366,5   " />
<polygon fill="#231F20" points="147.964,5 139.077,13 139.622,13 148.507,5   " />
<polygon fill="#231F20" points="153.545,5 153.104,5 144.218,13 144.761,13 153.626,5.018     " />
<polygon fill="#231F20" points="155.264,7.684 149.358,13 149.901,13 155.264,8.172 155.264,8.174 161.009,13 161.56,13

                            155.264,7.711   " />
<polygon fill="#231F20" points="155.264,11.025 153.069,13 153.612,13 155.264,11.514 155.264,11.547 156.993,13 157.544,13

                            155.264,11.085  " />
<polygon fill="#231F20" points="156.669,5 166.192,13 166.743,13 157.221,5   " />
<polygon fill="#231F20" points="162.05,5 171.575,13 172.125,13 162.601,5    " />
<polygon fill="#231F20" points="167.1,5 176.436,12.842 176.867,12.741 167.651,5     " />
<polygon fill="#231F20" points="179.39,10.627 172.692,5 172.141,5 178.96,10.728     " />
<polygon fill="#231F20" points="181.872,8.441 177.778,5 177.227,5 181.442,8.542     " />
<polygon fill="#231F20" points="184.292,6.223 182.837,5 182.284,5 183.862,6.324     " />
<polygon fill="#231F20" points="186.368,5 177.484,13 178.026,13 186.911,5   " />
<polygon fill="#231F20" points="191.509,5 182.622,13 183.167,13 192.052,5   " />
<polygon fill="#231F20" points="197.09,5 196.649,5 187.763,13 188.306,13 197.171,5.018  " />
<polygon fill="#231F20" points="198.809,7.684 192.903,13 193.446,13 198.809,8.172 198.809,8.174 204.554,13 205.104,13

                            198.809,7.711   " />
<polygon fill="#231F20" points="198.809,11.025 196.614,13 197.157,13 198.809,11.514 198.809,11.547 200.538,13 201.089,13

                            198.809,11.085  " />
<polygon fill="#231F20" points="200.214,5 209.737,13 210.288,13 200.766,5   " />
<polygon fill="#231F20" points="205.595,5 215.12,13 215.67,13 206.146,5     " />
<polygon fill="#231F20" points="210.645,5 219.98,12.842 220.412,12.741 211.196,5    " />
<polygon fill="#231F20" points="222.935,10.627 216.237,5 215.686,5 222.505,10.728   " />
<polygon fill="#231F20" points="225.417,8.441 221.323,5 220.771,5 224.987,8.542     " />
<polygon fill="#231F20" points="227.837,6.223 226.382,5 225.829,5 227.407,6.324     " />
<polygon fill="#231F20" points="229.913,5 221.029,13 221.571,13 230.456,5   " />
<polygon fill="#231F20" points="235.054,5 226.167,13 226.712,13 235.597,5   " />
<polygon fill="#231F20" points="240.635,5 240.194,5 231.308,13 231.851,13 240.716,5.018     " />
<polygon fill="#231F20" points="242.354,7.684 236.448,13 236.991,13 242.354,8.172 242.354,8.174 248.099,13 248.649,13

                            242.354,7.711   " />
<polygon fill="#231F20" points="242.354,11.025 240.159,13 240.702,13 242.354,11.514 242.354,11.547 244.083,13 244.634,13

                            242.354,11.085  " />
<polygon fill="#231F20" points="243.759,5 253.282,13 253.833,13 244.311,5   " />
<polygon fill="#231F20" points="249.14,5 258.665,13 259.215,13 249.69,5     " />
<polygon fill="#231F20" points="254.189,5 263.525,12.842 263.957,12.741 254.741,5   " />
<polygon fill="#231F20" points="266.479,10.627 259.782,5 259.23,5 266.05,10.728     " />
<polygon fill="#231F20" points="268.962,8.441 264.868,5 264.316,5 268.532,8.542     " />
<polygon fill="#231F20" points="271.382,6.223 269.927,5 269.374,5 270.952,6.324     " />
<polygon fill="#231F20" points="273.458,5 264.574,13 265.116,13 274.001,5   " />
<polygon fill="#231F20" points="278.599,5 269.712,13 270.257,13 279.142,5   " />
<polygon fill="#231F20" points="284.18,5 283.739,5 274.853,13 275.396,13 284.261,5.018  " />
<polygon fill="#231F20" points="285.898,7.684 279.993,13 280.536,13 285.898,8.172 285.898,8.174 291.644,13 292.194,13

                            285.898,7.711   " />
<polygon fill="#231F20" points="285.898,11.025 283.704,13 284.247,13 285.898,11.514 285.898,11.547 287.628,13 288.179,13

                            285.898,11.085  " />
<polygon fill="#231F20" points="287.304,5 296.827,13 297.378,13 287.855,5   " />
<polygon fill="#231F20" points="292.685,5 302.21,13 302.76,13 293.235,5     " />
<polygon fill="#231F20" points="297.734,5 307.07,12.842 307.502,12.741 298.286,5    " />
<polygon fill="#231F20" points="310.024,10.627 303.327,5 302.775,5 309.595,10.728   " />
<polygon fill="#231F20" points="312.507,8.441 308.413,5 307.861,5 312.077,8.542     " />
<polygon fill="#231F20" points="314.927,6.223 313.472,5 312.919,5 314.497,6.324     " />
<polygon fill="#231F20" points="317.003,5 308.119,13 308.661,13 317.546,5   " />
<polygon fill="#231F20" points="322.144,5 313.257,13 313.802,13 322.687,5   " />
<polygon fill="#231F20" points="327.725,5 327.284,5 318.397,13 318.94,13 327.806,5.018  " />
<polygon fill="#231F20" points="329.443,7.684 323.538,13 324.081,13 329.443,8.172 329.443,8.174 335.188,13 335.739,13

                            329.443,7.711   " />
<polygon fill="#231F20" points="329.443,11.025 327.249,13 327.792,13 329.443,11.514 329.443,11.547 331.173,13 331.724,13

                            329.443,11.085  " />
<polygon fill="#231F20" points="330.849,5 340.372,13 340.923,13 331.4,5     " />
<polygon fill="#231F20" points="336.229,5 345.755,13 346.305,13 336.78,5    " />
<polygon fill="#231F20" points="341.279,5 350.615,12.842 351.047,12.741 341.831,5   " />
<polygon fill="#231F20" points="353.569,10.627 346.872,5 346.32,5 353.14,10.728     " />
<polygon fill="#231F20" points="356.052,8.441 351.958,5 351.406,5 355.622,8.542     " />
<polygon fill="#231F20" points="358.472,6.223 357.017,5 356.464,5 358.042,6.324     " />
<polygon fill="#231F20" points="360.548,5 351.664,13 352.206,13 361.091,5   " />
<polygon fill="#231F20" points="365.688,5 356.802,13 357.347,13 366.231,5   " />
<polygon fill="#231F20" points="371.27,5 370.829,5 361.942,13 362.485,13 371.351,5.018  " />
<polygon fill="#231F20" points="372.988,7.684 367.083,13 367.626,13 372.988,8.172 372.988,8.174 378.733,13 379.284,13

                            372.988,7.711   " />
<polygon fill="#231F20" points="372.988,11.025 370.794,13 371.337,13 372.988,11.514 372.988,11.547 374.718,13 375.269,13

                            372.988,11.085  " />
<polygon fill="#231F20" points="374.394,5 383.917,13 384.468,13 374.945,5   " />
<polygon fill="#231F20" points="379.774,5 389.3,13 389.85,13 380.325,5  " />
<polygon fill="#231F20" points="384.824,5 394.16,12.842 394.592,12.741 385.376,5    " />
<polygon fill="#231F20" points="397.114,10.627 390.417,5 389.865,5 396.685,10.728   " />
<polygon fill="#231F20" points="399.597,8.441 395.503,5 394.951,5 399.167,8.542     " />
<polygon fill="#231F20" points="402.017,6.223 400.562,5 400.009,5 401.587,6.324     " />
<polygon fill="#231F20" points="404.093,5 395.209,13 395.751,13 404.636,5   " />
<polygon fill="#231F20" points="409.233,5 400.347,13 400.892,13 409.776,5   " />
<polygon fill="#231F20" points="414.814,5 414.374,5 405.487,13 406.03,13 414.896,5.018  " />
<polygon fill="#231F20" points="416.533,7.684 410.628,13 411.171,13 416.533,8.172 416.533,8.174 422.278,13 422.829,13

                            416.533,7.711   " />
<polygon fill="#231F20" points="416.533,11.025 414.339,13 414.882,13 416.533,11.514 416.533,11.547 418.263,13 418.813,13

                            416.533,11.085  " />
<polygon fill="#231F20" points="417.938,5 427.462,13 428.013,13 418.49,5    " />
<polygon fill="#231F20" points="423.319,5 432.845,13 433.395,13 423.87,5    " />
<polygon fill="#231F20" points="428.369,5 437.705,12.842 438.137,12.741 428.921,5   " />
<polygon fill="#231F20" points="440.659,10.627 433.962,5 433.41,5 440.229,10.728    " />
<polygon fill="#231F20" points="443.142,8.441 439.048,5 438.496,5 442.712,8.542     " />
<polygon fill="#231F20" points="445.562,6.223 444.106,5 443.554,5 445.132,6.324     " />
<polygon fill="#231F20" points="447.638,5 438.754,13 439.296,13 448.181,5   " />
<polygon fill="#231F20" points="452.778,5 443.892,13 444.437,13 453.321,5   " />
<polygon fill="#231F20" points="458.359,5 457.919,5 449.032,13 449.575,13 458.44,5.018  " />
<polygon fill="#231F20" points="460.078,7.684 454.173,13 454.716,13 460.078,8.172 460.078,8.174 465.823,13 466.374,13

                            460.078,7.711   " />
<polygon fill="#231F20" points="460.078,11.025 457.884,13 458.427,13 460.078,11.514 460.078,11.547 461.808,13 462.358,13

                            460.078,11.085  " />
<polygon fill="#231F20" points="461.483,5 471.007,13 471.558,13 462.035,5   " />
<polygon fill="#231F20" points="466.864,5 476.39,13 476.939,13 467.415,5    " />
<polygon fill="#231F20" points="471.914,5 481.25,12.842 481.682,12.741 472.466,5    " />
<polygon fill="#231F20" points="484.204,10.627 477.507,5 476.955,5 483.774,10.728   " />
<polygon fill="#231F20" points="486.687,8.441 482.593,5 482.041,5 486.257,8.542     " />
<polygon fill="#231F20" points="489.106,6.223 487.651,5 487.099,5 488.677,6.324     " />
<polygon fill="#231F20" points="491.183,5 482.299,13 482.841,13 491.726,5   " />
<polygon fill="#231F20" points="496.323,5 487.437,13 487.981,13 496.866,5   " />
<polygon fill="#231F20" points="501.904,5 501.464,5 492.577,13 493.12,13 501.985,5.018  " />
<polygon fill="#231F20" points="503.623,7.684 497.718,13 498.261,13 503.623,8.172 503.623,8.174 509.368,13 509.919,13

                            503.623,7.711   " />
<polygon fill="#231F20" points="503.623,11.025 501.429,13 501.972,13 503.623,11.514 503.623,11.547 505.353,13 505.903,13

                            503.623,11.085  " />
<polygon fill="#231F20" points="505.028,5 514.552,13 515.103,13 505.58,5    " />
<polygon fill="#231F20" points="510.409,5 519.935,13 520.484,13 510.96,5    " />
<polygon fill="#231F20" points="515.459,5 524.795,12.842 525.227,12.741 516.011,5   " />
<polygon fill="#231F20" points="527.749,10.627 521.052,5 520.5,5 527.319,10.728     " />
<polygon fill="#231F20" points="530.231,8.441 526.138,5 525.586,5 529.802,8.542     " />
<polygon fill="#231F20" points="532.651,6.223 531.196,5 530.644,5 532.222,6.324     " />
<polygon fill="#231F20" points="534.728,5 525.844,13 526.386,13 535.271,5   " />
<polygon fill="#231F20" points="539.868,5 530.981,13 531.526,13 540.411,5   " />
<polygon fill="#231F20" points="545.449,5 545.009,5 536.122,13 536.665,13 545.53,5.018  " />
<polygon fill="#231F20" points="547.168,7.684 541.263,13 541.806,13 547.168,8.172 547.168,8.174 552.913,13 553.464,13

                            547.168,7.711   " />
<polygon fill="#231F20" points="547.168,11.025 544.974,13 545.517,13 547.168,11.514 547.168,11.547 548.897,13 549.448,13

                            547.168,11.085  " />
<polygon fill="#231F20" points="548.573,5 558.097,13 558.647,13 549.125,5   " />
<polygon fill="#231F20" points="553.954,5 563.479,13 564.029,13 554.505,5   " />
<polygon fill="#231F20" points="559.004,5 568.34,12.842 568.771,12.741 559.556,5    " />
<polygon fill="#231F20" points="571.294,10.627 564.597,5 564.045,5 570.864,10.728   " />
<polygon fill="#231F20" points="573.776,8.441 569.683,5 569.131,5 573.347,8.542     " />
<polygon fill="#231F20" points="576.196,6.223 574.741,5 574.188,5 575.767,6.324     " />
<polygon fill="#231F20" points="578.272,5 569.389,13 569.931,13 578.815,5   " />
<polygon fill="#231F20" points="583.413,5 574.526,13 575.071,13 583.956,5   " />
<polygon fill="#231F20" points="588.994,5 588.554,5 579.667,13 580.21,13 589.075,5.018  " />
<polygon fill="#231F20" points="590.713,7.684 584.808,13 585.351,13 590.713,8.172 590.713,8.174 596.458,13 597.009,13

                            590.713,7.711   " />
<polygon fill="#231F20" points="590.713,11.025 588.519,13 589.062,13 590.713,11.514 590.713,11.547 592.442,13 592.993,13

                            590.713,11.085  " />
<polygon fill="#231F20" points="592.118,5 601.642,13 602.192,13 592.67,5    " />
<polygon fill="#231F20" points="597.499,5 607.024,13 607.574,13 598.05,5    " />
<polygon fill="#231F20" points="602.549,5 611.885,12.842 612.316,12.741 603.101,5   " />
<polygon fill="#231F20" points="614.839,10.627 608.142,5 607.59,5 614.409,10.728    " />
<polygon fill="#231F20" points="617.321,8.441 613.228,5 612.676,5 616.892,8.542     " />
<polygon fill="#231F20" points="619.741,6.223 618.286,5 617.733,5 619.312,6.324     " />
<polygon fill="#231F20" points="621.817,5 612.934,13 613.476,13 622.36,5    " />
<polygon fill="#231F20" points="626.958,5 618.071,13 618.616,13 627.501,5   " />
<polygon fill="#231F20" points="632.539,5 632.099,5 623.212,13 623.755,13 632.62,5.018  " />
<polygon fill="#231F20" points="634.258,7.684 628.353,13 628.896,13 634.258,8.172 634.258,8.174 640.003,13 640.554,13

                            634.258,7.711   " />
<polygon fill="#231F20" points="634.258,11.025 632.063,13 632.606,13 634.258,11.514 634.258,11.547 635.987,13 636.538,13

                            634.258,11.085  " />
<polygon fill="#231F20" points="635.663,5 645.187,13 645.737,13 636.215,5   " />
<polygon fill="#231F20" points="641.044,5 650.569,13 651.119,13 641.595,5   " />
<polygon fill="#231F20" points="646.094,5 655.43,12.842 655.861,12.741 646.646,5    " />
<polygon fill="#231F20" points="658.384,10.627 651.687,5 651.135,5 657.954,10.728   " />
<polygon fill="#231F20" points="660.866,8.441 656.772,5 656.221,5 660.437,8.542     " />
<polygon fill="#231F20" points="663.286,6.223 661.831,5 661.278,5 662.856,6.324     " />
<polygon fill="#231F20" points="665.362,5 656.479,13 657.021,13 665.905,5   " />
<polygon fill="#231F20" points="670.503,5 661.616,13 662.161,13 671.046,5   " />
<polygon fill="#231F20" points="676.084,5 675.644,5 666.757,13 667.3,13 676.165,5.018   " />
<polygon fill="#231F20" points="677.803,7.684 671.897,13 672.44,13 677.803,8.172 677.803,8.174 683.548,13 684.099,13

                            677.803,7.711   " />
<polygon fill="#231F20" points="677.803,11.025 675.608,13 676.151,13 677.803,11.514 677.803,11.547 679.532,13 680.083,13

                            677.803,11.085  " />
<polygon fill="#231F20" points="679.208,5 688.731,13 689.282,13 679.76,5    " />
<polygon fill="#231F20" points="684.589,5 694.114,13 694.664,13 685.14,5    " />
<polygon fill="#231F20" points="689.639,5 698.975,12.842 699.406,12.741 690.19,5    " />
<polygon fill="#231F20" points="701.929,10.627 695.231,5 694.68,5 701.499,10.728    " />
<polygon fill="#231F20" points="704.411,8.441 700.317,5 699.766,5 703.981,8.542     " />
<polygon fill="#231F20" points="706.831,6.223 705.376,5 704.823,5 706.401,6.324     " />
<polygon fill="#231F20" points="708.907,5 700.023,13 700.565,13 709.45,5    " />
<polygon fill="#231F20" points="714.048,5 705.161,13 705.706,13 714.591,5   " />
<polygon fill="#231F20" points="719.629,5 719.188,5 710.302,13 710.845,13 719.71,5.018  " />
<polygon fill="#231F20" points="721.348,7.684 715.442,13 715.985,13 721.348,8.172 721.348,8.174 727.093,13 727.644,13

                            721.348,7.711   " />
<polygon fill="#231F20" points="721.348,11.025 719.153,13 719.696,13 721.348,11.514 721.348,11.547 723.077,13 723.628,13

                            721.348,11.085  " />
<polygon fill="#231F20" points="722.753,5 732.276,13 732.827,13 723.305,5   " />
<polygon fill="#231F20" points="728.134,5 737.659,13 738.209,13 728.685,5   " />
<polygon fill="#231F20" points="733.184,5 742.52,12.842 742.951,12.741 733.735,5    " />
<polygon fill="#231F20" points="745.474,10.627 738.776,5 738.225,5 745.044,10.728   " />
<polygon fill="#231F20" points="747.956,8.441 743.862,5 743.311,5 747.526,8.542     " />
<polygon fill="#231F20" points="750.376,6.223 748.921,5 748.368,5 749.946,6.324     " />
<polygon fill="#231F20" points="752.452,5 743.568,13 744.11,13 752.995,5    " />
<polygon fill="#231F20" points="757.593,5 748.706,13 749.251,13 758.136,5   " />
<polygon fill="#231F20" points="763.174,5 762.733,5 753.847,13 754.39,13 763.255,5.018  " />
<polygon fill="#231F20" points="764.893,7.684 758.987,13 759.53,13 764.893,8.172 764.893,8.174 770.638,13 771.188,13

                            764.893,7.711   " />
<polygon fill="#231F20" points="764.893,11.025 762.698,13 763.241,13 764.893,11.514 764.893,11.547 766.622,13 767.173,13

                            764.893,11.085  " />
<polygon fill="#231F20" points="766.298,5 775.821,13 776.372,13 766.85,5    " />
<polygon fill="#231F20" points="771.679,5 781.204,13 781.754,13 772.229,5   " />
<polygon fill="#231F20" points="776.729,5 786.064,12.842 786.496,12.741 777.28,5    " />
<polygon fill="#231F20" points="789.019,10.627 782.321,5 781.77,5 788.589,10.728    " />
<polygon fill="#231F20" points="791.501,8.441 787.407,5 786.855,5 791.071,8.542     " />
<polygon fill="#231F20" points="793.921,6.223 792.466,5 791.913,5 793.491,6.324     " />
<polygon fill="#231F20" points="795.997,5 787.113,13 787.655,13 796.54,5    " />
<polygon fill="#231F20" points="801.138,5 792.251,13 792.796,13 801.681,5   " />
<polygon fill="#231F20" points="806.719,5 806.278,5 797.392,13 797.935,13 806.8,5.018   " />
<polygon fill="#231F20" points="808.438,7.684 802.532,13 803.075,13 808.438,8.172 808.438,8.174 814.183,13 814.733,13

                            808.438,7.711   " />
<polygon fill="#231F20" points="808.438,11.025 806.243,13 806.786,13 808.438,11.514 808.438,11.547 810.167,13 810.718,13

                            808.438,11.085  " />
<polygon fill="#231F20" points="809.843,5 819.366,13 819.917,13 810.395,5   " />
<polygon fill="#231F20" points="815.224,5 824.749,13 825.299,13 815.774,5   " />
<polygon fill="#231F20" points="820.273,5 829.609,12.842 830.041,12.741 820.825,5   " />
<polygon fill="#231F20" points="832.563,10.627 825.866,5 825.314,5 832.134,10.728   " />
<polygon fill="#231F20" points="835.046,8.441 830.952,5 830.4,5 834.616,8.542   " />
<polygon fill="#231F20" points="837.466,6.223 836.011,5 835.458,5 837.036,6.324     " />
<polygon fill="#231F20" points="839.542,5 830.658,13 831.2,13 840.085,5     " />
<polygon fill="#231F20" points="844.683,5 835.796,13 836.341,13 845.226,5   " />
<polygon fill="#231F20" points="850.264,5 849.823,5 840.937,13 841.479,13 850.345,5.018     " />
<polygon fill="#231F20" points="851.982,7.684 846.077,13 846.62,13 851.982,8.172 851.982,8.174 857.728,13 858.278,13

                            851.982,7.711   " />
<polygon fill="#231F20" points="851.982,11.025 849.788,13 850.331,13 851.982,11.514 851.982,11.547 853.712,13 854.263,13

                            851.982,11.085  " />
<polygon fill="#231F20" points="853.388,5 862.911,13 863.462,13 853.939,5   " />
<polygon fill="#231F20" points="858.769,5 868.294,13 868.844,13 859.319,5   " />
<polygon fill="#231F20" points="863.818,5 873.154,12.842 873.586,12.741 864.37,5    " />
<polygon fill="#231F20" points="876.108,10.627 869.411,5 868.859,5 875.679,10.728   " />
<polygon fill="#231F20" points="878.591,8.441 874.497,5 873.945,5 878.161,8.542     " />
<polygon fill="#231F20" points="881.011,6.223 879.556,5 879.003,5 880.581,6.324     " />
<polygon fill="#231F20" points="883.087,5 874.203,13 874.745,13 883.63,5    " />
<polygon fill="#231F20" points="888.228,5 879.341,13 879.886,13 888.771,5   " />
<polygon fill="#231F20" points="893.809,5 893.368,5 884.481,13 885.024,13 893.89,5.018  " />
<polygon fill="#231F20" points="895.527,7.684 889.622,13 890.165,13 895.527,8.172 895.527,8.174 901.272,13 901.823,13

                            895.527,7.711   " />
<polygon fill="#231F20" points="895.527,11.025 893.333,13 893.876,13 895.527,11.514 895.527,11.547 897.257,13 897.808,13

                            895.527,11.085  " />
<polygon fill="#231F20" points="896.933,5 906.456,13 907.007,13 897.484,5   " />
<polygon fill="#231F20" points="902.313,5 911.839,13 912.389,13 902.864,5   " />
<polygon fill="#231F20" points="907.363,5 916.699,12.842 917.131,12.741 907.915,5   " />
<polygon fill="#231F20" points="919.653,10.627 912.956,5 912.404,5 919.224,10.728   " />
<polygon fill="#231F20" points="922.136,8.441 918.042,5 917.49,5 921.706,8.542  " />
<polygon fill="#231F20" points="924.556,6.223 923.101,5 922.548,5 924.126,6.324     " />
<polygon fill="#231F20" points="926.632,5 917.748,13 918.29,13 927.175,5    " />
<polygon fill="#231F20" points="931.772,5 922.886,13 923.431,13 932.315,5   " />
<polygon fill="#231F20" points="937.354,5 936.913,5 928.026,13 928.569,13 937.435,5.018     " />
<polygon fill="#231F20" points="939.072,7.684 933.167,13 933.71,13 939.072,8.172 939.072,8.174 944.817,13 945.368,13

                            939.072,7.711   " />
<polygon fill="#231F20" points="939.072,11.025 936.878,13 937.421,13 939.072,11.514 939.072,11.547 940.802,13 941.353,13

                            939.072,11.085  " />
<polygon fill="#231F20" points="940.478,5 950.001,13 950.552,13 941.029,5   " />
<polygon fill="#231F20" points="945.858,5 955.384,13 955.934,13 946.409,5   " />
<polygon fill="#231F20" points="950.908,5 960.244,12.842 960.676,12.741 951.46,5    " />
<polygon fill="#231F20" points="963.198,10.627 956.501,5 955.949,5 962.769,10.728   " />
<polygon fill="#231F20" points="965.681,8.441 961.587,5 961.035,5 965.251,8.542     " />
<polygon fill="#231F20" points="968.101,6.223 966.646,5 966.093,5 967.671,6.324     " />
<polygon fill="#231F20" points="970.177,5 961.293,13 961.835,13 970.72,5    " />
<polygon fill="#231F20" points="975.317,5 966.431,13 966.976,13 975.86,5    " />
<polygon fill="#231F20" points="980.898,5 980.458,5 971.571,13 972.114,13 980.979,5.018     " />
<polygon fill="#231F20" points="982.617,7.684 976.712,13 977.255,13 982.617,8.172 982.617,8.174 988.362,13 988.913,13

                            982.617,7.711   " />
<polygon fill="#231F20" points="982.617,11.025 980.423,13 980.966,13 982.617,11.514 982.617,11.547 984.347,13 984.897,13

                            982.617,11.085  " />
<polygon fill="#231F20" points="984.022,5 993.546,13 994.097,13 984.574,5   " />
<polygon fill="#231F20" points="989.403,5 998.929,13 999.479,13 989.954,5   " />
<polygon fill="#231F20" points="994.453,5 1003.789,12.842 1004.221,12.741 995.005,5     " />
<polygon fill="#231F20" points="1006.743,10.627 1000.046,5 999.494,5 1006.313,10.728    " />
<polygon fill="#231F20" points="1009.226,8.441 1005.132,5 1004.58,5 1008.796,8.542  " />
<polygon fill="#231F20" points="1011.646,6.223 1010.19,5 1009.638,5 1011.216,6.324  " />
<polygon fill="#231F20" points="1013.722,5 1004.838,13 1005.38,13 1014.265,5    " />
<polygon fill="#231F20" points="1018.862,5 1009.976,13 1010.521,13 1019.405,5   " />
<polygon fill="#231F20" points="1024.443,5 1024.003,5 1015.116,13 1015.659,13 1024.524,5.018    " />
<polygon fill="#231F20" points="1026.162,7.684 1020.257,13 1020.8,13 1026.162,8.172 1026.162,8.174 1031.907,13 1032.458,13

                            1026.162,7.711  " />
<polygon fill="#231F20" points="1026.162,11.025 1023.968,13 1024.511,13 1026.162,11.514 1026.162,11.547 1027.892,13

                            1028.442,13 1026.162,11.085     " />
<polygon fill="#231F20" points="1027.567,5 1037.091,13 1037.642,13 1028.119,5   " />
<polygon fill="#231F20" points="1032.948,5 1042.474,13 1043.023,13 1033.499,5   " />
<polygon fill="#231F20" points="1037.998,5 1047.334,12.842 1047.766,12.741 1038.55,5    " />
<polygon fill="#231F20" points="1050.288,10.627 1043.591,5 1043.039,5 1049.858,10.728   " />
<polygon fill="#231F20" points="1052.771,8.441 1048.677,5 1048.125,5 1052.341,8.542     " />
<polygon fill="#231F20" points="1055.19,6.223 1053.735,5 1053.183,5 1054.761,6.324  " />
<polygon fill="#231F20" points="1057.267,5 1048.383,13 1048.925,13 1057.81,5    " />
<polygon fill="#231F20" points="1062.407,5 1053.521,13 1054.065,13 1062.95,5    " />
<polygon fill="#231F20" points="1067.988,5 1067.548,5 1058.661,13 1059.204,13 1068.069,5.018    " />
<polygon fill="#231F20" points="1069.707,7.684 1063.802,13 1064.345,13 1069.707,8.172 1069.707,8.174 1075.452,13 1076.003,13

                            1069.707,7.711  " />
<polygon fill="#231F20" points="1069.707,11.025 1067.513,13 1068.056,13 1069.707,11.514 1069.707,11.547 1071.437,13

                            1071.987,13 1069.707,11.085     " />
<polygon fill="#231F20" points="1071.112,5 1080.636,13 1081.187,13 1071.664,5   " />
<polygon fill="#231F20" points="1076.493,5 1086.019,13 1086.568,13 1077.044,5   " />
<polygon fill="#231F20" points="1081.543,5 1090.879,12.842 1091.311,12.741 1082.095,5   " />
<polygon fill="#231F20" points="1093.833,10.627 1087.136,5 1086.584,5 1093.403,10.728   " />
<polygon fill="#231F20" points="1096.315,8.441 1092.222,5 1091.67,5 1095.886,8.542  " />
<polygon fill="#231F20" points="1098.735,6.223 1097.28,5 1096.728,5 1098.306,6.324  " />
<polygon fill="#231F20" points="1100.812,5 1091.928,13 1092.47,13 1101.354,5    " />
<polygon fill="#231F20" points="1105.952,5 1097.065,13 1097.61,13 1106.495,5    " />
<polygon fill="#231F20" points="1111.533,5 1111.093,5 1102.206,13 1102.749,13 1111.614,5.018    " />
<polygon fill="#231F20" points="1113.252,7.684 1107.347,13 1107.89,13 1113.252,8.172 1113.252,8.174 1118.997,13 1119.548,13

                            1113.252,7.711  " />
<polygon fill="#231F20" points="1113.252,11.025 1111.058,13 1111.601,13 1113.252,11.514 1113.252,11.547 1114.981,13

                            1115.532,13 1113.252,11.085     " />
<polygon fill="#231F20" points="1114.657,5 1124.181,13 1124.731,13 1115.209,5   " />
<polygon fill="#231F20" points="1120.038,5 1129.563,13 1130.113,13 1120.589,5   " />
<polygon fill="#231F20" points="1125.088,5 1134.424,12.842 1134.855,12.741 1125.64,5    " />
<polygon fill="#231F20" points="1137.378,10.627 1130.681,5 1130.129,5 1136.948,10.728   " /> 
<polygon fill="#231F20" points="1139.86,8.441 1135.767,5 1135.215,5 1139.431,8.542  " />
<polygon fill="#231F20" points="1142.28,6.223 1140.825,5 1140.272,5 1141.851,6.324  " />
<polygon fill="#231F20" points="1144.356,5 1135.473,13 1136.015,13 1144.899,5   " />
<polygon fill="#231F20" points="1149.497,5 1140.61,13 1141.155,13 1150.04,5     " />
<polygon fill="#231F20" points="1155.078,5 1154.638,5 1145.751,13 1146.294,13 1155.159,5.018    " />
<polygon fill="#231F20" points="1156.797,7.684 1150.892,13 1151.435,13 1156.797,8.172 1156.797,8.174 1162.542,13 1163.093,13

                            1156.797,7.711  " />
<polygon fill="#231F20" points="1156.797,11.025 1154.603,13 1155.146,13 1156.797,11.514 1156.797,11.547 1158.526,13

                            1159.077,13 1156.797,11.085     " />
<polygon fill="#231F20" points="1158.202,5 1167.726,13 1168.276,13 1158.754,5   " />
<polygon fill="#231F20" points="1163.583,5 1173.108,13 1173.658,13 1164.134,5   " />
<polygon fill="#231F20" points="1168.633,5 1177.969,12.842 1178.4,12.741 1169.185,5     " />
<polygon fill="#231F20" points="1180.923,10.627 1174.226,5 1173.674,5 1180.493,10.728   " />
<polygon fill="#231F20" points="1183.405,8.441 1179.312,5 1178.76,5 1182.976,8.542  " />
<polygon fill="#231F20" points="1185.825,6.223 1184.37,5 1183.817,5 1185.396,6.324  " />
<polygon fill="#231F20" points="1187.901,5 1179.018,13 1179.56,13 1188.444,5    " />
<polygon fill="#231F20" points="1193.042,5 1184.155,13 1184.7,13 1193.585,5     " />
<polygon fill="#231F20" points="1189.296,13 1189.839,13 1194.629,8.688 1194.629,8.199   " />
<polygon fill="#231F20" points="1194.629,13 1194.629,12.827 1194.437,13     " />
</g>
</svg></div>
<div class="clear"></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="df-social-connect"><a class="df-facebook" href="#" target="_blank"><i class="fa fa-facebook"></i><span class="social-text">Facebook</span></a><a class="df-twitter" href="#" target="_blank"><i class="fa fa-twitter"></i><span class="social-text">Twitter</span></a><a class="df-instagram" href="#" target="_blank"><i class="fa fa-instagram"></i><span class="social-text">Instagram</span></a><a class="df-pinterest" href="#" target="_blank"><i class="fa fa-pinterest"></i><span class="social-text">pinterest</span></a><a class="df-bloglovin" href="#" target="_blank"><i class="fa fa-heart"></i><span class="social-text">Bloglovin</span></a><a class="df-gplus" href="#" target="_blank"><i class="fa fa-google-plus"></i><span class="social-text">Google+</span></a></div> <div class="df-misc-section"><a class="df-misc-search"><span class="df-misc-text">Search</span><i class="ion-ios-search-strong"></i></a><a class="df-misc-mail"><span class="df-misc-text">Subscribe</span><i class="ion-android-remove"></i></a><a class="df-misc-archive" href="../../archives/index.html"><span class="df-misc-text">Archive</span><i class="ion-android-remove"></i></a></div>
</div>
<div id="footer-colophon" role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter" class="site-footer aligncenter">
<div class="df-footer-top">
<div id="text-3" class="widget widget_text"> <div class="textwidget"><div id="sb_instagram" class="sbi sbi_col_6" style="width:100%; " data-id="442412877" data-num="12" data-res="auto" data-cols="6" data-options='{&quot;sortby&quot;: &quot;none&quot;, &quot;showbio&quot;: &quot;true&quot;, &quot;headercolor&quot;: &quot;&quot;, &quot;imagepadding&quot;: &quot;0&quot;}'><div id="sbi_images" style="padding: 0px;"><div class="sbi_loader fa-spin"></div></div><div id="sbi_load"></div></div></div>
</div>
</div>
<div class="df-footer-bottom border-top">
<div class="container">
<div class="df-foot-logo">
<a href="../../index.html" class="df-sitename" title="Applique Men&#039;s Wear">
<img src="../../wp-content/uploads/sites/26/2016/02/menswear-logo.png" alt="Applique Men&#039;s Wear">
</a>
</div>
<div class="main-navigation" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<div class="container"><ul class="nav aligncenter"><li id="menu-item-86" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-86"><a href="#">Facebook</a></li>
<li id="menu-item-87" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-87"><a href="#">Instagram</a></li>
<li id="menu-item-247" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-247"><a href="#">Twitter</a></li>
<li id="menu-item-88" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-88"><a href="#">Bloglovin</a></li>
<li id="menu-item-89" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-89"><a href="#">Pinterest</a></li>
</ul></div>
</div>
<div class="siteinfo">
<p>Copyright &copy; <span itemprop="copyrightYear">2018</span> <span itemprop="copyrightHolder">DAHZ</span> All Rights Reserved. Applique Men&#039;s Wear.</p>
</div>
<a href="#" class="scroll-top">
<i class="ion-ios-arrow-thin-up"></i>
<i class="ion-ios-arrow-thin-up"></i>
</a>
<div class="df-floating-search">
<div class="search-container-close"></div>
<div class="container df-floating-search-form">
<form class="df-floating-search-form-wrap col-md-8 col-md-push-2" method="get" action="http://dahz.daffyhazan.com/applique/menswear/">
<label class="label-text">
<input type="search" class="df-floating-search-form-input" placeholder="What are you looking for" value="" name="s" title="Search for:">
</label>
<div class="df-floating-search-close"><i class="ion-ios-close-empty"></i></div>
</form>
</div>
</div>
<div class="df-floating-subscription">
<div class="container-close"></div>
<div class="container">
<div class="row">
<div class="wrapper col-md-8 col-md-push-2">
<div class="row flex-box">
<div class="col-left col-md-5">
<div class="wrap">
<img src="../../wp-content/uploads/sites/26/2016/02/subscribe.jpg" alt="" />
</div>
</div>
<div class="col-right col-md-7">
<div class="wrap">
<form id="mc4wp-form-2" class="mc4wp-form mc4wp-form-65" method="post" data-id="65" data-name="Newsletter"><div class="mc4wp-form-fields"><h1>Never Miss a Post</h1>
<p>A black and white style philosophy is only one click away</p>
<p>
<label>Email address: </label>
<input type="email" name="EMAIL" placeholder="Your email address" required />
</p>
<p>
<input type="submit" value="Sign up" />
</p><div style="display: none;"><input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off" /></div><input type="hidden" name="_mc4wp_timestamp" value="1522839203" /><input type="hidden" name="_mc4wp_form_id" value="65" /><input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-2" /></div><div class="mc4wp-response"></div></form> </div>
</div>
</div>
<div class="df-floating-close"><i class="ion-ios-close-empty"></i></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-20219691-40', 'auto');
  ga('send', 'pageview');

</script><!--[if lte IE 8]>
<link rel='stylesheet' id='jetpack-carousel-ie8fix-css'  href='http://dahz.daffyhazan.com/applique/menswear/wp-content/plugins/jetpack/modules/carousel/jetpack-carousel-ie8fix.css?ver=20121024' type='text/css' media='all' />
<![endif]-->
<script type='text/javascript'>
/* <![CDATA[ */
var carousel = {"type":"slider","auto_play":"1","slide_type":"df-slider-4"};
/* ]]> */
</script>
<script type='text/javascript' src='../../wp-content/themes/applique/assets/js/owl.carousel.min001e.js?ver=2.0.0'></script>
<script type='text/javascript' src='../../wp-content/themes/applique/assets/js/waypointcce7.js?ver=4.0.0'></script>
<script type='text/javascript' src='../../wp-content/themes/applique/assets/js/fitvids4963.js?ver=1.1'></script>
<script type='text/javascript' src='../../wp-content/themes/applique/assets/js/debounced-resize.js'></script>
<script type='text/javascript' src='../../wp-content/themes/applique/assets/js/parallaxc358.js?ver=1.1.3'></script>
<script type='text/javascript' src='../../wp-content/themes/applique/assets/js/grid605a.js?ver=2.2.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var inf = {"finishText":"All Post Loaded"};
/* ]]> */
</script>
<script type='text/javascript' src='../../wp-content/themes/applique/assets/js/infinite-scroll3c94.js?ver=2.1.0'></script>
<script type='text/javascript' src='../../wp-content/themes/applique/assets/js/jquery.scrolldepth.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var df = {"navClass":""};
/* ]]> */
</script>
<script type='text/javascript' src='../../wp-content/themes/applique/assets/js/main.minfeec.js?ver=1.6.5'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */
</script>
<script type='text/javascript' src='../../wp-content/plugins/contact-form-7/includes/js/scripts5597.js?ver=5.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var sb_instagram_js_options = {"sb_instagram_at":"612741813.3a81a9f.4472fbf925924a709fc46205b5ed9a6b"};
/* ]]> */
</script>
<script type='text/javascript' src='../../wp-content/plugins/instagram-feed/js/sb-instagram.minf24c.js?ver=1.6'></script>
<script type='text/javascript' src='../../../../../s0.wp.com/wp-content/js/devicepx-jetpack1a52.js?ver=201814'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/applique\/menswear\/wp-admin\/admin-ajax.php","wc_ajax_url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script type='text/javascript' src='../../wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min3d36.js?ver=3.3.1'></script>
<script type='text/javascript' src='../../wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min44fd.js?ver=2.70'></script>
<script type='text/javascript' src='../../wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min6b25.js?ver=2.1.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/applique\/menswear\/wp-admin\/admin-ajax.php","wc_ajax_url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script type='text/javascript' src='../../wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min3d36.js?ver=3.3.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/applique\/menswear\/wp-admin\/admin-ajax.php","wc_ajax_url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_60bed813bcdf7f8a43b3e086e55ad050","fragment_name":"wc_fragments_60bed813bcdf7f8a43b3e086e55ad050"};
/* ]]> */
</script>
<script type='text/javascript' src='../../wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min3d36.js?ver=3.3.1'></script>
<script type='text/javascript' src='../../wp-includes/js/wp-embed.min55fe.js?ver=4.9.5'></script>
<script type='text/javascript' src='../../wp-content/plugins/jetpack/_inc/build/spin.min4e44.js?ver=1.3'></script>
<script type='text/javascript' src='../../wp-content/plugins/jetpack/_inc/build/jquery.spin.min4e44.js?ver=1.3'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var jetpackCarouselStrings = {"widths":[370,700,1000,1200,1400,2000],"is_logged_in":"","lang":"en","ajaxurl":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/wp-admin\/admin-ajax.php","nonce":"bdc6a3f792","display_exif":"0","display_geo":"1","single_image_gallery":"1","single_image_gallery_media_file":"","background_color":"white","comment":"Comment","post_comment":"Post Comment","write_comment":"Write a Comment...","loading_comments":"Loading Comments...","download_original":"View full size <span class=\"photo-size\">{0}<span class=\"photo-size-times\">\u00d7<\/span>{1}<\/span>","no_comment_text":"Please be sure to submit some text with your comment.","no_comment_email":"Please provide an email address to comment.","no_comment_author":"Please provide your name to comment.","comment_post_error":"Sorry, but there was an error posting your comment. Please try again later.","comment_approved":"Your comment was approved.","comment_unapproved":"Your comment is in moderation.","camera":"Camera","aperture":"Aperture","shutter_speed":"Shutter Speed","focal_length":"Focal Length","copyright":"Copyright","comment_registration":"0","require_name_email":"1","login_url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/wp-login.php?redirect_to=http%3A%2F%2Fdahz.daffyhazan.com%2Fapplique%2Fmenswear%2Fpraha-street-style-february-2016%2F","blog_id":"26","meta_data":["camera","aperture","shutter_speed","focal_length","copyright"],"local_comments_commenting_as":"<fieldset><label for=\"email\">Email (Required)<\/label> <input type=\"text\" name=\"email\" class=\"jp-carousel-comment-form-field jp-carousel-comment-form-text-field\" id=\"jp-carousel-comment-form-email-field\" \/><\/fieldset><fieldset><label for=\"author\">Name (Required)<\/label> <input type=\"text\" name=\"author\" class=\"jp-carousel-comment-form-field jp-carousel-comment-form-text-field\" id=\"jp-carousel-comment-form-author-field\" \/><\/fieldset><fieldset><label for=\"url\">Website<\/label> <input type=\"text\" name=\"url\" class=\"jp-carousel-comment-form-field jp-carousel-comment-form-text-field\" id=\"jp-carousel-comment-form-url-field\" \/><\/fieldset>"};
/* ]]> */
</script>
<script type='text/javascript' src='../../wp-content/plugins/jetpack/_inc/build/carousel/jetpack-carousel.mina4d5.js?ver=20170209'></script>
<script type='text/javascript' src='../../wp-content/themes/applique/assets/js/TheiaStickySidebar8d1e.js?ver=1.2.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var mc4wp_forms_config = [];
/* ]]> */
</script>
<script type='text/javascript' src='../../wp-content/plugins/mailchimp-for-wp/assets/js/forms-api.min2d73.js?ver=3.1.11'></script>
<!--[if lte IE 9]>
<script type='text/javascript' src='http://dahz.daffyhazan.com/applique/menswear/wp-content/plugins/mailchimp-for-wp/assets/js/third-party/placeholders.min.js?ver=3.1.11'></script>
<![endif]-->
<script type="text/javascript">(function() {function addEventListener(element,event,handler) {
	if(element.addEventListener) {
		element.addEventListener(event,handler, false);
	} else if(element.attachEvent){
		element.attachEvent('on'+event,handler);
	}
}})();</script>
</body>

<!-- Mirrored from dahz.daffyhazan.com/applique/menswear/category/my-story/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 11:11:12 GMT -->
</html>