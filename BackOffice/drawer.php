        <!-- drawer -->
        <div class="mdk-drawer js-mdk-drawer" id="default-drawer">
            <div class="mdk-drawer__content">
                <div class="mdk-drawer__inner" data-simplebar data-simplebar-force-enabled="true">

                    <nav class="drawer  drawer--dark">
                        <div class="drawer-spacer">
                            <div class="media align-items-center">
                                <a href="index.html" class="drawer-brand-circle mr-2">H</a>
                                <div class="media-body">
                                    <a href="index.html" class="h5 m-0 text-link">TennisPoint - Admin</a>
                                </div>
                            </div>
                        </div>
                        <!-- HEADING -->
                        <!-- MENU -->
                        <ul class="drawer-menu" id="mainMenu" data-children=".drawer-submenu">
                       <li class="drawer-menu-item drawer-submenu">
                                <a data-toggle="collapse" data-parent="#mainMenu" href="#" data-target="#formsMenu" aria-controls="formsMenu" aria-expanded="false" class="collapsed">
        <i class="material-icons">text_format</i>
        <span class="drawer-menu-text"> Insertion</span>
      </a>
                                <ul class="collapse " id="formsMenu">
                                    <li class="drawer-menu-item "><a href="insertProduct.php">Products</a></li>
                                    <li class="drawer-menu-item "><a href="insertUser.php">Users</a></li>
                                </ul>
                            </li>

                            <ul class="drawer-menu" id="mainMenu" data-children=".drawer-submenu">
                            <li class="drawer-menu-item  ">
                            <a data-toggle="collapse" data-parent="#mainMenu" href="#" data-target="#tableMenu" aria-controls="tableMenu" aria-expanded="false" class="collapsed">
                                    <i class="material-icons">tab</i>
                                    <span class="drawer-menu-text"> List </span>
                                </a>
                                <ul class="collapse " id="tableMenu">
                                    <li class="drawer-menu-item "><a href="products.php">Products</a></li>
                                    <li class="drawer-menu-item "><a href="users.php">Users</a></li>
                                </ul>
                            </li>
                        <!-- HEADING -->
                        <div class="py-2 drawer-heading">
                            Pages
                        </div>

                        <!-- MENU -->
                        <ul class="drawer-menu" id="mainMenu" data-children=".drawer-submenu">
                            <li class="drawer-menu-item">
                                <a href="account.html">
        <i class="material-icons">edit</i>
        <span class="drawer-menu-text">Edit Account</span>
      </a>
                            </li>
                            <li class="drawer-menu-item">
                                <a href="login.html">
        <i class="material-icons">lock</i>
        <span class="drawer-menu-text">Login</span>
      </a>
                            </li>
                            <li class="drawer-menu-item">
                                <a href="signup.html">
        <i class="material-icons">account_circle</i>
        <span class="drawer-menu-text">Sign Up</span>
      </a>
                            </li>
                            <li class="drawer-menu-item">
                                <a href="forgot-password.html">
        <i class="material-icons">help</i>
        <span class="drawer-menu-text">Forgot Password</span>
      </a>
                            </li>
                        </ul>

                    </nav>
                </div>
            </div>
        </div>
        <!-- // END drawer -->
