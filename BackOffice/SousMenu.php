<?php
	
	 class SousMenu{
		public $id;
		public $nom;
        public $idMenu;
        public $fichier;
		
                function SousMenu($idE, $nomE,$idMenuE,$fichierE){
                    $this->setId($idE);
                    $this->setNom($nomE);
                    $this->setMenu($idMenuE);
                    $this->setFichier($fichierE);
                }
		function getId(){
			return $this->id;
		}
		
		function setId($newId){
			$this->id = $newId;
		}
				
		function getNom(){
			return $this->nom;
		}
		
		function setNom($newnom){
			$this->nom = $newnom;
		}
		function getMenu(){
			return $this->idMenu;
		}
		
		function setMenu($newSousmenu){
			$this->idMenu = $newSousmenu;
        }
        
        function getFichier(){
			return $this->fichier;
		}
		
		function setFichier($newfichier){
			$this->fichier = $newfichier;
		}
	?>