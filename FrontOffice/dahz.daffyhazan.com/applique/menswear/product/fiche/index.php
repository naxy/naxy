<?php 
  $article = $_GET["article"];
  require("../../../../../../BackOffice/Connexion.php");    
  require("../../../../../../BackOffice/ContenuDAO.php");
  $cDB = new ContenuDAO();
  $content = $cDB->contentById($article);
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en-US"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en-US"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en-US"> <!--<![endif]-->
<html class="no-js" lang="en-US">

<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<?php include("header.php") ?>
<body class="product-template-default single single-product postid-654 woocommerce woocommerce-page df-skin-bold unknown windows glob-bw" dir="ltr" itemscope="itemscope" itemtype="http://schema.org/WebPage">
<div class="ajax_loader">
<div class="ajax_loader_1">
<div class="double_pulse"><div class="double-bounce1" style="background-color:#1d1c1b"></div><div class="double-bounce2" style="background-color:#1d1c1b"></div></div>
</div>
</div>
<div id="wrapper" class="df-wrapper">
<div class="df-mobile-menu">
<div class="inner-wrapper container">
<div class="df-ham-menu">
<div class="col-left">
<a href="#">
<span class="df-top"></span>
<span class="df-middle"></span>
<span class="df-bottom"></span>
</a>
</div>
<div class="col-right">
<a href="#" class="mobile-subs"><i class="ion-ios-email-outline"></i></a>
<a href="#" class="mobile-search"><i class="ion-ios-search-strong"></i></a>
</div>
</div>
<div class="df-menu-content">
<div class="content-wrap">
<div class="main-navigation" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<div class="nav-wrapper-inner ">
<div class="sticky-logo">
<a href="../../index.html" class="df-sitename" title="Applique Men&#039;s Wear" itemprop="headline">
<img src="../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2016/02/menswear-sticky.png" alt="Applique Men&#039;s Wear">
</a>
</div>
<?php include("util.php") ?>
<div class="sticky-btp">
<a class="scroll-top"><i class="ion-ios-arrow-thin-up"></i><i class="ion-ios-arrow-thin-up"></i></a>
</div>
</div>
</div>
<div class="df-social-connect"><a class="df-facebook" href="#" target="_blank"><i class="fa fa-facebook"></i><span class="social-text">Facebook</span></a><a class="df-twitter" href="#" target="_blank"><i class="fa fa-twitter"></i><span class="social-text">Twitter</span></a><a class="df-instagram" href="#" target="_blank"><i class="fa fa-instagram"></i><span class="social-text">Instagram</span></a><a class="df-pinterest" href="#" target="_blank"><i class="fa fa-pinterest"></i><span class="social-text">pinterest</span></a><a class="df-bloglovin" href="#" target="_blank"><i class="fa fa-heart"></i><span class="social-text">Bloglovin</span></a><a class="df-gplus" href="#" target="_blank"><i class="fa fa-google-plus"></i><span class="social-text">Google+</span></a></div> </div>
</div>
</div>
</div>
<div id="masthead" role="banner" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
<div class="df-header-inner">
<div id="branding" class="site-branding border-bottom aligncenter">
<div class="container">
<a href="../../index.html" class="df-sitename" title="Applique Men&#039;s Wear" id="site-title" itemprop="headline">
<img src="../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2016/02/tennispoint-logo.png" alt="Applique Men&#039;s Wear">
</a>
</div>
</div>
<div class="main-navigation" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<div class="nav-wrapper-inner ">
<div class="sticky-logo">
<a href="../../index.html" class="df-sitename" title="Applique Men&#039;s Wear" itemprop="headline">
<img src="../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2016/02/menswear-sticky.png" alt="Applique Men&#039;s Wear">
</a>
</div>
<?php include("menus.php") ?>
<div class="sticky-btp">
<a class="scroll-top"><i class="ion-ios-arrow-thin-up"></i><i class="ion-ios-arrow-thin-up"></i></a>
</div>
</div>
</div>
</div>
</div>

<div class="df-header-title aligncenter"><div class="container"><div class="df-single-category"><span class="entry-terms category" itemprop="articleSection"></span></div><div class="df-header"><h1 class="entry-title display-1" itemprop="headline"><?php echo $content->Text; ?></h1></div></div></div>
<div id="content-wrap">
<div class="main-sidebar-container container">
<div class="row">
<div id="df-content" class="df-content col-md-8 col-md-push-2 df-no-sidebar">
<nav class="woocommerce-breadcrumb"><a href="http://dahz.daffyhazan.com/applique/menswear">Home</a>&nbsp;&#47;&nbsp;<a href="../../product-category/posters/index.html">Posters</a>&nbsp;&#47;&nbsp;<?php echo $content->Text; ?></nav>
<div id="product-654" class="post-654 product type-product status-publish has-post-thumbnail product_cat-posters first instock sale shipping-taxable purchasable product-type-simple">
<div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4" style="opacity: 0; transition: opacity .25s ease-in-out;">
<figure class="woocommerce-product-gallery__wrapper">
<div data-thumb="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content->Image; ?>" class="woocommerce-product-gallery__image"><a href="FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_2_up.jpg"><img width="600" src="../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content->Image; ?>" class="attachment-woocommerce_single size-woocommerce_single wp-post-image" alt="" title="poster_2_up" data-caption="" data-src="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content->Image; ?>" data-large_image="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content->Image; ?>" data-large_image_width="1000" data-large_image_height="1000" data-attachment-id="71" data-permalink="http://dahz.daffyhazan.com/applique/menswear/?attachment_id=71" data-orig-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content->Image; ?>" data-orig-size="1000,1000" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="poster_2_up" data-image-description="" data-medium-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content->Image; ?>" data-large-file="FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content->Image; ?>" /></a></div><div data-thumb="" class="woocommerce-product-gallery__image"><a href="#"></a></div> </figure>
</div>
<div class="summary entry-summary">
<h1 class="product_title entry-title"><?php echo $content->Text; ?></h1>
<div class="woocommerce-product-rating">
<div class="star-rating"><span style="width:80%">Rated <strong class="rating">4.00</strong> out of 5 based on <span class="rating">4</span> customer ratings</span></div> <a href="#reviews" class="woocommerce-review-link" rel="nofollow">(<span class="count">4</span> customer reviews)</a> </div>
<?php $price = ($content->remarque)+2; ?>
<p class="price"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span><?php echo $price ?></span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span><?php echo $content->remarque; ?></span></ins></p>
<div class="woocommerce-product-details__short-description">

<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
</div>
<form class="cart" action="http://dahz.daffyhazan.com/applique/menswear/product/flying-ninja/" method="post" enctype='multipart/form-data'>
<div class="quantity">
<label class="screen-reader-text" for="quantity_5ac4b334ba470">Quantity</label>
<input type="number" id="quantity_5ac4b334ba470" class="input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="Qty" size="4" pattern="[0-9]*" inputmode="numeric" aria-labelledby="" />
</div>
<button type="submit" name="add-to-cart" value="654" class="single_add_to_cart_button button alt">Add Cart</button>
</form>
<div class="product_meta">
<span class="posted_in">Category: <a href="../../product-category/posters/index.html" rel="tag">Posters</a></span>
</div>
</div>
<div class="woocommerce-tabs wc-tabs-wrapper">
<ul class="tabs wc-tabs" role="tablist">
<li class="description_tab" id="tab-title-description" role="tab" aria-controls="tab-description">
<a href="#tab-description">Description</a>
</li>
<li class="reviews_tab" id="tab-title-reviews" role="tab" aria-controls="tab-reviews">
<a href="#tab-reviews">Reviews (4)</a>
</li>
</ul>
<div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab" id="tab-description" role="tabpanel" aria-labelledby="tab-title-description">
<h2>Description</h2>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
</div>
<div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--reviews panel entry-content wc-tab" id="tab-reviews" role="tabpanel" aria-labelledby="tab-title-reviews">
<div id="reviews" class="woocommerce-Reviews">
<div id="comments">
<h2 class="woocommerce-Reviews-title">4 reviews for <span>Flying Ninja</span></h2>
<ol class="commentlist">
<li class="comment even thread-even depth-1" id="li-comment-41">
<div id="comment-41" class="comment_container">
<img alt='' src='http://0.gravatar.com/avatar/f0cde930b42c79145194679d5b6e3b1d?s=60&amp;d=mm&amp;r=g' srcset='http://0.gravatar.com/avatar/f0cde930b42c79145194679d5b6e3b1d?s=120&#038;d=mm&#038;r=g 2x' class='avatar avatar-60 photo' height='60' width='60' />
<div class="comment-text">
<div class="star-rating"><span style="width:80%">Rated <strong class="rating">4</strong> out of 5</span></div>
<p class="meta">
<strong class="woocommerce-review__author">Cobus Bester</strong> <span class="woocommerce-review__dash">&ndash;</span> <time class="woocommerce-review__published-date" datetime="2013-06-07T11:52:25+00:00">June 7, 2013</time>
</p>
<div class="description"><p>Really happy with this print. The colors are great, and the paper quality is good too.</p>
</div>
</div>
</div>
</li>
<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-42">
<div id="comment-42" class="comment_container">
<img alt='' src='http://2.gravatar.com/avatar/8026a390d28369f7a2befcaeb9557359?s=60&amp;d=mm&amp;r=g' srcset='http://2.gravatar.com/avatar/8026a390d28369f7a2befcaeb9557359?s=120&#038;d=mm&#038;r=g 2x' class='avatar avatar-60 photo' height='60' width='60' />
<div class="comment-text">
<div class="star-rating"><span style="width:60%">Rated <strong class="rating">3</strong> out of 5</span></div>
<p class="meta">
<strong class="woocommerce-review__author">Andrew</strong> <span class="woocommerce-review__dash">&ndash;</span> <time class="woocommerce-review__published-date" datetime="2013-06-07T11:56:36+00:00">June 7, 2013</time>
</p>
<div class="description"><p>You only get the picture, not the person holding it, something they don&#8217;t mention in the description, now I&#8217;ve got to find my own person</p>
</div>
</div>
</div>
</li>
<li class="comment even thread-even depth-1" id="li-comment-43">
<div id="comment-43" class="comment_container">
<img alt='' src='http://0.gravatar.com/avatar/3472757f6a3732d6470f98d7d7e9cece?s=60&amp;d=mm&amp;r=g' srcset='http://0.gravatar.com/avatar/3472757f6a3732d6470f98d7d7e9cece?s=120&#038;d=mm&#038;r=g 2x' class='avatar avatar-60 photo' height='60' width='60' />
<div class="comment-text">
<div class="star-rating"><span style="width:100%">Rated <strong class="rating">5</strong> out of 5</span></div>
<p class="meta">
<strong class="woocommerce-review__author">Coen Jacobs</strong> <span class="woocommerce-review__dash">&ndash;</span> <time class="woocommerce-review__published-date" datetime="2013-06-07T12:19:25+00:00">June 7, 2013</time>
</p>
<div class="description"><p>This is my favorite poster. In fact, I&#8217;ve ordered 5 of them!</p>
</div>
</div>
</div>
</li>
<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-44">
<div id="comment-44" class="comment_container">
<img alt='' src='http://1.gravatar.com/avatar/7a6df00789e50714fcde1b759befcc84?s=60&amp;d=mm&amp;r=g' srcset='http://1.gravatar.com/avatar/7a6df00789e50714fcde1b759befcc84?s=120&#038;d=mm&#038;r=g 2x' class='avatar avatar-60 photo' height='60' width='60' />
<div class="comment-text">
<div class="star-rating"><span style="width:80%">Rated <strong class="rating">4</strong> out of 5</span></div>
<p class="meta">
<strong class="woocommerce-review__author">Stuart</strong> <span class="woocommerce-review__dash">&ndash;</span> <time class="woocommerce-review__published-date" datetime="2013-06-07T12:59:49+00:00">June 7, 2013</time>
</p>
<div class="description"><p>This is a fantastic quality print and is happily hanging framed on my wall now.</p>
</div>
</div>
</div>
</li>
</ol>
</div>
<div id="review_form_wrapper">
<div id="review_form">
<div id="respond" class="comment-respond">
<span id="reply-title" class="comment-reply-title">Add a review <small><a rel="nofollow" id="cancel-comment-reply-link" href="index.html#respond" style="display:none;">Cancel reply</a></small></span> <form action="http://dahz.daffyhazan.com/applique/menswear/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate>
<p class="comment-notes"><span id="email-notes">Your email address will not be published.</span> Required fields are marked <span class="required">*</span></p><div class="comment-form-rating"><label for="rating">Your rating</label><select name="rating" id="rating" aria-required="true" required>
<option value="">Rate&hellip;</option>
<option value="5">Perfect</option>
<option value="4">Good</option>
<option value="3">Average</option>
<option value="2">Not that bad</option>
<option value="1">Very poor</option>
</select></div><p class="comment-form-comment"><label for="comment">Your review <span class="required">*</span></label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" required></textarea></p><p class="comment-form-author"><label for="author">Name <span class="required">*</span></label> <input id="author" name="author" type="text" value="" size="30" aria-required="true" required /></p>
<p class="comment-form-email"><label for="email">Email <span class="required">*</span></label> <input id="email" name="email" type="email" value="" size="30" aria-required="true" required /></p>
<p class="form-submit"><input name="submit" type="submit" id="submit" class="submit" value="Submit" /> <input type='hidden' name='comment_post_ID' value='654' id='comment_post_ID' />
<input type='hidden' name='comment_parent' id='comment_parent' value='0' />
</p><p style="display: none;"><input type="hidden" id="akismet_comment_nonce" name="akismet_comment_nonce" value="5b4d3a3da1" /></p><p style="display: none;"><input type="hidden" id="ak_js" name="ak_js" value="249" /></p> </form>
</div>
</div>
</div>
<div class="clear"></div>
</div>
</div>
</div>
<section class="related products">
<h2>Related products</h2>
<ul class="products columns-4">
<li class="post-67 product type-product status-publish has-post-thumbnail product_cat-posters first instock shipping-taxable purchasable product-type-simple">
<a href="../ship-your-idea-3/index.html" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="300" src="FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_1_up-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_1_up-300x300.jpg 300w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_1_up-150x150.jpg 150w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_1_up-768x768.jpg 768w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_1_up-180x180.jpg 180w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_1_up-600x600.jpg 600w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_1_up-250x250.jpg 250w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_1_up-80x80.jpg 80w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_1_up-330x330.jpg 330w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_1_up-160x160.jpg 160w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_1_up-293x293.jpg 293w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_1_up.jpg 1000w" sizes="(max-width: 300px) 100vw, 300px" data-attachment-id="68" data-permalink="//dahz.daffyhazan.com/applique/menswear/?attachment_id=68" data-orig-file="//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_1_up.jpg" data-orig-size="1000,1000" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="poster_1_up" data-image-description="" data-medium-file="//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_1_up-300x300.jpg" data-large-file="FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_1_up.jpg" /><h4>Ship Your Idea</h4>
<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>15.00</span></span>
</a><a href="index1950.html?add-to-cart=67" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="67" data-product_sku="" aria-label="Add &ldquo;Ship Your Idea&rdquo; to your cart" rel="nofollow">Add cart</a></li>
<li class="post-73 product type-product status-publish has-post-thumbnail product_cat-posters  instock sale shipping-taxable purchasable product-type-simple">
<a href="../premium-quality-2/index.html" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
<span class="onsale">Sale!</span>
<img width="300" height="300" src="../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_3_up-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_3_up-300x300.jpg 300w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_3_up-150x150.jpg 150w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_3_up-768x768.jpg 768w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_3_up-180x180.jpg 180w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_3_up-600x600.jpg 600w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_3_up-250x250.jpg 250w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_3_up-80x80.jpg 80w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_3_up-330x330.jpg 330w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_3_up-160x160.jpg 160w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_3_up-293x293.jpg 293w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_3_up.jpg 1000w" sizes="(max-width: 300px) 100vw, 300px" data-attachment-id="74" data-permalink="//dahz.daffyhazan.com/applique/menswear/?attachment_id=74" data-orig-file="//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_3_up.jpg" data-orig-size="1000,1000" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="poster_3_up" data-image-description="" data-medium-file="//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_3_up-300x300.jpg" data-large-file="../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_3_up.jpg" /><h4>Premium Quality</h4><div class="star-rating"><span style="width:40%">Rated <strong class="rating">2.00</strong> out of 5</span></div>
<span class="price"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>15.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>12.00</span></ins></span>
</a><a href="index18c1.html?add-to-cart=73" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="73" data-product_sku="" aria-label="Add &ldquo;Premium Quality&rdquo; to your cart" rel="nofollow">Add cart</a></li>
<li class="post-79 product type-product status-publish has-post-thumbnail product_cat-posters  instock shipping-taxable purchasable product-type-simple">
<a href="../woo-logo-3/index.html" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="300" src="../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_5_up-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_5_up-300x300.jpg 300w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_5_up-150x150.jpg 150w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_5_up-768x768.jpg 768w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_5_up-180x180.jpg 180w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_5_up-600x600.jpg 600w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_5_up-250x250.jpg 250w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_5_up-80x80.jpg 80w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_5_up-330x330.jpg 330w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_5_up-160x160.jpg 160w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_5_up-293x293.jpg 293w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_5_up.jpg 1000w" sizes="(max-width: 300px) 100vw, 300px" data-attachment-id="639" data-permalink="//dahz.daffyhazan.com/applique/menswear/?attachment_id=639" data-orig-file="//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_5_up.jpg" data-orig-size="1000,1000" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="poster_5_up" data-image-description="" data-medium-file="//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_5_up-300x300.jpg" data-large-file="FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_5_up.jpg" /><h4>Woo Logo</h4>
<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>15.00</span></span>
</a><a href="index70f8.html?add-to-cart=79" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="79" data-product_sku="" aria-label="Add &ldquo;Woo Logo&rdquo; to your cart" rel="nofollow">Add cart</a></li>
<li class="post-76 product type-product status-publish has-post-thumbnail product_cat-posters last instock shipping-taxable purchasable product-type-simple">
<a href="../woo-ninja-3/index.html" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="300" src="../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_4_up-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_4_up-300x300.jpg 300w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_4_up-150x150.jpg 150w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_4_up-768x768.jpg 768w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_4_up-180x180.jpg 180w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_4_up-600x600.jpg 600w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_4_up-250x250.jpg 250w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_4_up-80x80.jpg 80w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_4_up-330x330.jpg 330w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_4_up-160x160.jpg 160w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_4_up-293x293.jpg 293w, //dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_4_up.jpg 1000w" sizes="(max-width: 300px) 100vw, 300px" data-attachment-id="637" data-permalink="//dahz.daffyhazan.com/applique/menswear/?attachment_id=637" data-orig-file="//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_4_up.jpg" data-orig-size="1000,1000" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="poster_4_up" data-image-description="" data-medium-file="//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_4_up-300x300.jpg" data-large-file="FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/poster_4_up.jpg" /><h4>Woo Ninja</h4><div class="star-rating"><span style="width:80%">Rated <strong class="rating">4.00</strong> out of 5</span></div>
<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>15.00</span></span>
</a><a href="index499f.html?add-to-cart=76" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="76" data-product_sku="" aria-label="Add &ldquo;Woo Ninja&rdquo; to your cart" rel="nofollow">Add cart</a></li>
</ul>
</section>
</div>
</div>
</div>
</div>
<div class="df-social-connect"><a class="df-facebook" href="#" target="_blank"><i class="fa fa-facebook"></i><span class="social-text">Facebook</span></a><a class="df-twitter" href="#" target="_blank"><i class="fa fa-twitter"></i><span class="social-text">Twitter</span></a><a class="df-instagram" href="#" target="_blank"><i class="fa fa-instagram"></i><span class="social-text">Instagram</span></a><a class="df-pinterest" href="#" target="_blank"><i class="fa fa-pinterest"></i><span class="social-text">pinterest</span></a><a class="df-bloglovin" href="#" target="_blank"><i class="fa fa-heart"></i><span class="social-text">Bloglovin</span></a><a class="df-gplus" href="#" target="_blank"><i class="fa fa-google-plus"></i><span class="social-text">Google+</span></a></div> <div class="df-misc-section"><a class="df-misc-search"><span class="df-misc-text">Search</span><i class="ion-ios-search-strong"></i></a><a class="df-misc-mail"><span class="df-misc-text">Subscribe</span><i class="ion-android-remove"></i></a><a class="df-misc-archive" href="../../archives/index.html"><span class="df-misc-text">Archive</span><i class="ion-android-remove"></i></a></div>
</div>
<div id="footer-colophon" role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter" class="site-footer aligncenter">
<div class="df-footer-top">
<div id="text-3" class="widget widget_text"> <div class="textwidget"><div id="sb_instagram" class="sbi sbi_col_6" style="width:100%; " data-id="442412877" data-num="12" data-res="auto" data-cols="6" data-options='{&quot;sortby&quot;: &quot;none&quot;, &quot;showbio&quot;: &quot;true&quot;, &quot;headercolor&quot;: &quot;&quot;, &quot;imagepadding&quot;: &quot;0&quot;}'><div id="sbi_images" style="padding: 0px;"><div class="sbi_loader fa-spin"></div></div><div id="sbi_load"></div></div></div>
</div>
</div>
<div class="df-footer-bottom border-top">
<div class="container">
<div class="df-foot-logo">
<a href="../../index.html" class="df-sitename" title="Applique Men&#039;s Wear">
<img src="../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2016/02/tennispoint-logo.png" alt="Applique Men&#039;s Wear">
</a>
</div>
<div class="main-navigation" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<div class="container"><ul class="nav aligncenter"><li id="menu-item-86" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-86"><a href="#">Facebook</a></li>
<li id="menu-item-87" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-87"><a href="#">Instagram</a></li>
<li id="menu-item-247" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-247"><a href="#">Twitter</a></li>
<li id="menu-item-88" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-88"><a href="#">Bloglovin</a></li>
<li id="menu-item-89" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-89"><a href="#">Pinterest</a></li>
</ul></div>
</div>
<div class="siteinfo">
<p>Copyright &copy; <span itemprop="copyrightYear">2018</span> <span itemprop="copyrightHolder">DAHZ</span> All Rights Reserved. Applique Men&#039;s Wear.</p>
</div>
<a href="#" class="scroll-top">
<i class="ion-ios-arrow-thin-up"></i>
<i class="ion-ios-arrow-thin-up"></i>
</a>
<div class="df-floating-search">
<div class="search-container-close"></div>
<div class="container df-floating-search-form">
<form class="df-floating-search-form-wrap col-md-8 col-md-push-2" method="get" action="http://dahz.daffyhazan.com/applique/menswear/">
<label class="label-text">
<input type="search" class="df-floating-search-form-input" placeholder="What are you looking for" value="" name="s" title="Search for:">
</label>
<div class="df-floating-search-close"><i class="ion-ios-close-empty"></i></div>
</form>
</div>
</div>
<div class="df-floating-subscription">
<div class="container-close"></div>
<div class="container">
<div class="row">
<div class="wrapper col-md-8 col-md-push-2">
<div class="row flex-box">
<div class="col-left col-md-5">
<div class="wrap">
<img src="../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2016/02/subscribe.jpg" alt="" />
</div>
</div>
<div class="col-right col-md-7">
<div class="wrap">
<form id="mc4wp-form-2" class="mc4wp-form mc4wp-form-65" method="post" data-id="65" data-name="Newsletter"><div class="mc4wp-form-fields"><h1>Never Miss a Post</h1>
<p>A black and white style philosophy is only one click away</p>
<p>
<label>Email address: </label>
<input type="email" name="EMAIL" placeholder="Your email address" required />
</p>
<p>
<input type="submit" value="Sign up" />
</p><div style="display: none;"><input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off" /></div><input type="hidden" name="_mc4wp_timestamp" value="1522840372" /><input type="hidden" name="_mc4wp_form_id" value="65" /><input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-2" /></div><div class="mc4wp-response"></div></form> </div>
</div>
</div>
<div class="df-floating-close"><i class="ion-ios-close-empty"></i></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-20219691-40', 'auto');
  ga('send', 'pageview');

</script><script type="application/ld+json">{"@context":"https:\/\/schema.org\/","@graph":[{"@context":"https:\/\/schema.org\/","@type":"BreadcrumbList","itemListElement":[{"@type":"ListItem","position":"1","item":{"name":"Home","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear"}},{"@type":"ListItem","position":"2","item":{"name":"Posters","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product-category\/posters\/"}},{"@type":"ListItem","position":"3","item":{"name":"Flying Ninja"}}]},{"@context":"https:\/\/schema.org\/","@type":"Product","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/flying-ninja\/","name":"Flying Ninja","image":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/wp-content\/uploads\/sites\/26\/2013\/06\/poster_2_up.jpg","description":"Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.","sku":"","offers":[{"@type":"Offer","price":"12.00","priceCurrency":"USD","availability":"https:\/\/schema.org\/InStock","url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/flying-ninja\/","seller":{"@type":"Organization","name":"Applique Men&#039;s Wear","url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear"}}],"aggregateRating":{"@type":"AggregateRating","ratingValue":"4.00","reviewCount":"4"}},{"@context":"https:\/\/schema.org\/","@graph":[{"@type":"Review","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/flying-ninja\/#comment-41","datePublished":"2013-06-07T11:52:25+00:00","description":"Really happy with this print. The colors are great, and the paper quality is good too.","itemReviewed":{"@type":"Product","name":"Flying Ninja"},"reviewRating":{"@type":"rating","ratingValue":"4"},"author":{"@type":"Person","name":"Cobus Bester"}},{"@type":"Review","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/flying-ninja\/#comment-42","datePublished":"2013-06-07T11:56:36+00:00","description":"You only get the picture, not the person holding it, something they don't mention in the description, now I've got to find my own person","itemReviewed":{"@type":"Product","name":"Flying Ninja"},"reviewRating":{"@type":"rating","ratingValue":"3"},"author":{"@type":"Person","name":"Andrew"}},{"@type":"Review","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/flying-ninja\/#comment-43","datePublished":"2013-06-07T12:19:25+00:00","description":"This is my favorite poster. In fact, I've ordered 5 of them!","itemReviewed":{"@type":"Product","name":"Flying Ninja"},"reviewRating":{"@type":"rating","ratingValue":"5"},"author":{"@type":"Person","name":"Coen Jacobs"}},{"@type":"Review","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/flying-ninja\/#comment-44","datePublished":"2013-06-07T12:59:49+00:00","description":"This is a fantastic quality print and is happily hanging framed on my wall now.","itemReviewed":{"@type":"Product","name":"Flying Ninja"},"reviewRating":{"@type":"rating","ratingValue":"4"},"author":{"@type":"Person","name":"Stuart"}}]}]}</script><script type='text/javascript'>
/* <![CDATA[ */
var carousel = {"type":"slider","auto_play":"1","slide_type":"df-slider-4"};
/* ]]> */
</script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/owl.carousel.min001e.js?ver=2.0.0'></script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/waypointcce7.js?ver=4.0.0'></script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/fitvids4963.js?ver=1.1'></script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/debounced-resize.js'></script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/parallaxc358.js?ver=1.1.3'></script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/grid605a.js?ver=2.2.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var inf = {"finishText":"All Post Loaded"};
/* ]]> */
</script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/infinite-scroll3c94.js?ver=2.1.0'></script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/jquery.scrolldepth.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var df = {"navClass":""};
/* ]]> */
</script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/main.minfeec.js?ver=1.6.5'></script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-includes/js/comment-reply.min55fe.js?ver=4.9.5'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */
</script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/plugins/contact-form-7/includes/js/scripts5597.js?ver=5.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var sb_instagram_js_options = {"sb_instagram_at":"612741813.3a81a9f.4472fbf925924a709fc46205b5ed9a6b"};
/* ]]> */
</script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/plugins/instagram-feed/js/sb-instagram.minf24c.js?ver=1.6'></script>
<script type='text/javascript' src='../../FrontOffice/s0.wp.com/wp-content/js/devicepx-jetpack1a52.js?ver=201814'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/applique\/menswear\/wp-admin\/admin-ajax.php","wc_ajax_url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min3d36.js?ver=3.3.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_single_product_params = {"i18n_required_rating_text":"Please select a rating","review_rating_required":"yes","flexslider":{"rtl":false,"animation":"slide","smoothHeight":true,"directionNav":false,"controlNav":"thumbnails","slideshow":false,"animationSpeed":500,"animationLoop":false,"allowOneSlide":false},"zoom_enabled":"","zoom_options":[],"photoswipe_enabled":"","photoswipe_options":{"shareEl":false,"closeOnScroll":false,"history":false,"hideAnimationDuration":0,"showAnimationDuration":0},"flexslider_enabled":""};
/* ]]> */
</script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/plugins/woocommerce/assets/js/frontend/single-product.min3d36.js?ver=3.3.1'></script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min44fd.js?ver=2.70'></script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min6b25.js?ver=2.1.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/applique\/menswear\/wp-admin\/admin-ajax.php","wc_ajax_url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min3d36.js?ver=3.3.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/applique\/menswear\/wp-admin\/admin-ajax.php","wc_ajax_url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_60bed813bcdf7f8a43b3e086e55ad050","fragment_name":"wc_fragments_60bed813bcdf7f8a43b3e086e55ad050"};
/* ]]> */
</script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min3d36.js?ver=3.3.1'></script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-includes/js/wp-embed.min55fe.js?ver=4.9.5'></script>
<script async="async" type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/plugins/akismet/_inc/form05da.js?ver=4.0.2'></script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/TheiaStickySidebar8d1e.js?ver=1.2.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var mc4wp_forms_config = [];
/* ]]> */
</script>
<script type='text/javascript' src='../../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/plugins/mailchimp-for-wp/assets/js/forms-api.min2d73.js?ver=3.1.11'></script>
<!--[if lte IE 9]>
<script type='text/javascript' src='http://dahz.daffyhazan.com/applique/menswear/wp-content/plugins/mailchimp-for-wp/assets/js/third-party/placeholders.min.js?ver=3.1.11'></script>
<![endif]-->
<script type="text/javascript">(function() {function addEventListener(element,event,handler) {
	if(element.addEventListener) {
		element.addEventListener(event,handler, false);
	} else if(element.attachEvent){
		element.attachEvent('on'+event,handler);
	}
}})();</script>
</body>

</html>