<?php 
      require("Connexion.php");    
      require("ContenuDAO.php");
      $cDB = new ContenuDAO();
    $taille_maxi = 250000;
    $dossier="../FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/";

    $desc = $_POST["desc"];
    $category = $_POST["category"];
    $price = $_POST["price"];

    if(isset($_POST["gps"])){
        $gps = $_POST["gps"];
    }
    if(isset($_POST["clim"])){
        $clim = $_POST["clim"];
    }

    $photo_nom = $_FILES['image']['name'];
    $photo_type = $_FILES['image']['type'];
    $photo_voiture = $_FILES['image']['tmp_name'];
    $taille = filesize($_FILES['image']['tmp_name']);

    $extension_valide = array('.jpg','.jpeg','.png');
    $extension = strrchr($_FILES['image']['name'],'.');

    if(!in_array($extension, $extension_valide)) //Si l'extension n'est pas dans le tableau
    {
        $erreur = 'Vous devez uploader un fichier de type png, jpg, jpeg';
    }
    if($taille>$taille_maxi){
        $erreur = 'Le fichier est trop lourd...';
    }
    if(!isset($erreur)) //S'il n'y a pas d'erreur, on upload
    {
        if(move_uploaded_file($_FILES['image']['tmp_name'], $dossier . basename($photo_nom))) //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
        {
            $cDB->insertContenu($desc,$photo_nom,$price,$category);
            echo 'Upload effectué avec succès !';
            echo $gps.$clim;
            header('Location:products.php');
        }
        else //Sinon (la fonction renvoie FALSE).
        {
            echo 'Echec de l\'upload !';
        }
    }
    else
    {
        echo $erreur;
    }
?>