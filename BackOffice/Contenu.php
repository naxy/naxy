<?php
	
	 class Contenu{
		public $id;
		public $text;
		public $image;
		public $remarque;
		public $idMenu;
		public $type;
		
                function Contenu($idE, $textE,$imageE,$remarqueE,$idMenuE,$typeE){
                    $this->setId($idE);
                    $this->settext($textE);
                    $this->setimage($imageE);
                    $this->setremarque($remarqueE);
                    $this->setMenu($idMenuE);
                    $this->settype($typeE);
                }
		function getId(){
			return $this->id;
		}
		
		function setId($newId){
			$this->id = $newId;
		}
				
		function gettext(){
			return $this->text;
		}
		
		function settext($newtext){
			$this->text = $newtext;
		}
		function getimage(){
			return $this->image;
		}
		
		function setimage($newimage){
			$this->image = $newimage;
		}

        function getremarque(){
			return $this->remarque;
		}
		
		function setremarque($newrmrq){
			$this->remarque = $newrmrq;
		}
        function getMenu(){
			return $this->idMenu;
		}
		
		function setMenu($newmenu){
			$this->idMenu = $newmenu;
		}
        function gettype(){
			return $this->type;
		}
		
		function settype($newtype){
			$this->type = $newtype;
		}
	?>