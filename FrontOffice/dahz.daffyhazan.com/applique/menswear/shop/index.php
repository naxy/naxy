<?php
  require("../../../../../BackOffice/Connexion.php");    
  require("../../../../../BackOffice/ContenuDAO.php");
  $cDB = new ContenuDAO();
  $content = $cDB->findContenu();

?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en-US"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en-US"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en-US"> <!--<![endif]-->
<html class="no-js" lang="en-US">

<!-- Mirrored from dahz.daffyhazan.com/applique/menswear/shop/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 11:11:15 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<?php include("header.php") ?>
<body class="archive post-type-archive post-type-archive-product woocommerce woocommerce-page df-skin-bold unknown windows glob-bw" dir="ltr" itemscope="itemscope" itemtype="http://schema.org/NewsArticle">
<div class="ajax_loader">
<div class="ajax_loader_1">
<div class="double_pulse"><div class="double-bounce1" style="background-color:#1d1c1b"></div><div class="double-bounce2" style="background-color:#1d1c1b"></div></div>
</div>
</div>
<div id="wrapper" class="df-wrapper">
<div class="df-mobile-menu">
<div class="inner-wrapper container">
<div class="df-ham-menu">
<div class="col-left">
<a href="#">
<span class="df-top"></span>
<span class="df-middle"></span>
<span class="df-bottom"></span>
</a>
</div>
<div class="col-right">
<a href="#" class="mobile-subs"><i class="ion-ios-email-outline"></i></a>
<a href="#" class="mobile-search"><i class="ion-ios-search-strong"></i></a>
</div>
</div>
<div class="df-menu-content">
<div class="content-wrap">
<div class="main-navigation" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<div class="nav-wrapper-inner ">
<div class="sticky-logo">
<a href="../index.html" class="df-sitename" title="Applique Men&#039;s Wear" itemprop="headline">
<img src="FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2016/02/menswear-sticky.png" alt="Applique Men&#039;s Wear">
</a>
</div>
<?php include("util.php") ?>
<div class="sticky-btp">
<a class="scroll-top"><i class="ion-ios-arrow-thin-up"></i><i class="ion-ios-arrow-thin-up"></i></a>
</div>
</div>
</div>
<div class="df-social-connect"><a class="df-facebook" href="#" target="_blank"><i class="fa fa-facebook"></i><span class="social-text">Facebook</span></a><a class="df-twitter" href="#" target="_blank"><i class="fa fa-twitter"></i><span class="social-text">Twitter</span></a><a class="df-instagram" href="#" target="_blank"><i class="fa fa-instagram"></i><span class="social-text">Instagram</span></a><a class="df-pinterest" href="#" target="_blank"><i class="fa fa-pinterest"></i><span class="social-text">pinterest</span></a><a class="df-bloglovin" href="#" target="_blank"><i class="fa fa-heart"></i><span class="social-text">Bloglovin</span></a><a class="df-gplus" href="#" target="_blank"><i class="fa fa-google-plus"></i><span class="social-text">Google+</span></a></div> </div>
</div>
</div>
</div>
<div id="masthead" role="banner" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
<div class="df-header-inner">
<div id="branding" class="site-branding border-bottom aligncenter">
<div class="container">
<a href="../index.html" class="df-sitename" title="Applique Men&#039;s Wear" id="site-title" itemprop="headline">
<img src="FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2016/02/tennispoint-logo.png" alt="Applique Men&#039;s Wear">
</a>
</div>
</div>
<div class="main-navigation" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<div class="nav-wrapper-inner ">
<div class="sticky-logo">
<a href="../index.html" class="df-sitename" title="Applique Men&#039;s Wear" itemprop="headline">
<img src="FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2016/02/menswear-sticky.png" alt="Applique Men&#039;s Wear">
</a>
</div>
<?php include("menus.php") ?>
<div class="sticky-btp">
<a class="scroll-top"><i class="ion-ios-arrow-thin-up"></i><i class="ion-ios-arrow-thin-up"></i></a>
</div>
</div>
</div>
</div>
</div>
<div class="df-header-title aligncenter " style="background-color: #FFFFFF;"><div class="container"><div class="df-header"><h1 class="entry-title display-1" itemprop="headline">Shop</h1></div></div></div>
<div id="content-wrap">
<div class="main-sidebar-container container">
<div class="row">
<div id="df-content" class="df-content col-md-12 df-no-sidebar">
<p class="woocommerce-result-count">
Showing 1&ndash;16 of 23 results</p>
<form class="woocommerce-ordering" method="get">
<select name="orderby" class="orderby">
<option value="menu_order" selected='selected'>Default sorting</option>
<option value="popularity">Sort by popularity</option>
<option value="rating">Sort by average rating</option>
<option value="date">Sort by newness</option>
<option value="price">Sort by price: low to high</option>
<option value="price-desc">Sort by price: high to low</option>
</select>
<input type="hidden" name="paged" value="1" />
</form>
<ul class="products columns-4">
<li class="post-654 product type-product status-publish has-post-thumbnail product_cat-posters first instock sale shipping-taxable purchasable product-type-simple">

<?php include("../utilitaire.php") ?>
<?php $nb = sizeof($content);
  for($i=0; $i<$nb; $i++){ ?>
<a href="product/fiche/<?php echo getUrl($content[$i]->Text); ?>-<?php echo $content[$i]->id; ?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
<img width="300" height="300" src="FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content[$i]->Image; ?>" 
class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" 
srcset="//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/ 300w, 
//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content[$i]->Image; ?> 150w,
//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content[$i]->Image; ?> 768w, 
//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content[$i]->Image; ?> 180w, 
//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content[$i]->Image; ?> 600w, 
//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content[$i]->Image; ?> 250w, 
//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content[$i]->Image; ?> 80w, 
//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content[$i]->Image; ?> 330w, 
//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content[$i]->Image; ?> 160w, 
//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content[$i]->Image; ?> 293w, 
//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content[$i]->Image; ?> 1000w" 
sizes="(max-width: 300px) 100vw, 300px" data-attachment-id="71" data-permalink="//dahz.daffyhazan.com/applique/menswear/?attachment_id=71" 
data-orig-file="//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content[$i]->Image; ?>" 
data-orig-size="1000,1000" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;
focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;
title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="poster_2_up" data-image-description="" 
data-medium-file="//dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content[$i]->Image; ?>" 
data-large-file="FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2013/06/<?php echo $content[$i]->Image; ?>" /><h4><?php echo $content[$i]->Text; ?></h4><div 
class="star-rating"><span style="width:80%">Rated <strong class="rating">5.00</strong> out of 5</span></div>
<span class="price"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span><?php echo $content[$i]->remarque; ?></span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span><?php echo $content[$i]->remarque; ?></span></ins></span>
</a><a href="index5d11.html?add-to-cart=654" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="654" data-product_sku="" aria-label="Add &ldquo;Flying Ninja&rdquo; to your cart" rel="nofollow">Add cart</a></li>
<li class="post-651 product type-product status-publish has-post-thumbnail product_cat-clothing product_cat-t-shirts  instock shipping-taxable purchasable product-type-simple">

<?php } ?>
<nav class="woocommerce-pagination">
<ul class='page-numbers'>
<li><span aria-current='page' class='page-numbers current'>1</span></li>
<li><a class='page-numbers' href='page/2/index.html'>2</a></li>
<li><a class="next page-numbers" href="page/2/index.html">&rarr;</a></li>
</ul>
</nav>
</div>
</div>
</div>
<div class="df-social-connect"><a class="df-facebook" href="#" target="_blank"><i class="fa fa-facebook"></i><span class="social-text">Facebook</span></a><a class="df-twitter" href="#" target="_blank"><i class="fa fa-twitter"></i><span class="social-text">Twitter</span></a><a class="df-instagram" href="#" target="_blank"><i class="fa fa-instagram"></i><span class="social-text">Instagram</span></a><a class="df-pinterest" href="#" target="_blank"><i class="fa fa-pinterest"></i><span class="social-text">pinterest</span></a><a class="df-bloglovin" href="#" target="_blank"><i class="fa fa-heart"></i><span class="social-text">Bloglovin</span></a><a class="df-gplus" href="#" target="_blank"><i class="fa fa-google-plus"></i><span class="social-text">Google+</span></a></div> <div class="df-misc-section"><a class="df-misc-search"><span class="df-misc-text">Search</span><i class="ion-ios-search-strong"></i></a><a class="df-misc-mail"><span class="df-misc-text">Subscribe</span><i class="ion-android-remove"></i></a><a class="df-misc-archive" href="../archives/index.html"><span class="df-misc-text">Archive</span><i class="ion-android-remove"></i></a></div>
</div>
<div id="footer-colophon" role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter" class="site-footer aligncenter">
<div class="df-footer-top">
<div id="text-3" class="widget widget_text"> <div class="textwidget"><div id="sb_instagram" class="sbi sbi_col_6" style="width:100%; " data-id="442412877" data-num="12" data-res="auto" data-cols="6" data-options='{&quot;sortby&quot;: &quot;none&quot;, &quot;showbio&quot;: &quot;true&quot;, &quot;headercolor&quot;: &quot;&quot;, &quot;imagepadding&quot;: &quot;0&quot;}'><div id="sbi_images" style="padding: 0px;"><div class="sbi_loader fa-spin"></div></div><div id="sbi_load"></div></div></div>
</div>
</div>
<div class="df-footer-bottom border-top">
<div class="container">
<div class="df-foot-logo">
<a href="../index.html" class="df-sitename" title="Applique Men&#039;s Wear">
<img src="FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2016/02/tennispoint-logo.png" alt="Applique Men&#039;s Wear">
</a>
</div>
<div class="main-navigation" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<div class="container"><ul class="nav aligncenter"><li id="menu-item-86" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-86"><a href="#">Facebook</a></li>
<li id="menu-item-87" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-87"><a href="#">Instagram</a></li>
<li id="menu-item-247" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-247"><a href="#">Twitter</a></li>
<li id="menu-item-88" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-88"><a href="#">Bloglovin</a></li>
<li id="menu-item-89" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-89"><a href="#">Pinterest</a></li>
</ul></div>
</div>
<div class="siteinfo">
<p>Copyright &copy; <span itemprop="copyrightYear">2018</span> <span itemprop="copyrightHolder">DAHZ</span> All Rights Reserved. Applique Men&#039;s Wear.</p>
</div>
<a href="#" class="scroll-top">
<i class="ion-ios-arrow-thin-up"></i>
<i class="ion-ios-arrow-thin-up"></i>
</a>
<div class="df-floating-search">
<div class="search-container-close"></div>
<div class="container df-floating-search-form">
<form class="df-floating-search-form-wrap col-md-8 col-md-push-2" method="get" action="http://dahz.daffyhazan.com/applique/menswear/">
<label class="label-text">
<input type="search" class="df-floating-search-form-input" placeholder="What are you looking for" value="" name="s" title="Search for:">
</label>
<div class="df-floating-search-close"><i class="ion-ios-close-empty"></i></div>
</form>
</div>
</div>
<div class="df-floating-subscription">
<div class="container-close"></div>
<div class="container">
<div class="row">
<div class="wrapper col-md-8 col-md-push-2">
<div class="row flex-box">
<div class="col-left col-md-5">
<div class="wrap">
<img src="FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2016/02/subscribe.jpg" alt="" />
</div>
</div>
<div class="col-right col-md-7">
<div class="wrap">
<form id="mc4wp-form-2" class="mc4wp-form mc4wp-form-65" method="post" data-id="65" data-name="Newsletter"><div class="mc4wp-form-fields"><h1>Never Miss a Post</h1>
<p>A black and white style philosophy is only one click away</p>
<p>
<label>Email address: </label>
<input type="email" name="EMAIL" placeholder="Your email address" required />
</p>
<p>
<input type="submit" value="Sign up" />
</p><div style="display: none;"><input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off" /></div><input type="hidden" name="_mc4wp_timestamp" value="1522839211" /><input type="hidden" name="_mc4wp_form_id" value="65" /><input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-2" /></div><div class="mc4wp-response"></div></form> </div>
</div>
</div>
<div class="df-floating-close"><i class="ion-ios-close-empty"></i></div>
</div>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-20219691-40', 'auto');
  ga('send', 'pageview');

</script><script type="application/ld+json">{"@context":"https:\/\/schema.org\/","@graph":[{"@type":"Product","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/flying-ninja\/","name":"Flying Ninja","url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/flying-ninja\/"},{"@type":"Product","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/happy-ninja\/","name":"Happy Ninja","url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/happy-ninja\/"},{"@type":"Product","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/happy-ninja-2\/","name":"Happy Ninja","url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/happy-ninja-2\/"},{"@type":"Product","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/ninja-silhouette\/","name":"Ninja Silhouette","url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/ninja-silhouette\/"},{"@type":"Product","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/ninja-silhouette-2\/","name":"Ninja Silhouette","url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/ninja-silhouette-2\/"},{"@type":"Product","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/patient-ninja\/","name":"Patient Ninja","url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/patient-ninja\/"},{"@type":"Product","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/premium-quality\/","name":"Premium Quality","url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/premium-quality\/"},{"@type":"Product","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/premium-quality-2\/","name":"Premium Quality","url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/premium-quality-2\/"},{"@type":"Product","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/ship-your-idea\/","name":"Ship Your Idea","url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/ship-your-idea\/"},{"@type":"Product","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/ship-your-idea-2\/","name":"Ship Your Idea","url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/ship-your-idea-2\/"},{"@type":"Product","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/ship-your-idea-3\/","name":"Ship Your Idea","url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/ship-your-idea-3\/"},{"@type":"Product","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/woo-album-1\/","name":"Woo Album #1","url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/woo-album-1\/"},{"@type":"Product","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/woo-album-2\/","name":"Woo Album #2","url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/woo-album-2\/"},{"@type":"Product","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/woo-album-3\/","name":"Woo Album #3","url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/woo-album-3\/"},{"@type":"Product","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/woo-album-4\/","name":"Woo Album #4","url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/woo-album-4\/"},{"@type":"Product","@id":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/woo-logo\/","name":"Woo Logo","url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/product\/woo-logo\/"}]}</script><script type='text/javascript'>
/* <![CDATA[ */
var carousel = {"type":"slider","auto_play":"1","slide_type":"df-slider-4"};
/* ]]> */
</script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/owl.carousel.min001e.js?ver=2.0.0'></script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/waypointcce7.js?ver=4.0.0'></script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/fitvids4963.js?ver=1.1'></script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/debounced-resize.js'></script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/parallaxc358.js?ver=1.1.3'></script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/grid605a.js?ver=2.2.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var inf = {"finishText":"All Post Loaded"};
/* ]]> */
</script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/infinite-scroll3c94.js?ver=2.1.0'></script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/jquery.scrolldepth.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var df = {"navClass":""};
/* ]]> */
</script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/main.minfeec.js?ver=1.6.5'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */
</script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/plugins/contact-form-7/includes/js/scripts5597.js?ver=5.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var sb_instagram_js_options = {"sb_instagram_at":"612741813.3a81a9f.4472fbf925924a709fc46205b5ed9a6b"};
/* ]]> */
</script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/plugins/instagram-feed/js/sb-instagram.minf24c.js?ver=1.6'></script>
<script type='text/javascript' src='FrontOffice/s0.wp.com/wp-content/js/devicepx-jetpack1a52.js?ver=201814'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/applique\/menswear\/wp-admin\/admin-ajax.php","wc_ajax_url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min3d36.js?ver=3.3.1'></script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min44fd.js?ver=2.70'></script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min6b25.js?ver=2.1.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/applique\/menswear\/wp-admin\/admin-ajax.php","wc_ajax_url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min3d36.js?ver=3.3.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/applique\/menswear\/wp-admin\/admin-ajax.php","wc_ajax_url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_60bed813bcdf7f8a43b3e086e55ad050","fragment_name":"wc_fragments_60bed813bcdf7f8a43b3e086e55ad050"};
/* ]]> */
</script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min3d36.js?ver=3.3.1'></script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-includes/js/wp-embed.min55fe.js?ver=4.9.5'></script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/themes/applique/assets/js/TheiaStickySidebar8d1e.js?ver=1.2.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var mc4wp_forms_config = [];
/* ]]> */
</script>
<script type='text/javascript' src='FrontOffice/dahz.daffyhazan.com/applique/menswear/wp-content/plugins/mailchimp-for-wp/assets/js/forms-api.min2d73.js?ver=3.1.11'></script>
<!--[if lte IE 9]>
<script type='text/javascript' src='http://dahz.daffyhazan.com/applique/menswear/wp-content/plugins/mailchimp-for-wp/assets/js/third-party/placeholders.min.js?ver=3.1.11'></script>
<![endif]-->
<script type="text/javascript">(function() {function addEventListener(element,event,handler) {
	if(element.addEventListener) {
		element.addEventListener(event,handler, false);
	} else if(element.attachEvent){
		element.attachEvent('on'+event,handler);
	}
}})();</script>
</body>

<!-- Mirrored from dahz.daffyhazan.com/applique/menswear/shop/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 11:13:23 GMT -->
</html>