<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en-US"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en-US"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en-US"> <!--<![endif]-->
<html class="no-js" lang="en-US">

<!-- Mirrored from dahz.daffyhazan.com/applique/menswear/contact-me/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 11:09:55 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<?php include("header.php") ?>
<body class="page-template-default page page-id-133 df-skin-bold unknown windows glob-bw" dir="ltr" itemscope="itemscope" itemtype="http://schema.org/WebPage">
<div class="ajax_loader">
<div class="ajax_loader_1">
<div class="double_pulse"><div class="double-bounce1" style="background-color:#1d1c1b"></div><div class="double-bounce2" style="background-color:#1d1c1b"></div></div>
</div>
</div>
<div id="wrapper" class="df-wrapper">
<div class="df-mobile-menu">
<div class="inner-wrapper container">
<div class="df-ham-menu">
<div class="col-left">
<a href="#">
<span class="df-top"></span>
<span class="df-middle"></span>
<span class="df-bottom"></span>
</a>
</div>
<div class="col-right">
<a href="#" class="mobile-subs"><i class="ion-ios-email-outline"></i></a>
<a href="#" class="mobile-search"><i class="ion-ios-search-strong"></i></a>
</div>
</div>
<div class="df-menu-content">
<div class="content-wrap">
<div class="main-navigation" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<div class="nav-wrapper-inner ">
<div class="sticky-logo">
<a href="../index.html" class="df-sitename" title="Applique Men&#039;s Wear" itemprop="headline">
<img src="../wp-content/uploads/sites/26/2016/02/menswear-sticky.png" alt="Applique Men&#039;s Wear">
</a>
</div>
<?php include("util.php") ?>
<div class="sticky-btp">
<a class="scroll-top"><i class="ion-ios-arrow-thin-up"></i><i class="ion-ios-arrow-thin-up"></i></a>
</div>
</div>
</div>
<div class="df-social-connect"><a class="df-facebook" href="#" target="_blank"><i class="fa fa-facebook"></i><span class="social-text">Facebook</span></a><a class="df-twitter" href="#" target="_blank"><i class="fa fa-twitter"></i><span class="social-text">Twitter</span></a><a class="df-instagram" href="#" target="_blank"><i class="fa fa-instagram"></i><span class="social-text">Instagram</span></a><a class="df-pinterest" href="#" target="_blank"><i class="fa fa-pinterest"></i><span class="social-text">pinterest</span></a><a class="df-bloglovin" href="#" target="_blank"><i class="fa fa-heart"></i><span class="social-text">Bloglovin</span></a><a class="df-gplus" href="#" target="_blank"><i class="fa fa-google-plus"></i><span class="social-text">Google+</span></a></div> </div>
</div>
</div>
</div>
<div id="masthead" role="banner" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
<div class="df-header-inner">
<div id="branding" class="site-branding border-bottom aligncenter">
<div class="container">
<a href="../index.html" class="df-sitename" title="Applique Men&#039;s Wear" id="site-title" itemprop="headline">
<img src="../wp-content/uploads/sites/26/2016/02/menswear-logo.png" alt="Applique Men&#039;s Wear">
</a>
</div>
</div>
<div class="main-navigation" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<div class="nav-wrapper-inner ">
<div class="sticky-logo">
<a href="../index.html" class="df-sitename" title="Applique Men&#039;s Wear" itemprop="headline">
<img src="../wp-content/uploads/sites/26/2016/02/menswear-sticky.png" alt="Applique Men&#039;s Wear">
</a>
</div>
<?php include("menus.php") ?>
<div class="sticky-btp">
<a class="scroll-top"><i class="ion-ios-arrow-thin-up"></i><i class="ion-ios-arrow-thin-up"></i></a>
</div>
</div>
</div>
</div>
</div>
<div class="df-header-title aligncenter"><div class="container"><div class="df-page-subtitle">CONNECT WITH US</div><div class="df-header"><h1 class="entry-title display-1" itemprop="headline">Contact Me</h1></div></div></div>
<div id="content-wrap">
<div class="container main-sidebar-container">
<div class="row">
<div id="df-content" class="df-content col-md-8" role="main" itemprop="mainContentOfPage">
<div id="post-133" class="post-133 page type-page status-publish has-post-thumbnail hentry" itemscope itemtype="http://schema.org/CreativeWork" role="article">
<div class="featured-media">
<img width="1200" height="857" src="../wp-content/uploads/sites/26/2015/11/menswear_02.jpg" class="attachment-full size-full wp-post-image" alt="" srcset="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/11/menswear_02.jpg 1200w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/11/menswear_02-300x214.jpg 300w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/11/menswear_02-768x548.jpg 768w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/11/menswear_02-1024x731.jpg 1024w" sizes="(max-width: 1200px) 100vw, 1200px" data-attachment-id="571" data-permalink="http://dahz.daffyhazan.com/applique/menswear/contact-me/menswear_02/" data-orig-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/11/menswear_02.jpg" data-orig-size="1200,857" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="menswear_02" data-image-description="" data-medium-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/11/menswear_02-300x214.jpg" data-large-file="../wp-content/uploads/sites/26/2015/11/menswear_02-1024x731.jpg" />
</div>
<div class="entry-content" itemprop="text">
<p>This contact form is generated with a free third party plugin called <a href="http://wordpress.org/plugins/contact-form-7/" target="_blank" rel="nofollow">Contact Form 7</a>. We believe that a blog theme, should feel fluid, light, and intuitive. That’s what we are aiming to make with applique, using clean code and detailed designs in each our custom features maintaining balance between design and technology. We present to you Applique our most elegant theme yet.</p>
<div role="form" class="wpcf7" id="wpcf7-f4-p133-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="http://dahz.daffyhazan.com/applique/menswear/contact-me/#wpcf7-f4-p133-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="4" />
<input type="hidden" name="_wpcf7_version" value="5.0" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4-p133-o1" />
<input type="hidden" name="_wpcf7_container_post" value="133" />
</div>
<p>Your Name (required)<br />
<span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span> </p>
<p>Your Email (required)<br />
<span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" /></span> </p>
<p>Subject<br />
<span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" /></span> </p>
<p>Your Message<br />
<span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span> </p>
<p><span class="wpcf7-form-control-wrap topyenoh-wrap" style="display:none !important; visibility:hidden !important;"><label class="hp-message">Please leave this field empty.</label><input class="wpcf7-form-control wpcf7-text" type="text" name="topyenoh" value="" size="40" tabindex="-1" autocomplete="nope" /></span></p>
<p><input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit" /></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
<p>&nbsp;</p>
</div>
<div class="clear"></div>
<div class="clear"></div>
</div>
</div>
<div class="df-sidebar col-md-4" role="complementary" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
<div class="sticky-sidebar">
<div id="about-widget-2" class="widget about-widget"><h4>About Me</h4><div class="about-content aligncenter"><div class="about-avatar"><img src="../wp-content/uploads/sites/26/2016/02/about.jpg" alt="About Me" /></div><div class="content-wrap"><p>Applique is a fully responsive Fashion blog theme that designed with style publishers in mind! We believe that a blog theme, should feel fluid, light, and intuitive.</p><ul><li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li><li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li><li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li><li><a href="#" target="_blank"><i class="fa fa-heart"></i></a></li></ul></div></div></div><div id="category-widget-2" class="widget category-widget"><h4>Categories</h4><div class="cat-banner-4 df-category-widget aligncenter" style="background-image: url(../wp-content/uploads/sites/26/2016/02/category_03.jpg);"><a href="../category/be-social/index.html" class="df-cat-link"></a><h3>Be Social</h3></div><div class="cat-banner-5 df-category-widget aligncenter" style="background-image: url(../wp-content/uploads/sites/26/2016/02/category_02.jpg);"><a href="../category/music/index.html" class="df-cat-link"></a><h3>Music</h3></div><div class="cat-banner-6 df-category-widget aligncenter" style="background-image: url(../wp-content/uploads/sites/26/2016/02/category_01.jpg);"><a href="../category/street-style/index.html" class="df-cat-link"></a><h3>Street Style</h3></div></div><div id="recent-medium-widget-6" class="widget recent-medium-widget"><h4>Recent Post</h4>
<ul class="recent-wrapper ver2">
<li class=col-md-6>
<div class="featured-media">
<a href="../weekly-inspiration/index.html">
<img width="160" height="160" src="../wp-content/uploads/sites/26/2015/12/menswear_24-160x160.jpg" class="attachment-recent-medium2 size-recent-medium2 wp-post-image" alt="" srcset="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_24-160x160.jpg 160w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_24-150x150.jpg 150w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_24-250x250.jpg 250w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_24-600x600.jpg 600w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_24-80x80.jpg 80w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_24-330x330.jpg 330w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_24-293x293.jpg 293w" sizes="(max-width: 160px) 100vw, 160px" data-attachment-id="528" data-permalink="http://dahz.daffyhazan.com/applique/menswear/weekly-inspiration/menswear_24/" data-orig-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_24.jpg" data-orig-size="1200,857" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="menswear_24" data-image-description="" data-medium-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_24-300x214.jpg" data-large-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_24-1024x731.jpg" /> </a>
</div>
<div class="recent-content">
<div class="df-single-category"><span class="entry-terms category" itemprop="articleSection"><a href="../category/the-modern-man/index.html" rel="category tag">The Modern Man</a></span></div>
<h5 class="entry-title" itemprop="headline"><a href="../weekly-inspiration/index.html">Weekly Inspiration</a></h5>
<div class="df-post-on"><time class="entry-published updated" datetime="2015-12-11T07:14:03+00:00" title="Friday, December 11, 2015, 7:14 am"><a href="../2015/12/11/index.html"><meta itemprop="datePublished" content="December 11, 2015">December 11, 2015</a></time></div>
</div>
</li>
<li class=col-md-6>
<div class="featured-media">
<a href="../praha-street-style-february-2016/index.html">
<img width="160" height="160" src="../wp-content/uploads/sites/26/2015/12/menswear_06-160x160.jpg" class="attachment-recent-medium2 size-recent-medium2 wp-post-image" alt="" srcset="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06-160x160.jpg 160w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06-150x150.jpg 150w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06-250x250.jpg 250w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06-600x600.jpg 600w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06-80x80.jpg 80w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06-330x330.jpg 330w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06-293x293.jpg 293w" sizes="(max-width: 160px) 100vw, 160px" data-attachment-id="540" data-permalink="http://dahz.daffyhazan.com/applique/menswear/praha-street-style-february-2016/menswear_06/" data-orig-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06.jpg" data-orig-size="1200,857" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="menswear_06" data-image-description="" data-medium-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06-300x214.jpg" data-large-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_06-1024x731.jpg" /> </a>
</div>
<div class="recent-content">
<div class="df-single-category"><span class="entry-terms category" itemprop="articleSection"><a href="../category/my-story/index.html" rel="category tag">My Story</a></span></div>
<h5 class="entry-title" itemprop="headline"><a href="../praha-street-style-february-2016/index.html">Praha Street Style&hellip;</a></h5>
<div class="df-post-on"><time class="entry-published updated" datetime="2015-12-11T07:12:00+00:00" title="Friday, December 11, 2015, 7:12 am"><a href="../2015/12/11/index.html"><meta itemprop="datePublished" content="December 11, 2015">December 11, 2015</a></time></div>
</div>
</li>
<li class=col-md-6>
<div class="featured-media">
<a href="../top-ten-stylish-gadget-2016/index.html">
 <img width="160" height="160" src="../wp-content/uploads/sites/26/2015/12/menswear_28-160x160.jpg" class="attachment-recent-medium2 size-recent-medium2 wp-post-image" alt="" srcset="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_28-160x160.jpg 160w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_28-150x150.jpg 150w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_28-250x250.jpg 250w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_28-600x600.jpg 600w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_28-80x80.jpg 80w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_28-330x330.jpg 330w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_28-293x293.jpg 293w" sizes="(max-width: 160px) 100vw, 160px" data-attachment-id="543" data-permalink="http://dahz.daffyhazan.com/applique/menswear/top-ten-stylish-gadget-2016/menswear_28/" data-orig-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_28.jpg" data-orig-size="1200,857" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="menswear_28" data-image-description="" data-medium-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_28-300x214.jpg" data-large-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_28-1024x731.jpg" /> </a>
</div>
<div class="recent-content">
<div class="df-single-category"><span class="entry-terms category" itemprop="articleSection"><a href="../category/the-modern-man/index.html" rel="category tag">The Modern Man</a></span></div>
<h5 class="entry-title" itemprop="headline"><a href="../top-ten-stylish-gadget-2016/index.html">Top Ten Stylish&hellip;</a></h5>
<div class="df-post-on"><time class="entry-published updated" datetime="2015-12-11T07:09:01+00:00" title="Friday, December 11, 2015, 7:09 am"><a href="../2015/12/11/index.html"><meta itemprop="datePublished" content="December 11, 2015">December 11, 2015</a></time></div>
</div>
</li>
<li class=col-md-6>
<div class="featured-media">
<a href="../2-years-old-jeans/index.html">
<img width="160" height="160" src="../wp-content/uploads/sites/26/2015/12/menswear_23-160x160.jpg" class="attachment-recent-medium2 size-recent-medium2 wp-post-image" alt="" srcset="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23-160x160.jpg 160w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23-150x150.jpg 150w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23-250x250.jpg 250w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23-600x600.jpg 600w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23-80x80.jpg 80w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23-330x330.jpg 330w, http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23-293x293.jpg 293w" sizes="(max-width: 160px) 100vw, 160px" data-attachment-id="547" data-permalink="http://dahz.daffyhazan.com/applique/menswear/2-years-old-jeans/menswear_23/" data-orig-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23.jpg" data-orig-size="1200,857" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="menswear_23" data-image-description="" data-medium-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23-300x214.jpg" data-large-file="http://dahz.daffyhazan.com/applique/menswear/wp-content/uploads/sites/26/2015/12/menswear_23-1024x731.jpg" /> </a>
</div>
<div class="recent-content">
<div class="df-single-category"><span class="entry-terms category" itemprop="articleSection"><a href="../category/my-story/index.html" rel="category tag">My Story</a></span></div> 
<h5 class="entry-title" itemprop="headline"><a href="../2-years-old-jeans/index.html">2 Years Old&hellip;</a></h5>
<div class="df-post-on"><time class="entry-published updated" datetime="2015-12-11T07:06:17+00:00" title="Friday, December 11, 2015, 7:06 am"><a href="../2015/12/11/index.html"><meta itemprop="datePublished" content="December 11, 2015">December 11, 2015</a></time></div>
</div>
</li>
</ul>
</div><div id="tag_cloud-2" class="widget widget_tag_cloud"><h4>Tags</h4><div class="tagcloud"><a href="../tag/audio/index.html" class="tag-cloud-link tag-link-17 tag-link-position-1" style="font-size: 0.65em;" aria-label="Audio (1 item)">Audio</a>
<a href="../tag/beauty/index.html" class="tag-cloud-link tag-link-12 tag-link-position-2" style="font-size: 0.65em;" aria-label="Beauty (6 items)">Beauty</a>
<a href="../tag/blogger/index.html" class="tag-cloud-link tag-link-9 tag-link-position-3" style="font-size: 0.65em;" aria-label="Blogger (9 items)">Blogger</a>
<a href="../tag/fashion/index.html" class="tag-cloud-link tag-link-8 tag-link-position-4" style="font-size: 0.65em;" aria-label="Fashion (19 items)">Fashion</a>
<a href="../tag/look/index.html" class="tag-cloud-link tag-link-13 tag-link-position-5" style="font-size: 0.65em;" aria-label="Look (19 items)">Look</a>
<a href="../tag/music/index.html" class="tag-cloud-link tag-link-18 tag-link-position-6" style="font-size: 0.65em;" aria-label="Music (1 item)">Music</a>
<a href="../tag/outfit/index.html" class="tag-cloud-link tag-link-7 tag-link-position-7" style="font-size: 0.65em;" aria-label="Outfit (11 items)">Outfit</a>
<a href="../tag/products/index.html" class="tag-cloud-link tag-link-11 tag-link-position-8" style="font-size: 0.65em;" aria-label="Products (7 items)">Products</a>
<a href="../tag/travel/index.html" class="tag-cloud-link tag-link-10 tag-link-position-9" style="font-size: 0.65em;" aria-label="Travel (5 items)">Travel</a>
<a href="../tag/video/index.html" class="tag-cloud-link tag-link-22 tag-link-position-10" style="font-size: 0.65em;" aria-label="Video (1 item)">Video</a></div>
</div><div id="ads300-widget-2" class="widget ads300-widget"><div class="ads300-wrap"><a href="http://themeforest.net/item/fashion-blog-theme-applique/13915999?utm_source=sharetw"><img src="../wp-content/uploads/sites/26/2016/02/ad.jpg" alt="Ads" /></a></div></div>
</div>
</div>
</div>
</div>
<div class="df-social-connect"><a class="df-facebook" href="#" target="_blank"><i class="fa fa-facebook"></i><span class="social-text">Facebook</span></a><a class="df-twitter" href="#" target="_blank"><i class="fa fa-twitter"></i><span class="social-text">Twitter</span></a><a class="df-instagram" href="#" target="_blank"><i class="fa fa-instagram"></i><span class="social-text">Instagram</span></a><a class="df-pinterest" href="#" target="_blank"><i class="fa fa-pinterest"></i><span class="social-text">pinterest</span></a><a class="df-bloglovin" href="#" target="_blank"><i class="fa fa-heart"></i><span class="social-text">Bloglovin</span></a><a class="df-gplus" href="#" target="_blank"><i class="fa fa-google-plus"></i><span class="social-text">Google+</span></a></div> <div class="df-misc-section"><a class="df-misc-search"><span class="df-misc-text">Search</span><i class="ion-ios-search-strong"></i></a><a class="df-misc-mail"><span class="df-misc-text">Subscribe</span><i class="ion-android-remove"></i></a><a class="df-misc-archive" href="../archives/index.html"><span class="df-misc-text">Archive</span><i class="ion-android-remove"></i></a></div>
</div>
<div id="footer-colophon" role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter" class="site-footer aligncenter">
<div class="df-footer-top">
<div id="text-3" class="widget widget_text"> <div class="textwidget"><div id="sb_instagram" class="sbi sbi_col_6" style="width:100%; " data-id="442412877" data-num="12" data-res="auto" data-cols="6" data-options='{&quot;sortby&quot;: &quot;none&quot;, &quot;showbio&quot;: &quot;true&quot;, &quot;headercolor&quot;: &quot;&quot;, &quot;imagepadding&quot;: &quot;0&quot;}'><div id="sbi_images" style="padding: 0px;"><div class="sbi_loader fa-spin"></div></div><div id="sbi_load"></div></div></div>
</div>
</div>
<div class="df-footer-bottom border-top">

<div class="container">
<div class="df-foot-logo">
<a href="../index.html" class="df-sitename" title="Applique Men&#039;s Wear">
<img src="../wp-content/uploads/sites/26/2016/02/menswear-logo.png" alt="Applique Men&#039;s Wear">
</a>
</div>
<div class="main-navigation" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<div class="container"><ul class="nav aligncenter"><li id="menu-item-86" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-86"><a href="#">Facebook</a></li>
<li id="menu-item-87" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-87"><a href="#">Instagram</a></li>
<li id="menu-item-247" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-247"><a href="#">Twitter</a></li>
<li id="menu-item-88" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-88"><a href="#">Bloglovin</a></li>
<li id="menu-item-89" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-89"><a href="#">Pinterest</a></li>
</ul></div>
</div>
<div class="siteinfo">
<p>Copyright &copy; <span itemprop="copyrightYear">2018</span> <span itemprop="copyrightHolder">DAHZ</span> All Rights Reserved. Applique Men&#039;s Wear.</p>
</div>
<a href="#" class="scroll-top">
<i class="ion-ios-arrow-thin-up"></i>
<i class="ion-ios-arrow-thin-up"></i>
</a>
<div class="df-floating-search">
<div class="search-container-close"></div>
<div class="container df-floating-search-form">
<form class="df-floating-search-form-wrap col-md-8 col-md-push-2" method="get" action="http://dahz.daffyhazan.com/applique/menswear/">
<label class="label-text">
<input type="search" class="df-floating-search-form-input" placeholder="What are you looking for" value="" name="s" title="Search for:">
</label>
<div class="df-floating-search-close"><i class="ion-ios-close-empty"></i></div>
</form>
</div>
</div>
<div class="df-floating-subscription">
<div class="container-close"></div>
<div class="container">
<div class="row">
<div class="wrapper col-md-8 col-md-push-2">
<div class="row flex-box">
<div class="col-left col-md-5">
<div class="wrap">
<img src="../wp-content/uploads/sites/26/2016/02/subscribe.jpg" alt="" />
</div>
</div>
<div class="col-right col-md-7">
<div class="wrap">
<form id="mc4wp-form-2" class="mc4wp-form mc4wp-form-65" method="post" data-id="65" data-name="Newsletter"><div class="mc4wp-form-fields"><h1>Never Miss a Post</h1>
<p>A black and white style philosophy is only one click away</p>
<p>
<label>Email address: </label>
<input type="email" name="EMAIL" placeholder="Your email address" required />
</p>
<p>
<input type="submit" value="Sign up" />
</p><div style="display: none;"><input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off" /></div><input type="hidden" name="_mc4wp_timestamp" value="1522839180" /><input type="hidden" name="_mc4wp_form_id" value="65" /><input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-2" /></div><div class="mc4wp-response"></div></form> </div>
</div>
</div>
<div class="df-floating-close"><i class="ion-ios-close-empty"></i></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-20219691-40', 'auto');
  ga('send', 'pageview');

</script><script type='text/javascript'>
/* <![CDATA[ */
var carousel = {"type":"slider","auto_play":"1","slide_type":"df-slider-4"};
/* ]]> */
</script>
<script type='text/javascript' src='../wp-content/themes/applique/assets/js/owl.carousel.min001e.js?ver=2.0.0'></script>
<script type='text/javascript' src='../wp-content/themes/applique/assets/js/waypointcce7.js?ver=4.0.0'></script>
<script type='text/javascript' src='../wp-content/themes/applique/assets/js/fitvids4963.js?ver=1.1'></script>
<script type='text/javascript' src='../wp-content/themes/applique/assets/js/debounced-resize.js'></script>
<script type='text/javascript' src='../wp-content/themes/applique/assets/js/parallaxc358.js?ver=1.1.3'></script>
<script type='text/javascript' src='../wp-content/themes/applique/assets/js/grid605a.js?ver=2.2.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var inf = {"finishText":"All Post Loaded"};
/* ]]> */
</script>
<script type='text/javascript' src='../wp-content/themes/applique/assets/js/infinite-scroll3c94.js?ver=2.1.0'></script>
<script type='text/javascript' src='../wp-content/themes/applique/assets/js/jquery.scrolldepth.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var df = {"navClass":""};
/* ]]> */
</script>
<script type='text/javascript' src='../wp-content/themes/applique/assets/js/main.minfeec.js?ver=1.6.5'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */
</script>
<script type='text/javascript' src='../wp-content/plugins/contact-form-7/includes/js/scripts5597.js?ver=5.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var sb_instagram_js_options = {"sb_instagram_at":"612741813.3a81a9f.4472fbf925924a709fc46205b5ed9a6b"};
/* ]]> */
</script>
<script type='text/javascript' src='../wp-content/plugins/instagram-feed/js/sb-instagram.minf24c.js?ver=1.6'></script>
<script type='text/javascript' src='../../../../s0.wp.com/wp-content/js/devicepx-jetpack1a52.js?ver=201814'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/applique\/menswear\/wp-admin\/admin-ajax.php","wc_ajax_url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script type='text/javascript' src='../wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min3d36.js?ver=3.3.1'></script>
<script type='text/javascript' src='../wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min44fd.js?ver=2.70'></script>
<script type='text/javascript' src='../wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min6b25.js?ver=2.1.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/applique\/menswear\/wp-admin\/admin-ajax.php","wc_ajax_url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script type='text/javascript' src='../wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min3d36.js?ver=3.3.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/applique\/menswear\/wp-admin\/admin-ajax.php","wc_ajax_url":"http:\/\/dahz.daffyhazan.com\/applique\/menswear\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_60bed813bcdf7f8a43b3e086e55ad050","fragment_name":"wc_fragments_60bed813bcdf7f8a43b3e086e55ad050"};
/* ]]> */
</script>
<script type='text/javascript' src='../wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min3d36.js?ver=3.3.1'></script>
<script type='text/javascript' src='../wp-includes/js/wp-embed.min55fe.js?ver=4.9.5'></script>
<script type='text/javascript' src='../wp-content/themes/applique/assets/js/TheiaStickySidebar8d1e.js?ver=1.2.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var mc4wp_forms_config = [];
/* ]]> */
</script>
<script type='text/javascript' src='../wp-content/plugins/mailchimp-for-wp/assets/js/forms-api.min2d73.js?ver=3.1.11'></script>
<!--[if lte IE 9]>
<script type='text/javascript' src='http://dahz.daffyhazan.com/applique/menswear/wp-content/plugins/mailchimp-for-wp/assets/js/third-party/placeholders.min.js?ver=3.1.11'></script>
<![endif]-->
<script type="text/javascript">(function() {function addEventListener(element,event,handler) {
	if(element.addEventListener) {
		element.addEventListener(event,handler, false);
	} else if(element.attachEvent){
		element.attachEvent('on'+event,handler);
	}
}})();</script>
</body>

<!-- Mirrored from dahz.daffyhazan.com/applique/menswear/contact-me/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 11:10:11 GMT -->
</html>