create database Tennis;
use Tennis

create table users(
    id Integer not null auto_increment,
    name varchar(50),
    password varchar(50),
    email varchar(50),
    mode int,
    status int,
    primary key(id)
)Engine=innoDB;


insert into users values(1,'narindra','nax','narindra@motorent.mg',1,1);
insert into users values('','root','root','root@dagocar.mg',1,1);

create table products(
    id Integer not null auto_increment,
    name varchar(50),
    image varchar(50),
    category int,
    price numeric(10,2),
    primary key(id)
)Engine=innoDB;

insert into products values(1,'','nike1',1,500);
insert into products values('','','nike2',1,500);
insert into products values('','','nike3',1,600);
insert into products values('','','nike4',1,600);
insert into products values('','','nike5',1,700);
insert into products values('','','nike6',1,700);
insert into products values('','','nike7',1,800);
insert into products values('','','nike8',1,800);
insert into products values('','','nike9',1,900);
insert into products values('','','nike10',1,900);

insert into products values('','','adidas1',2,500);
insert into products values('','','adidas2',2,500);
insert into products values('','','adidas3',2,600);
insert into products values('','','adidas4',2,600);
insert into products values('','','adidas5',2,700);
insert into products values('','','adidas6',2,700);
insert into products values('','','adidas7',2,800);
insert into products values('','','adidas8',2,800);
insert into products values('','','adidas9',2,900);
insert into products values('','','adidas10',2,900);

insert into products values('','','lacoste1',2,500);
insert into products values('','','lacoste2',2,600);
insert into products values('','','lacoste3',2,700);
insert into products values('','','lacoste4',2,800);
insert into products values('','','lacoste5',2,900);

insert into products values('','','fila1',2,500);
insert into products values('','','fila2',2,600);
insert into products values('','','fila3',2,700);
insert into products values('','','fila4',2,800);
insert into products values('','','fila5',2,900);

create table category(
    id Integer not null auto_increment,
    name varchar(50),
    primary key(id)
)Engine=innoDB;
insert into category values(1,'Nike');
insert into category values('','Adidas');
insert into category values('','Lacoste');
insert into category values('','Fila');
